<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBBRANCH"%>
<%@page import="Biometrics.TBATTENDANCE"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBACCESSGROUP"%>
<%@page import="util.ProcessUtil"%>
<%@page import="java.io.*"%>
<%@page import="util.DateFormatConverter"%>
<%@page import="java.util.Date"%>
<%@page import="Biometrics.REPORT1"%>

<jsp:useBean id="AccessGroupMgr" scope="page" class="Biometrics.AccessGroupManager" />
<jsp:useBean id="AttendanceMgr" scope="page" class="Biometrics.AttendanceManager" />
<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="ProcessUtl" scope="session" class="util.ProcessUtil" />

<% String sTableTitle = "Monthly Area Manager Report"; %>
<% String sDataTitle = "Monthly Report"; %>
<% String sPageName = "MonthlyReport.jsp"; %>
<% String sBackPageName = "MonthlyReport.jsp"; %>
<% String sProcessPageName = "processor/GenerateMonthlyReport.jsp"; %>

<%!TBUSER SessionUser; %>
<%!TBBRANCH[] aBranchs;%>
<%!TBACCESSGROUP[] aAccessGroups;%>

<%!String txtStaffId; %>
<%!String cbBranchId; %>
<%!String txtMonth; %>
<%!String txtYear; %>

<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	BranchMgr.setConnection(DBMgr.getConnection());
	AccessGroupMgr.setConnection(DBMgr.getConnection());
	AttendanceMgr.setConnection(DBMgr.getConnection());
	SessionUser = UserMgr.getUserById(SessionUserId);
	
	aBranchs = BranchMgr.getBranchAllNoOffset();
	aAccessGroups = AccessGroupMgr.getAccessGroupAllNoOffset();
	
	txtStaffId = request.getParameter("txtStaffId");
	cbBranchId = request.getParameter("cbBranchId");
	txtMonth = request.getParameter("txtMonth");
	txtYear = request.getParameter("txtYear");
	
	if (txtStaffId == null || txtStaffId.trim().length() <= 0) {
		txtStaffId = "";		
	}
	if (cbBranchId == null || cbBranchId.trim().length() <= 0) {
		cbBranchId = "";		
	}
	if (txtMonth == null || txtMonth.trim().length() <= 0) {
		txtMonth = DateFormatConverter.format(new Date(), "MM");		
	}
	if (txtYear == null || txtYear.trim().length() <= 0) {
		txtYear = DateFormatConverter.format(new Date(), "yyyy");		
	}
		
	String genMonth =  txtYear + txtMonth;

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
                               	<div class="form-group">
                                    <label>Staff Id</label>
                                    <input class="form-control" id="txtStaffId" type="text" name="txtStaffId" value = "<%=txtStaffId%>" />
                                </div>
                                <!-- <div class="form-group">
                                    <label>Branch</label>
                                    <select class="form-control selectpicker" data-live-search="true"  id ="cbBranchId" name ="cbBranchId">
										<option value = "">All Branch</option>
										<%if(aBranchs != null){ %>
											<%for (int i = 0; i < aBranchs.length; i++) { %>
												<option value = "<%=aBranchs[i].getBRANCHID()%>" <%=cbBranchId.equals(aBranchs[i].getBRANCHID().trim())?"selected":""%>><%=aBranchs[i].getBRANCHNAME()%></option>
											<%} %>
										<%} %>
									</select>
                                </div> -->
                                <div class="form-group">
                                    <label>Access Group</label>
                                    <select class="form-control" name ="cbAccessGroupId">
                                    <option value = "">All Access Group</option>
									<%if(aAccessGroups != null){ %>
										<%for (int i = 0; i < aAccessGroups.length; i++) { %>
											<option value = "<%=aAccessGroups[i].getACCESSGROUPID()%>"><%=aAccessGroups[i].getACCESSGROUPNAME()%></option>
										<%} %>
									<%} %>
									</select>
                                </div>
                                <div class="form-group">
                                    <label>Monthly - Year</label>
									<div class="row">
										<div class="col-md-6">
											<div class="input-group">
												<input class="form-control" id = "txtYear" name="txtYear" type="text" placeholder="YYYY" size="4" onkeypress="return isNumberKey(event)" value="<%= txtYear == null ? "" : txtYear%>" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<input class="form-control" id = "txtMonth" name="txtMonth" type="text" placeholder="MM" size="2" onkeypress="return isNumberKey(event)" value="<%= txtMonth == null ? "" : txtMonth%>" />
											</div>
										</div>
									</div>									
                                </div>
                                <button type="submit" class="btn btn-info">Download </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
