<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.AreaManager"%>
<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="AreaMgr" scope="page" class="Biometrics.AreaManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sTableTitle = "Edit Area"; %>
<% String sDataTitle = "Area Detail"; %>
<% String sPageName = "AreaEdit.jsp"; %>
<% String sBackPageName = "AreaList.jsp" + CurrentParam; %>
<% String sProcessPageName = "processor/UpdateArea.jsp"; %>

<%!Biometrics.TBUSER SessionUser;%>
<%!Biometrics.TBAREA sArea;%>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0; String sAreaId = ""; String sAreaName = "";
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	AreaMgr.setConnection(DBMgr.getConnection());
	
	sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
	SessionUser = UserMgr.getUserById(SessionUserId); 	
	sArea = AreaMgr.getAreaById(sId);	

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (sArea == null) {
		request.getSession().setAttribute("LastErrMsg", "Area not found");
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		return;
	}
	
	sId = sArea.getID();
	sAreaId = sArea.getAREAID();
	sAreaName = sArea.getAREANAME();
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
 
function CheckForm(theform) {

	if (!CheckAreaId(theform)){
		return (false);
	}
	
	if (!CheckAreaName(theform)){
		return (false);
	}
	  
	return (true);
}

function CheckAreaId(theForm){
    if (theForm.txtAreaId.value==""){
       	alert("Please enter Area Id!");
        return (false);
    }
    return (true);
}

function CheckAreaName(theForm){
    if (theForm.txtAreaName.value==""){
       	alert("Please enter Area Name!");
        return (false);
    }
    return (true);
}
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
								<input type=hidden name="hdnId" value='<%=sId%>' />
                                <div class="form-group">
                                    <label>Area Id</label>
                                    <input class="form-control" id="txtAreaId" type="text" name="txtAreaId" value = "<%=sAreaId%>" readonly />
                                </div>
                                <div class="form-group">
                                    <label>Area Name</label>
                                    <input class="form-control" id="txtAreaName" type="text" name="txtAreaName" value = "<%=sAreaName%>" />
                                </div>
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
