﻿// JScript File
// Handle the option movement between two Multiple Selection List.

function addOption(theSel, theText, theValue)
{
    var newOpt = new Option(theText, theValue);
    var selLength = theSel.length;
    theSel.options[selLength] = newOpt;
}

function deleteOption(theSel, theIndex)
{ 
    var selLength = theSel.length;
    
    if(selLength>0)
    {
        theSel.options[theIndex] = null;
    }
}

function moveOptions(theSelFrom, theSelTo)
{
    var selLength = theSelFrom.length;
    var selectedText = new Array();
    var selectedValues = new Array();
    var selectedCount = 0;
    var bolFound = false;
    var i;
  
    // Find the selected Options in reverse order
    // and delete them from the 'from' Select.

    for(i=selLength-1; i>=0; i--)
    {
        if(theSelFrom.options[i].selected)
        {
            selectedText[selectedCount] = theSelFrom.options[i].text;
            selectedValues[selectedCount] = theSelFrom.options[i].value;
            deleteOption(theSelFrom, i);
            selectedCount++;
            bolFound = true;
        }
    }
  
    if (!bolFound)
    {
        alert("Nothing to move!");
        return false;
    }
  
    // Add the selected text/values in reverse order.
    // This will add the Options to the 'to' Select
    // in the same order as they were in the 'from' Select.
    for(i=selectedCount-1; i>=0; i--)
    {
	    addOption(theSelTo, selectedText[i], selectedValues[i]);
    }
 
}

function selectAllOption(theSel)
{
	var selLength = theSel.length;

	for (i = 0; i < selLength; i++)
	{
		theSel.options[i].selected = true;
	}
	
	return true;
}