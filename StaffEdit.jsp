<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.StaffManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBACCESSGROUP"%>
<%@page import="Biometrics.TBBRANCH"%>

<jsp:useBean id="AccessGroupMgr" scope="page" class="Biometrics.AccessGroupManager" />
<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />
<jsp:useBean id="StaffMgr" scope="page" class="Biometrics.StaffManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sTableTitle = "Edit Staff"; %>
<% String sDataTitle = "Staff Detail"; %>
<% String sPageName = "StaffEdit.jsp"; %>
<% String sBackPageName = "StaffList.jsp" + CurrentParam; %>
<% String sProcessPageName = "processor/UpdateStaff.jsp"; %>

<%!Biometrics.TBUSER SessionUser;%>
<%!Biometrics.TBSTAFF sStaff;%>
<%!TBACCESSGROUP[] aAccessGroups;%>
<%!TBBRANCH[] aBranchs;%>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
int sId = 0; String sStaffId = ""; String sStaffName = ""; String sPrivilege = ""; String sAccessGroupId = ""; String sDepartmentId = ""; String sCardNo = ""; String sPassword = ""; String sBranchId = "";
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	AccessGroupMgr.setConnection(DBMgr.getConnection());
	StaffMgr.setConnection(DBMgr.getConnection());
	BranchMgr.setConnection(DBMgr.getConnection());
	
	sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
	SessionUser = UserMgr.getUserById(SessionUserId); 	
	aAccessGroups = AccessGroupMgr.getAccessGroupAllNoOffset();
	sStaff = StaffMgr.getStaffById(sId);
	aBranchs = BranchMgr.getBranchAllNoOffset();

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (sStaff == null) {
		request.getSession().setAttribute("LastErrMsg", "Staff not found");
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		return;
	}
	
	sId = sStaff.getID();
	sStaffId = sStaff.getSTAFFID();
	sStaffName = sStaff.getSTAFFNAME();
	sPrivilege = sStaff.getPRIVILEGE();
	sAccessGroupId = sStaff.getACCESSGROUPID();
	sCardNo = sStaff.getCARDNO();
	sPassword = sStaff.getPASSWORD();	
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
 
function CheckForm(theform) {

	if (!CheckStaffId(theform)){
		return (false);
	}
	
	if (!CheckStaffName(theform)){
		return (false);
	}
	
	if (!CheckAccessGroup(theform)){
		return (false);
	}
	
	if (!CheckBranch(theform)){
		return (false);
	}
	
	return (true);
}

function CheckStaffId(theForm){
    if (theForm.txtStaffId.value==""){
       	alert("Please enter Staff Id!");
        return (false);
    }
    return (true);
}

function CheckStaffName(theForm){
    if (theForm.txtStaffName.value==""){
       	alert("Please enter Staff Name!");
        return (false);
    }
    return (true);
}

function CheckAccessGroup(theForm){
    if (theForm.cbAccessGroupId.value==""){
       	alert("Please select AccessGroup!");
        return (false);
    }
    return (true);
}

function CheckBranch(theForm){
    if (theForm.cbBranchId.value==""){
       	alert("Please select Branch!");
        return (false);
    }
    return (true);
}
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
								<input type=hidden name="hdnId" value='<%=sId%>' />
                                <div class="form-group">
                                    <label>Staff Id</label>
                                    <input class="form-control" id="txtStaffId" type="text" name="txtStaffId" value = "<%=sStaffId%>" readonly />
                                </div>
                                <div class="form-group">
                                    <label>Staff Name</label>
                                    <input class="form-control" id="txtStaffName" type="text" name="txtStaffName" value = "<%=sStaffName%>" />
                                </div>
								<div class="form-group">
                                    <label>Privilege</label>
									<select class="form-control" name ="cbPrivilege">
										<option value = "0" <%=sPrivilege.equals("0")?"selected":""%>>Common User</option>
										<option value = "1" <%=sPrivilege.equals("1")?"selected":""%>>Registra User</option>
										<option value = "2" <%=sPrivilege.equals("2")?"selected":""%>>Administrator</option>
									</select>
                                </div>
								<div class="form-group">
                                    <label>Access Group</label>
                                    <select class="form-control" name ="cbAccessGroupId">
									<%if(aAccessGroups != null){ %>
										<%for (int i = 0; i < aAccessGroups.length; i++) { %>
											<option value = "<%=aAccessGroups[i].getACCESSGROUPID()%>" <%=sAccessGroupId.equals(aAccessGroups[i].getACCESSGROUPID())?"selected":""%>><%=aAccessGroups[i].getACCESSGROUPNAME()%></option>
										<%} %>
									<%} %>
									</select>
                                </div>
								<div class="form-group">
                                    <label>Card No</label>
                                    <input class="form-control" id="txtCardNo" type="text" name="txtCardNo" value = "<%=sCardNo%>" />
                                </div>
								<div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" id="txtPassword" type="password" name="txtPassword" value = "<%=sPassword%>" />
                                </div>
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
