<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBBRANCH"%>
<%@page import="Biometrics.TBDEVICE"%>
<%@page import="Biometrics.TBATTENDANCE"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="util.ProcessUtil"%>
<%@page import="java.io.*"%>
<%@page import="util.DateFormatConverter"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>


<%@page import="Biometrics.REPORT1"%>

<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />
<jsp:useBean id="AttendanceMgr" scope="page" class="Biometrics.AttendanceManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="ProcessUtl" scope="session" class="util.ProcessUtil" />

<% String sTableTitle = "Report"; %>
<% String sDataTitle = "Report Detail"; %>
<% String sPageName = "Report.jsp"; %>
<% String sAddPageName = ""; %>
<% String sEditPageName = ""; %>
<% String sDeletePageName = ""; %>
<% String sBackPageName = "Report.jsp"; %>
<% String sProcessPageName = "processor/GenerateReport.jsp"; %>

<%!TBBRANCH[] aBranchs;%>
<%!String txtStaffId; %>
<%!String cbBranchId; %>
<%!String sReportType; %>
<%!TBUSER SessionUser; %>
<%!String txtStartDate; %>
<%!String txtStartMonth; %>
<%!String txtStartYear; %>
<%!String txtEndDate; %>
<%!String txtEndMonth; %>
<%!String txtEndYear; %>

<%!Biometrics.REPORT1[] aReport1;%>
<%!String sReport1Display; %>
<%!int iPage1; %>
<%!int iMax1Page; %>
<%!String sTotalRecord1; %>
<%!String sOffset1; %>

<%!Biometrics.REPORT2[] aReport2;%>
<%!String sReport2Display; %>
<%!int iPage2; %>
<%!int iMax2Page; %>
<%!String sTotalRecord2; %>
<%!String sOffset2; %>

<%!Biometrics.REPORT3[] aReport3;%>
<%!String sReport3Display; %>
<%!int iPage3; %>
<%!int iMax3Page; %>
<%!String sTotalRecord3; %>
<%!String sOffset3; %>

<%!String sTotalRecord = "0"; %>
<%!String sOffset = "0"; %>
<%!int iPage = 0; %>
<%!int iMaxPage = 0; %>
<%!int iReportNo = 1; %>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}

	UserMgr.setConnection(DBMgr.getConnection());
	BranchMgr.setConnection(DBMgr.getConnection());
	AttendanceMgr.setConnection(DBMgr.getConnection());
	
	SessionUser = UserMgr.getUserById(SessionUserId);
	aBranchs = BranchMgr.getBranchAllNoOffset();
	
	txtStaffId = request.getParameter("txtStaffId");
	cbBranchId = request.getParameter("cbBranchId");
	sReportType = request.getParameter("comboReport");
	txtStartDate = request.getParameter("txtStartDate");
	txtStartMonth = request.getParameter("txtStartMonth");
	txtStartYear = request.getParameter("txtStartYear");
	txtEndDate = request.getParameter("txtEndDate");
	txtEndMonth = request.getParameter("txtEndMonth");
	txtEndYear = request.getParameter("txtEndYear");
	
	sOffset1 = request.getParameter("txtOffset1");
	sOffset2 = request.getParameter("txtOffset2");
	sOffset3 = request.getParameter("txtOffset3");
	
	if (txtStaffId == null || txtStaffId.trim().length() <= 0) {
		txtStaffId = "";		
	}
	if (cbBranchId == null || cbBranchId.trim().length() <= 0) {
		cbBranchId = "";		
	}
	if (sReportType == null || sReportType.trim().length() <= 0) {
		sReportType = "1";		
	}
	if (txtStartDate == null || txtStartDate.trim().length() <= 0) {
		txtStartDate = DateFormatConverter.format(new Date(), "dd");		
	}
	if (txtStartMonth == null || txtStartMonth.trim().length() <= 0) {
		txtStartMonth = DateFormatConverter.format(new Date(), "MM");	
	}
	if (txtStartYear == null || txtStartYear.trim().length() <= 0) {
		txtStartYear = DateFormatConverter.format(new Date(), "yyyy");		
	}
	if (txtEndDate == null || txtEndDate.trim().length() <= 0) {
		txtEndDate = DateFormatConverter.format(new Date(), "dd");		
	}
	if (txtEndMonth == null || txtEndMonth.trim().length() <= 0) {
		txtEndMonth = DateFormatConverter.format(new Date(), "MM");	
	}
	if (txtEndYear == null || txtEndYear.trim().length() <= 0) {
		txtEndYear = DateFormatConverter.format(new Date(), "yyyy");		
	}
	if (sOffset1 == null || sOffset1.trim().length() <= 0) {
		sOffset1 = "0";		
	}
	if (sOffset2 == null || sOffset2.trim().length() <= 0) {
		sOffset2 = "0";		
	}
	if (sOffset3 == null || sOffset3.trim().length() <= 0) {
		sOffset3 = "0";		
	}
	
	String Start =  txtStartYear + txtStartMonth + txtStartDate;
	String End =  txtEndYear + txtEndMonth + txtEndDate;
		
	if(sReportType.equals("1")){
		sReport2Display = "none";
		aReport2 = null;
		sTotalRecord2 = "0";
		
		sReport3Display = "none";
		aReport3 = null;
		sTotalRecord3 = "0";
		
		sReport1Display = "block";
		aReport1 = AttendanceMgr.genReport1View(txtStaffId, cbBranchId, Start, End, sOffset1);
		sTotalRecord1 = AttendanceMgr.countReport1View(txtStaffId, cbBranchId, Start, End) + "";	
		iPage1 = (Integer.parseInt(sOffset1) / 30) + 1;
		if(Integer.parseInt(sTotalRecord1) % 30 != 0){
			iMax1Page = (Integer.parseInt(sTotalRecord1) / 30) + 1;
		} else {
			iMax1Page = (Integer.parseInt(sTotalRecord1) / 30);
		}
		
		
	} else if(sReportType.equals("2")){
		sReport1Display = "none";
		aReport1 = null;
		sTotalRecord1 = "0";
		
		sReport3Display = "none";
		aReport3 = null;
		sTotalRecord3 = "0";
		
		sReport2Display = "block";
		aReport2 = AttendanceMgr.genReport2View(txtStaffId, cbBranchId, Start, End, sOffset2);
		sTotalRecord2 = AttendanceMgr.countReport2View(txtStaffId,cbBranchId, Start, End) + "";
		iPage2 = (Integer.parseInt(sOffset2) / 30) + 1;
		if(Integer.parseInt(sTotalRecord2) % 30 != 0){
			iMax2Page = (Integer.parseInt(sTotalRecord2) / 30) + 1;
		} else {
			iMax2Page = (Integer.parseInt(sTotalRecord2) / 30);
		}
	} else if(sReportType.equals("3")){
		sReport1Display = "none";
		aReport1 = null;
		sTotalRecord1 = "0";
		
		sReport2Display = "none";
		aReport2 = null;
		sTotalRecord2 = "0";
		
		sReport3Display = "block";
		aReport3 = AttendanceMgr.genReport3View(txtStaffId, cbBranchId, Start, End, sOffset3);
		sTotalRecord3 = AttendanceMgr.countReport3View(txtStaffId, cbBranchId, Start, End) + "";
		iPage3 = (Integer.parseInt(sOffset3) / 30) + 1;
		if(Integer.parseInt(sTotalRecord3) % 30 != 0){
			iMax3Page = (Integer.parseInt(sTotalRecord3) / 30) + 1;
		} else {
			iMax3Page = (Integer.parseInt(sTotalRecord3) / 30);
		}
	}
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
}
%>

<link href="assets/css/bootstrap-select.min.css" rel="stylesheet" />
<script src="assets/js/bootstrap-select.min.js"></script>
<script language="javascript" type="text/javascript">

$(function() {
    $(".pageNumber1").click(function (){
    	var sNext1Offset = (parseInt($(this).html()) - 1) * 30;
    	var TotalRecord1 = parseInt($("#TotalRecord1").val());
		
    	var txtStaffId = $('#txtStaffId').val();
		var cbBranchId = $('#cbBranchId').val();
		var comboReport = $('#comboReport').val();
		var txtStartDate = $('#txtStartDate').val();
		var txtStartMonth = $('#txtStartMonth').val();
		var txtStartYear = $('#txtStartYear').val();
		var txtEndDate = $('#txtEndDate').val();
		var txtEndMonth = $('#txtEndMonth').val();
		var txtEndYear = $('#txtEndYear').val();
		
		HidePaging();
		
		var targetHref="Report.jsp?" +
			"txtOffset1=" + sNext1Offset + 
			"&txtStaffId=" + txtStaffId +
			"&cbBranchId=" + cbBranchId +
			"&comboReport=" + comboReport + 
			"&txtStartDate=" + txtStartDate + 
			"&txtStartMonth=" + txtStartMonth + 
			"&txtStartYear=" + txtStartYear + 
			"&txtEndDate=" + txtEndDate + 
			"&txtEndMonth=" + txtEndMonth + 
			"&txtEndYear=" + txtEndYear + "";
    	$(this).attr("href", targetHref);    	
     });
	 
	 $(".pageNumber2").click(function (){
    	var sNext2Offset = (parseInt($(this).html()) - 1) * 30;
    	var TotalRecord2 = parseInt($("#TotalRecord2").val());
		
    	var txtStaffId = $('#txtStaffId').val();
		var cbBranchId = $('#cbBranchId').val();
		var comboReport = $('#comboReport').val();
		var txtStartDate = $('#txtStartDate').val();
		var txtStartMonth = $('#txtStartMonth').val();
		var txtStartYear = $('#txtStartYear').val();
		var txtEndDate = $('#txtEndDate').val();
		var txtEndMonth = $('#txtEndMonth').val();
		var txtEndYear = $('#txtEndYear').val();
		
		HidePaging();
		
		var targetHref="Report.jsp?" +
			"txtOffset2=" + sNext2Offset + 
			"&txtStaffId=" + txtStaffId +
			"&cbBranchId=" + cbBranchId + 
			"&comboReport=" + comboReport + 
			"&txtStartDate=" + txtStartDate + 
			"&txtStartMonth=" + txtStartMonth + 
			"&txtStartYear=" + txtStartYear + 
			"&txtEndDate=" + txtEndDate + 
			"&txtEndMonth=" + txtEndMonth + 
			"&txtEndYear=" + txtEndYear + "";
    	$(this).attr("href", targetHref);    	
     });
	
	 $(".pageNumber3").click(function (){
    	var sNext3Offset = (parseInt($(this).html()) - 1) * 30;
    	var TotalRecord3 = parseInt($("#TotalRecord3").val());
		
    	var txtStaffId = $('#txtStaffId').val();
		var cbBranchId = $('#cbBranchId').val();
		var comboReport = $('#comboReport').val();
		var txtStartDate = $('#txtStartDate').val();
		var txtStartMonth = $('#txtStartMonth').val();
		var txtStartYear = $('#txtStartYear').val();
		var txtEndDate = $('#txtEndDate').val();
		var txtEndMonth = $('#txtEndMonth').val();
		var txtEndYear = $('#txtEndYear').val();
		
		HidePaging();
		
		var targetHref="Report.jsp?" +
			"txtOffset3=" + sNext3Offset + 
			"&txtStaffId=" + txtStaffId +
			"&cbBranchId=" + cbBranchId + 
			"&comboReport=" + comboReport + 
			"&txtStartDate=" + txtStartDate + 
			"&txtStartMonth=" + txtStartMonth + 
			"&txtStartYear=" + txtStartYear + 
			"&txtEndDate=" + txtEndDate + 
			"&txtEndMonth=" + txtEndMonth + 
			"&txtEndYear=" + txtEndYear + "";
    	$(this).attr("href", targetHref);    	
     });
 	 
    $("#btnGenerate").click(function (){
    	
    	var sOffset1 = 0;
    	var sOffset2 = 0;
    	var sOffset3 = 0;
    	var TotalRecord1 = parseInt($("#TotalRecord1").val());	
    	var TotalRecord2 = parseInt($("#TotalRecord2").val());	
    	var TotalRecord3 = parseInt($("#TotalRecord3").val());	

   		HidePaging();
   		
   		var txtStaffId = $('#txtStaffId').val();
   		var cbBranchId = $('#cbBranchId').val();
   		var comboReport = $('#comboReport').val();
   		var txtStartDate = $('#txtStartDate').val();
   		var txtStartMonth = $('#txtStartMonth').val();
   		var txtStartYear = $('#txtStartYear').val();
   		var txtEndDate = $('#txtEndDate').val();
   		var txtEndMonth = $('#txtEndMonth').val();
   		var txtEndYear = $('#txtEndYear').val();
   		
   		var targetHref="Report.jsp?" +
   				"txtOffset1=" + sOffset1 + 
   				"&txtOffset2=" + sOffset2 + 
   				"&txtOffset3=" + sOffset3 + 
   				"&txtStaffId=" + txtStaffId +
   				"&cbBranchId=" + cbBranchId + 
   				"&comboReport=" + comboReport + 
   				"&txtStartDate=" + txtStartDate + 
   				"&txtStartMonth=" + txtStartMonth + 
   				"&txtStartYear=" + txtStartYear + 
   				"&txtEndDate=" + txtEndDate + 
   				"&txtEndMonth=" + txtEndMonth + 
   				"&txtEndYear=" + txtEndYear + "";
   		
   		window.location = targetHref;
     });
    
	$("#btnDownload").click(function (){
		$("#hdnReportExt").val("csv");
		$("#myform").submit();
     });
	$("#btnDownloadPdf").click(function (){
		$("#hdnReportExt").val("pdf");
		$("#myform").submit();
     });
 });
 
function HidePaging(){
	$(".pageNumber1").css("display","none");	
	$(".Next1").css("display","none");	
	$(".Prev1").css("display","none");	
	$(".currentPage1").css("display","none");
	$(".minPageNumber1").css("display","none");
	$(".maxPageNumber1").css("display","none");
	
	$(".pageNumber2").css("display","none");	
	$(".Next2").css("display","none");	
	$(".Prev2").css("display","none");	
	$(".currentPage2").css("display","none");
	$(".minPageNumber2").css("display","none");
	$(".maxPageNumber2").css("display","none");	
	
	$(".pageNumber3").css("display","none");	
	$(".Next3").css("display","none");	
	$(".Prev3").css("display","none");	
	$(".currentPage3").css("display","none");
	$(".minPageNumber3").css("display","none");
	$(".maxPageNumber3").css("display","none");
}
 
function IsConfirmedDelete(obj)
{
	return confirm("Are you sure want to delete?");	
}
function IsConfirmedImport(obj)
{
	return confirm("Are you sure want to import?");	
}
function IsConfirmedSearch(obj)
{	
	$("#thisForm").attr("action", "Report.jsp");	
	$("#thisForm").submit();
	return true;
}

function Next1(obj)
{	
	var nextLink = document.getElementById("Next1");
	var prevLink = document.getElementById("Prev1");
		
	var sOffset1 = parseInt($("#txtOffset1").val());
	var TotalRecord1 = parseInt($("#TotalRecord1").val());	
	if(sOffset1+10 <=  TotalRecord1){
		HidePaging();
		
		var txtStaffId = $('#txtStaffId').val();
		var cbBranchId = $('#cbBranchId').val();
		var comboReport = $('#comboReport').val();
		var txtStartDate = $('#txtStartDate').val();
		var txtStartMonth = $('#txtStartMonth').val();
		var txtStartYear = $('#txtStartYear').val();
		var txtEndDate = $('#txtEndDate').val();
		var txtEndMonth = $('#txtEndMonth').val();
		var txtEndYear = $('#txtEndYear').val();
		
		sOffset1 = sOffset1 + 30;
		
		var targetHref="Report.jsp?" +
				"txtOffset1=" + sOffset1 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
		
		$(".Next1").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Prev1(obj)
{	
	var nextLink = document.getElementById("Next1");
	var prevLink = document.getElementById("Prev1");
	
	var sOffset1 = parseInt($("#txtOffset1").val());
	if(sOffset1 > 0){	
		HidePaging();
		
		var txtStaffId = $('#txtStaffId').val();
		var cbBranchId = $('#cbBranchId').val();
		var comboReport = $('#comboReport').val();
		var txtStartDate = $('#txtStartDate').val();
		var txtStartMonth = $('#txtStartMonth').val();
		var txtStartYear = $('#txtStartYear').val();
		var txtEndDate = $('#txtEndDate').val();
		var txtEndMonth = $('#txtEndMonth').val();
		var txtEndYear = $('#txtEndYear').val();
		
		sOffset1 = sOffset1 - 30;
		
		var targetHref="Report.jsp?" +
				"txtOffset1=" + sOffset1 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
		
		$(".Prev1").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Max1(obj)
{
	var sOffset1 = (parseInt($("#Max1Page").val()) - 1) * 30;
	var TotalRecord1 = parseInt($("#TotalRecord1").val());	
	HidePaging();
	
	var txtStaffId = $('#txtStaffId').val();
	var cbBranchId = $('#cbBranchId').val();
	var comboReport = $('#comboReport').val();
	var txtStartDate = $('#txtStartDate').val();
	var txtStartMonth = $('#txtStartMonth').val();
	var txtStartYear = $('#txtStartYear').val();
	var txtEndDate = $('#txtEndDate').val();
	var txtEndMonth = $('#txtEndMonth').val();
	var txtEndYear = $('#txtEndYear').val();
	
	var targetHref="Report.jsp?" +
				"txtOffset1=" + sOffset1 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
				
	$(".maxPageNumber1").attr("href", targetHref); 
	return true;
}

function Min1(obj)
{
	var sOffset1 = 0;
	var TotalRecord1 = parseInt($("#TotalRecord1").val());	
	HidePaging();		

	var txtStaffId = $('#txtStaffId').val();
	var cbBranchId = $('#cbBranchId').val();
	var comboReport = $('#comboReport').val();
	var txtStartDate = $('#txtStartDate').val();
	var txtStartMonth = $('#txtStartMonth').val();
	var txtStartYear = $('#txtStartYear').val();
	var txtEndDate = $('#txtEndDate').val();
	var txtEndMonth = $('#txtEndMonth').val();
	var txtEndYear = $('#txtEndYear').val();
	
	var targetHref="Report.jsp?" +
				"txtOffset1=" + sOffset1 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
					
	$(".minPageNumber1").attr("href", targetHref); 
	return true;
}

function Next2(obj)
{	
	var nextLink = document.getElementById("Next2");
	var prevLink = document.getElementById("Prev2");
		
	var sOffset2 = parseInt($("#txtOffset2").val());
	var TotalRecord2 = parseInt($("#TotalRecord2").val());	
	if(sOffset2+10 <=  TotalRecord2){
		HidePaging();
		
		var txtStaffId = $('#txtStaffId').val();
		var cbBranchId = $('#cbBranchId').val();
		var comboReport = $('#comboReport').val();
		var txtStartDate = $('#txtStartDate').val();
		var txtStartMonth = $('#txtStartMonth').val();
		var txtStartYear = $('#txtStartYear').val();
		var txtEndDate = $('#txtEndDate').val();
		var txtEndMonth = $('#txtEndMonth').val();
		var txtEndYear = $('#txtEndYear').val();
		
		
		sOffset2 = sOffset2 + 30;
		
		var targetHref="Report.jsp?" +
				"txtOffset2=" + sOffset2 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
		
		$(".Next2").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Prev2(obj)
{	
	var nextLink = document.getElementById("Next2");
	var prevLink = document.getElementById("Prev2");
	
	var sOffset2 = parseInt($("#txtOffset2").val());
	if(sOffset2 > 0){	
		HidePaging();
		
		var txtStaffId = $('#txtStaffId').val();
		var cbBranchId = $('#cbBranchId').val();
		var comboReport = $('#comboReport').val();
		var txtStartDate = $('#txtStartDate').val();
		var txtStartMonth = $('#txtStartMonth').val();
		var txtStartYear = $('#txtStartYear').val();
		var txtEndDate = $('#txtEndDate').val();
		var txtEndMonth = $('#txtEndMonth').val();
		var txtEndYear = $('#txtEndYear').val();
		
		sOffset2 = sOffset2 - 30;
		
		var targetHref="Report.jsp?" +
				"txtOffset2=" + sOffset2 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
		
		$(".Prev2").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Max2(obj)
{
	var sOffset2 = (parseInt($("#Max2Page").val()) - 1) * 30;
	var TotalRecord2 = parseInt($("#TotalRecord2").val());	
	HidePaging();
	
	var txtStaffId = $('#txtStaffId').val();
	var cbBranchId = $('#cbBranchId').val();
	var comboReport = $('#comboReport').val();
	var txtStartDate = $('#txtStartDate').val();
	var txtStartMonth = $('#txtStartMonth').val();
	var txtStartYear = $('#txtStartYear').val();
	var txtEndDate = $('#txtEndDate').val();
	var txtEndMonth = $('#txtEndMonth').val();
	var txtEndYear = $('#txtEndYear').val();
	
	var targetHref="Report.jsp?" +
				"txtOffset2=" + sOffset2 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
				
	$(".maxPageNumber2").attr("href", targetHref); 
	return true;
}

function Min2(obj)
{
	var sOffset2 = 0;
	var TotalRecord2 = parseInt($("#TotalRecord2").val());	
	HidePaging();		

	var txtStaffId = $('#txtStaffId').val();
	var cbBranchId = $('#cbBranchId').val();
	var comboReport = $('#comboReport').val();
	var txtStartDate = $('#txtStartDate').val();
	var txtStartMonth = $('#txtStartMonth').val();
	var txtStartYear = $('#txtStartYear').val();
	var txtEndDate = $('#txtEndDate').val();
	var txtEndMonth = $('#txtEndMonth').val();
	var txtEndYear = $('#txtEndYear').val();
	
	var targetHref="Report.jsp?" +
				"txtOffset2=" + sOffset2 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
					
	$(".minPageNumber2").attr("href", targetHref); 
	return true;
}

function Next3(obj)
{	
	var nextLink = document.getElementById("Next3");
	var prevLink = document.getElementById("Prev3");
		
	var sOffset3 = parseInt($("#txtOffset3").val());
	var TotalRecord3 = parseInt($("#TotalRecord3").val());	
	if(sOffset3+10 <=  TotalRecord3){
		HidePaging();
		
		var txtStaffId = $('#txtStaffId').val();
		var cbBranchId = $('#cbBranchId').val();
		var comboReport = $('#comboReport').val();
		var txtStartDate = $('#txtStartDate').val();
		var txtStartMonth = $('#txtStartMonth').val();
		var txtStartYear = $('#txtStartYear').val();
		var txtEndDate = $('#txtEndDate').val();
		var txtEndMonth = $('#txtEndMonth').val();
		var txtEndYear = $('#txtEndYear').val();
		
		sOffset3 = sOffset3 + 30;
		
		var targetHref="Report.jsp?" +
				"txtOffset3=" + sOffset3 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
		
		$(".Next3").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Prev3(obj)
{	
	var nextLink = document.getElementById("Next3");
	var prevLink = document.getElementById("Prev3");
	
	var sOffset3 = parseInt($("#txtOffset3").val());
	if(sOffset3 > 0){	
		HidePaging();
		
		var txtStaffId = $('#txtStaffId').val();
		var cbBranchId = $('#cbBranchId').val();
		var comboReport = $('#comboReport').val();
		var txtStartDate = $('#txtStartDate').val();
		var txtStartMonth = $('#txtStartMonth').val();
		var txtStartYear = $('#txtStartYear').val();
		var txtEndDate = $('#txtEndDate').val();
		var txtEndMonth = $('#txtEndMonth').val();
		var txtEndYear = $('#txtEndYear').val();
		
		sOffset3 = sOffset3 - 30;
		
		var targetHref="Report.jsp?" +
				"txtOffset3=" + sOffset3 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
		
		$(".Prev3").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Max3(obj)
{
	var sOffset3 = (parseInt($("#Max3Page").val()) - 1) * 30;
	var TotalRecord3 = parseInt($("#TotalRecord3").val());	
	HidePaging();
	
	var txtStaffId = $('#txtStaffId').val();
	var cbBranchId = $('#cbBranchId').val();
	var comboReport = $('#comboReport').val();
	var txtStartDate = $('#txtStartDate').val();
	var txtStartMonth = $('#txtStartMonth').val();
	var txtStartYear = $('#txtStartYear').val();
	var txtEndDate = $('#txtEndDate').val();
	var txtEndMonth = $('#txtEndMonth').val();
	var txtEndYear = $('#txtEndYear').val();
	
	var targetHref="Report.jsp?" +
				"txtOffset3=" + sOffset3 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
				
	$(".maxPageNumber3").attr("href", targetHref); 
	return true;
}

function Min3(obj)
{
	var sOffset3 = 0;
	var TotalRecord3 = parseInt($("#TotalRecord3").val());	
	HidePaging();		

	var txtStaffId = $('#txtStaffId').val();
	var cbBranchId = $('#cbBranchId').val();
	var comboReport = $('#comboReport').val();
	var txtStartDate = $('#txtStartDate').val();
	var txtStartMonth = $('#txtStartMonth').val();
	var txtStartYear = $('#txtStartYear').val();
	var txtEndDate = $('#txtEndDate').val();
	var txtEndMonth = $('#txtEndMonth').val();
	var txtEndYear = $('#txtEndYear').val();
	
	var targetHref="Report.jsp?" +
				"txtOffset3=" + sOffset3 + 
				"&txtStaffId=" + txtStaffId +
				"&cbBranchId=" + cbBranchId + 
				"&comboReport=" + comboReport + 
				"&txtStartDate=" + txtStartDate + 
				"&txtStartMonth=" + txtStartMonth + 
				"&txtStartYear=" + txtStartYear + 
				"&txtEndDate=" + txtEndDate + 
				"&txtEndMonth=" + txtEndMonth + 
				"&txtEndYear=" + txtEndYear + "";
					
	$(".minPageNumber3").attr("href", targetHref); 
	return true;
}
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
				
				<div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" >
                           		<input type = "hidden" name = "hdnReportExt" id = "hdnReportExt" value = "csv">
                           		
                           		<input type = "hidden" id="TotalRecord1" value = "<%=sTotalRecord1%>">
								<input type = "hidden" id="txtOffset1" name="txtOffset1" value="<%= sOffset1 == null ? "0" : sOffset1%>">
								<input type = "hidden" id="Max1Page" value = "<%=iMax1Page%>">
								
								<input type = "hidden" id="TotalRecord2" value = "<%=sTotalRecord2%>">
								<input type = "hidden" id="txtOffset2" name="txtOffset2" value="<%= sOffset2 == null ? "0" : sOffset2%>">
								<input type = "hidden" id="Max2Page" value = "<%=iMax2Page%>">
								
								<input type = "hidden" id="TotalRecord3" value = "<%=sTotalRecord3%>">
								<input type = "hidden" id="txtOffset3" name="txtOffset3" value="<%= sOffset3 == null ? "0" : sOffset3%>">
								<input type = "hidden" id="Max3Page" value = "<%=iMax3Page%>">
								
								<div class="form-group">
                                    <label>Start Date</label>
									<div class="row">
										<div class="col-md-3">
											<div class="input-group">
												<input class="form-control" id = "txtStartDate" name="txtStartDate" type="text" placeholder="DD" size="2" onkeypress="return isNumberKey(event)" value="<%= txtStartDate == null ? "" : txtStartDate%>" />
											</div>
										</div>
										<div class="col-md-3">
											<div class="input-group">
												<input class="form-control" id = "txtStartMonth" name="txtStartMonth" type="text" placeholder="MM" size="2" onkeypress="return isNumberKey(event)" value="<%= txtStartMonth == null ? "" : txtStartMonth%>" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<input class="form-control" id = "txtStartYear" name="txtStartYear" type="text" placeholder="YYYY" size="4" onkeypress="return isNumberKey(event)" value="<%= txtStartYear == null ? "" : txtStartYear%>" />
											</div>
										</div>
									</div>									
                                </div>
                                <div class="form-group">
                                    <label>End Date</label>
									<div class="row">
										<div class="col-md-3">
											<div class="input-group">
												<input class="form-control" id = "txtEndDate" name="txtEndDate" type="text" placeholder="DD" size="2" onkeypress="return isNumberKey(event)" value="<%= txtEndDate == null ? "" : txtEndDate%>" />
											</div>
										</div>
										<div class="col-md-3">
											<div class="input-group">
												<input class="form-control" id = "txtEndMonth" name="txtEndMonth" type="text" placeholder="MM" size="2" onkeypress="return isNumberKey(event)" value="<%= txtEndMonth == null ? "" : txtEndMonth%>" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<input class="form-control" id = "txtEndYear" name="txtEndYear" type="text" placeholder="YYYY" size="4" onkeypress="return isNumberKey(event)" value="<%= txtEndYear == null ? "" : txtEndYear%>" />
											</div>
										</div>
									</div>									
                                </div>
								 <div class="form-group">
                                    <label>Staff Id</label>
                                    <input class="form-control" id="txtStaffId" type="text" name="txtStaffId" value = "<%=txtStaffId%>" />
                                </div>
								<div class="form-group">
                                    <label>Branch</label>
                                    <select class="form-control selectpicker" data-live-search="true"  id ="cbBranchId" name ="cbBranchId">
										<option value = "">All Branch</option>
										<%if(aBranchs != null){ %>
											<%for (int i = 0; i < aBranchs.length; i++) { %>
												<option value = "<%=aBranchs[i].getBRANCHID()%>" <%=cbBranchId.equals(aBranchs[i].getBRANCHID().trim())?"selected":""%>><%=aBranchs[i].getBRANCHNAME()%></option>
											<%} %>
										<%} %>
									</select>
                                </div>
								<div class="form-group">
                                    <label>Report Type</label>
                                    <select class="form-control" id="comboReport" name ="comboReport" onchange = "CheckMode()">
										<option value = "1" <%=sReportType.equals("1")?"selected":""%>>Staff Register Report</option>
										<option value = "2" <%=sReportType.equals("2")?"selected":""%>>Transaction Attendance Report</option>
										<option value = "3" <%=sReportType.equals("3")?"selected":""%>>Device Downtime Report</option>
									</select>
                                </div>
                            </form>
                            <button class="btn btn-info" id="btnGenerate" />Generate</button>
							<button class="btn btn-info" id="btnDownload" />Download CSV</button>
							<button class="btn btn-info" id="btnDownloadPdf" />Download PDF</button>
                            <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                          </div>     
                   		</div>
                 	</div>
        		</div>
				
	            <div class="row">
	                <div class="col-md-12">
	                    <!--    Striped Rows Table  -->
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                        	Report Data
	                        </div>
	                        <div class="panel-body">
	                            <div class="table-responsive" id = "Report1Area" style = "display:<%=sReport1Display %>;">
	                                <table class="table table-striped">
	                                    <thead>
	                                        <tr>
	                                            <th>#</th>
												<th>Staff Id</th>
	                                            <th>Staff Name</th>
	                                            <th>Registered</th>
	                                            <th>Status</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    <%
											if (aReport1 != null) {
														
												for (int i = 0; i < aReport1.length; i++) {
										%>
	                                        <tr>
	                                            <td><%=Integer.parseInt(sOffset1)+i+1%></td>
												<td><%=aReport1[i].getSTAFFID()%></td>
												<td><%=aReport1[i].getSTAFFNAME()%></td>
												<td>
													<% if(Integer.parseInt(aReport1[i].getREGISTERED()) > 0) { %>
														Yes	
													<% } else { %>
														No
													<% } %>
												</td>
												<td>
													<% if(aReport1[i].getSTATUS().equals("A")) { %>
														Active	
													<% } else if(aReport1[i].getSTATUS().equals("D")) { %>
														Deleted
													<% } else { %>
														-
													<% } %>
												</td>
	                                        </tr>
	                                    <%
												}
											}
										%> 
	                                    </tbody>
	                                </table>
	                                <hr />
									<% iReportNo = 1; sTotalRecord = sTotalRecord1; iPage = iPage1; iMaxPage = iMax1Page; %>
                            		<ul class="pagination" style = "float:right;">
                            		<%if(Integer.parseInt(sTotalRecord) != 0) { %>
										<%if(iPage > 1){ %>
											<li><a href="<%=sPageName %>" class = "minPageNumber<%=iReportNo %>" onclick = "return Min<%=iReportNo %>(this);">First</a></li>
									  		<li><a href="<%=sPageName %>" class = "Prev<%=iReportNo %>" onclick="return Prev<%=iReportNo %>(this);">&laquo;</a></li>
										<% } %>
										<%if(iPage == 1){ %>
				
										<%} else if(iPage == 2){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" >1</a></li>
										<%} else { %>
										
										<%if(iPage == iMaxPage){ %>
											<% if(iPage-4 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-4; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else if(iPage == iMaxPage - 1){ %>
											<% if(iPage-3 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-3; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else { %>
											<% if(iPage-2 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-2; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} %>	
									<%} %>
									<li class="active"><a class = "currentPage<%=iReportNo %>"><%=iPage %><span class="sr-only">(current)</span></a></li>
									<%if(iPage == iMaxPage){ %>
				
									<%} else if(iPage == iMaxPage - 1){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=iMaxPage %></a></li>
									<%} else { %>
										<% if(iPage == 1){  %>
											<% if(iMaxPage < 5){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 5; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else if(iPage == 2){  %>
											<% if(iMaxPage <= 4){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 4; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else { %>
											<%for(int x = iPage + 1; x < iPage + 3; x++) { %>
												<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
											<% } %>
										<% } %>
									<%} %>		
									
									<%if(iPage < iMaxPage){ %>
											<li><a href="<%=sPageName %>" class = "Next<%=iReportNo %>" onclick = "return Next<%=iReportNo %>(this);">&raquo;</a></li>
									  		<li><a href="<%=sPageName %>" class = "maxPageNumber<%=iReportNo %>" onclick="return Max<%=iReportNo %>(this);">Last</a></li>
										<% } %>
									<%} %>
									</ul>
	                            </div>
	                            					
								<div class="table-responsive" id = "Report2Area" style = "display:<%=sReport2Display %>;">
	                                <table class="table table-striped">
	                                    <thead>
	                                        <tr>
	                                            <th>#</th>
												<th>Staff Id</th>
												<th>Staff Name</th>
												<th>Device Id</th>
												<th>Device Name</th>
												<th>Branch Id</th>
												<th>Branch Name</th>
												<th>Date</th>
												<th>Time</th>
												<th>In Out</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    <%
											if (aReport2 != null) {
														
												for (int i = 0; i < aReport2.length; i++) {
										%>
	                                        <tr>
	                                            <td><%=Integer.parseInt(sOffset2)+i+1%></td>
												<td><%=aReport2[i].getSTAFFID()%></td>
												<td><%=aReport2[i].getSTAFFNAME()%></td>
												<td><%=aReport2[i].getDEVICEID()%></td>
												<td><%=aReport2[i].getDEVICENAME()%></td>
												<td><%=aReport2[i].getBRANCHID()%></td>
												<td><%=aReport2[i].getBRANCHNAME()%></td>
												<% 
													String sAttDate = aReport2[i].getATTENDANCEDATE();
													String sAttTime = aReport2[i].getATTENDANCETIME();
												 %> 
												<td><%=sAttDate%></td>
												<td><%=sAttTime%></td>
												<td>
												<% if(aReport2[i].getINOUT().equals("A")) { %>
													IN	
												<% } else if(aReport2[i].getINOUT().equals("B")) { %>
													OUT
												<% } else { %>
													-
												<% } %>
												</td>
	                                        </tr>
	                                    <%
												}
											}
										%> 
	                                    </tbody>
	                                </table>
	                                <hr />
                            		<% iReportNo = 2; sTotalRecord = sTotalRecord2; iPage = iPage2; iMaxPage = iMax2Page; %>
                            		<ul class="pagination" style = "float:right;">
                            		<%if(Integer.parseInt(sTotalRecord) != 0) { %>
										<%if(iPage > 1){ %>
											<li><a href="<%=sPageName %>" class = "minPageNumber<%=iReportNo %>" onclick = "return Min<%=iReportNo %>(this);">First</a></li>
									  		<li><a href="<%=sPageName %>" class = "Prev<%=iReportNo %>" onclick="return Prev<%=iReportNo %>(this);">&laquo;</a></li>
										<% } %>
										<%if(iPage == 1){ %>
				
										<%} else if(iPage == 2){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" >1</a></li>
										<%} else { %>
										
										<%if(iPage == iMaxPage){ %>
											<% if(iPage-4 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-4; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else if(iPage == iMaxPage - 1){ %>
											<% if(iPage-3 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-3; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else { %>
											<% if(iPage-2 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-2; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} %>	
									<%} %>
									<li class="active"><a class = "currentPage<%=iReportNo %>"><%=iPage %><span class="sr-only">(current)</span></a></li>
									<%if(iPage == iMaxPage){ %>
				
									<%} else if(iPage == iMaxPage - 1){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=iMaxPage %></a></li>
									<%} else { %>
										<% if(iPage == 1){  %>
											<% if(iMaxPage < 5){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 5; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else if(iPage == 2){  %>
											<% if(iMaxPage <= 4){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 4; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else { %>
											<%for(int x = iPage + 1; x < iPage + 3; x++) { %>
												<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
											<% } %>
										<% } %>
									<%} %>		
									
									<%if(iPage < iMaxPage){ %>
											<li><a href="<%=sPageName %>" class = "Next<%=iReportNo %>" onclick = "return Next<%=iReportNo %>(this);">&raquo;</a></li>
									  		<li><a href="<%=sPageName %>" class = "maxPageNumber<%=iReportNo %>" onclick="return Max<%=iReportNo %>(this);">Last</a></li>
										<% } %>
									<%} %>
									</ul>
	                            </div>
								
								<div class="table-responsive" id = "Report3Area" style = "display:<%=sReport3Display %>;">
	                                <table class="table table-striped">
	                                    <thead>
	                                        <tr>
	                                            <th>#</th>
												<th>Device Id</th>
	                                            <th>Device Name</th>
	                                            <th>Date</th>
	                                            <th>Time</th>
	                                            <th>Status</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    <%
											if (aReport3 != null) {
														
												for (int i = 0; i < aReport3.length; i++) {
										%>
	                                        <tr>
	                                            <td><%=Integer.parseInt(sOffset3)+i+1%></td>
												<td><%=aReport3[i].getDEVICEID()%></td>
												<td><%=aReport3[i].getDEVICENAME()%></td>
												<% 
													String sAttDate = aReport3[i].getEVENTDATE();
													String sAttTime = aReport3[i].getEVENTTIME();
												 %> 
												<td><%=sAttDate%></td>
												<td><%=sAttTime%></td>
												<td>
												<% if(aReport3[i].getSTATUS().equals("1")){ %>
													Up
												 <% } else { %>
													Down
												 <% } %>
												</td>
	                                        </tr>
	                                    <%
												}
											}
										%> 
	                                    </tbody>
	                                </table>
	                                <hr />
                            		<% iReportNo = 3; sTotalRecord = sTotalRecord3; iPage = iPage3; iMaxPage = iMax3Page; %>
                            		<ul class="pagination" style = "float:right;">
                            		<%if(Integer.parseInt(sTotalRecord) != 0) { %>
										<%if(iPage > 1){ %>
											<li><a href="<%=sPageName %>" class = "minPageNumber<%=iReportNo %>" onclick = "return Min<%=iReportNo %>(this);">First</a></li>
									  		<li><a href="<%=sPageName %>" class = "Prev<%=iReportNo %>" onclick="return Prev<%=iReportNo %>(this);">&laquo;</a></li>
										<% } %>
										<%if(iPage == 1){ %>
				
										<%} else if(iPage == 2){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" >1</a></li>
										<%} else { %>
										
										<%if(iPage == iMaxPage){ %>
											<% if(iPage-4 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-4; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else if(iPage == iMaxPage - 1){ %>
											<% if(iPage-3 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-3; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else { %>
											<% if(iPage-2 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-2; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} %>	
									<%} %>
									<li class="active"><a class = "currentPage<%=iReportNo %>"><%=iPage %><span class="sr-only">(current)</span></a></li>
									<%if(iPage == iMaxPage){ %>
				
									<%} else if(iPage == iMaxPage - 1){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=iMaxPage %></a></li>
									<%} else { %>
										<% if(iPage == 1){  %>
											<% if(iMaxPage < 5){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 5; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else if(iPage == 2){  %>
											<% if(iMaxPage <= 4){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 4; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else { %>
											<%for(int x = iPage + 1; x < iPage + 3; x++) { %>
												<li><a href="<%=sPageName %>" class = "pageNumber<%=iReportNo %>" ><%=x %></a></li>
											<% } %>
										<% } %>
									<%} %>		
									
									<%if(iPage < iMaxPage){ %>
											<li><a href="<%=sPageName %>" class = "Next<%=iReportNo %>" onclick = "return Next<%=iReportNo %>(this);">&raquo;</a></li>
									  		<li><a href="<%=sPageName %>" class = "maxPageNumber<%=iReportNo %>" onclick="return Max<%=iReportNo %>(this);">Last</a></li>
										<% } %>
									<%} %>
									</ul>
	                            </div>
								
	                        </div>
	                    </div>
	                    <!--  End  Striped Rows Table  -->
	                </div>
	                
	            </div>
                <!-- /. ROW  -->
           

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
	<%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
