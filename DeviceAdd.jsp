<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBBRANCH"%>

<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />
<jsp:useBean id="DeviceMgr" scope="page" class="Biometrics.DeviceManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sTableTitle = "Add New Device"; %>
<% String sDataTitle = "Device Detail"; %>
<% String sPageName = "DeviceAdd.jsp"; %>
<% String sBackPageName = "DeviceList.jsp" + CurrentParam; %>
<% String sProcessPageName = "processor/CreateDevice.jsp"; %>

<%!TBUSER SessionUser; %>
<%!TBBRANCH[] aBranchs; %>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	BranchMgr.setConnection(DBMgr.getConnection());
	
	SessionUser = UserMgr.getUserById(SessionUserId);
	aBranchs = BranchMgr.getBranchAllNoOffset();
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
 
function CheckForm(theform) {

	if (!CheckDeviceId(theform)){
		return (false);
	}
	
	if (!CheckDeviceName(theform)){
		return (false);
	}
	
	if (!CheckBranch(theform)){
		return (false);
	}
	
	if (!CheckIpAddress(theform)){
		return (false);
	}
	
	if (!CheckIpPort(theform)){
		return (false);
	}
	
	return (true);
}

function CheckDeviceId(theForm){
    if (theForm.txtDeviceId.value==""){
       	alert("Please enter Device Id!");
        return (false);
    }
    return (true);
}

function CheckDeviceName(theForm){
    if (theForm.txtDeviceName.value==""){
       	alert("Please enter Device Name!");
        return (false);
    }
    return (true);
}

function CheckBranch(theForm){
    if (theForm.cbBranchId.value==""){
       	alert("Please select Branch!");
        return (false);
    }
    return (true);
}

function CheckIpAddress(theForm){
    if (theForm.txtIpAddress.value==""){
       	alert("Please enter IP Address!");
        return (false);
    }
    return (true);
}

function CheckIpPort(theForm){
    if (theForm.txtPortNo.value==""){
       	alert("Please enter IP Adress Port!");
        return (false);
    }
    return (true);
}

</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
                                <div class="form-group">
                                    <label>Device Id</label>
                                    <input class="form-control" id="txtDeviceId" type="text" name="txtDeviceId" />
                                </div>
                                <div class="form-group">
                                    <label>Device Name</label>
                                    <input class="form-control" id="txtDeviceName" type="text" name="txtDeviceName" />
                                </div>
								<div class="form-group">
                                    <label>IP Address</label>
                                    <input class="form-control" id="txtIpAddress" type="text" name="txtIpAddress" />
                                </div>
								<div class="form-group">
                                    <label>IP Address Port</label>
                                    <input class="form-control" id="txtPortNo" type="text" name="txtPortNo" />
                                </div>
								<div class="form-group">
                                    <label>Branch</label>
                                    <select class="form-control" name ="cbBranchId">
										<option value = "-">-</option>
										<%if(aBranchs != null){ %>
											<%for (int i = 0; i < aBranchs.length; i++) { %>
												<option value = "<%=aBranchs[i].getBRANCHID()%>"><%=aBranchs[i].getBRANCHNAME()%></option>
											<%} %>
										<%} %>
									</select>
                                </div>
								<div class="form-group">
                                    <label>Access</label>
									 <div class="checkbox">
										<label>
											<input type="checkbox" name = "cAllowBio" value="1">Allow Biometrics
										</label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name = "cAllowPin" value="1">Allow PIN
										</label>
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" name = "cAllowCard" value="1">Allow ACCESS CARD
										</label>
									</div>
                                </div>
                                <!-- <div class="form-group">
                                    <label>Service</label>
                                    <select class="form-control" name ="cbSvcInstance">
										<option value = "1">1</option>
										<option value = "2">2</option>
										<option value = "3">3</option>
									</select>
                                </div> -->
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
