<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBBRANCH"%>

<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />
<jsp:useBean id="AccessGroupMgr" scope="page" class="Biometrics.AccessGroupManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sTableTitle = "Add New Access Group"; %>
<% String sDataTitle = "Access Group Detail"; %>
<% String sPageName = "AccessGroupAdd.jsp"; %>
<% String sBackPageName = "AccessGroupList.jsp" + CurrentParam; %>
<% String sProcessPageName = "processor/CreateAccessGroup.jsp"; %>

<%!TBUSER SessionUser; %>
<%!TBBRANCH[] aBranchs;%>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	BranchMgr.setConnection(DBMgr.getConnection());
		
	aBranchs = BranchMgr.getBranchAllNoOffset();

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
	
	
    $("#selectAll").change( function() {
    	var status = this.checked;
        $(".cBranches").each( function() {
        	 this.checked = status
        });
    });
    
});

function CheckForm(theform) {

	if (!CheckAccessGroupId(theform)){
		return (false);
	}
	
	if (!CheckAccessGroupName(theform)){
		return (false);
	}
	
	if (!CheckBranches()){
		return (false);
	}
	
	return (true);
}

function CheckAccessGroupId(theForm){
    if (theForm.txtAccessGroupId.value==""){
       	alert("Please enter AccessGroup Id!");
        return (false);
    }
    return (true);
}

function CheckAccessGroupName(theForm){
    if (theForm.txtAccessGroupName.value==""){
       	alert("Please enter AccessGroup Name!");
        return (false);
    }
    return (true);
}

function CheckBranches(){
	if($('input:checked').length == '0'){
		alert('Please choose any branches.');	
		return (false);
	}
    return (true);
}

</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
                                <div class="form-group">
                                    <label>Access Group Id</label>
                                    <input class="form-control" id="txtAccessGroupId" type="text" name="txtAccessGroupId" />
                                </div>
                                <div class="form-group">
                                    <label>Access Group Name</label>
                                    <input class="form-control" id="txtAccessGroupName" type="text" name="txtAccessGroupName" />
                                </div>
                                <div class="form-group">
                                	<div class="checkbox">
										<label>
											<input type="checkbox" id = "selectAll"> Select All Branch
										</label>
									</div>
                                    <label>Branch Id</label>
									<%if(aBranchs != null){ %>
										<%for (int i = 0; i < aBranchs.length; i++) { %>
										 <div class="checkbox">
											<label>
												<input type="checkbox" class = "cBranches" name = "cBranches" value="<%=aBranchs[i].getBRANCHID()%>"><%=aBranchs[i].getBRANCHNAME()%>
											</label>
										</div>
										<%} %>
									<%} %>
                                </div>
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
