<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<% String ServerDescription = request.getSession().getServletContext().getInitParameter("ServerDescription"); %>
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />
<%
DBMgr.setDBUserId(request.getSession().getServletContext().getInitParameter("DatabaseUser"));
DBMgr.setDBPassword(request.getSession().getServletContext().getInitParameter("DatabasePassword"));
DBMgr.setDBUrl(request.getSession().getServletContext().getInitParameter("DatabaseURL"));
%>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!-- <html xmlns="http://www.w3.org/1999/xhtml"> -->
<html>
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%=ServerDescription%></title>
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' /> -->
    
    <link href="assets/css/basic.css" rel="stylesheet" />
    <link href="assets/css/custom.css" rel="stylesheet" />
    
    <script src="assets/js/jquery-1.10.2.js"></script>
    <script src="assets/js/common.js"></script>
</head>