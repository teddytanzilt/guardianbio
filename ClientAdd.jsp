<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBAREA"%>

<jsp:useBean id="AreaMgr" scope="page" class="Biometrics.AreaManager" />
<jsp:useBean id="ClientMgr" scope="page" class="Biometrics.ClientManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sTableTitle = "Add New Client"; %>
<% String sDataTitle = "Client Detail"; %>
<% String sPageName = "ClientAdd.jsp"; %>
<% String sBackPageName = "ClientList.jsp" + CurrentParam; %>
<% String sProcessPageName = "processor/CreateClient.jsp"; %>

<%!TBUSER SessionUser; %>
<%!TBAREA[] aAreas;%>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	AreaMgr.setConnection(DBMgr.getConnection());
	
	SessionUser = UserMgr.getUserById(SessionUserId);
	aAreas = AreaMgr.getAreaAllNoOffset();
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
 
function CheckForm(theform) {

	if (!CheckClientIp(theform)){
		return (false);
	}
	
	if (!CheckClientName(theform)){
		return (false);
	}
	
	if (!CheckArea(theform)){
		return (false);
	}
	
	return (true);
}

function CheckClientIp(theForm){
    if (theForm.txtClientIp.value==""){
       	alert("Please enter Client Ip!");
        return (false);
    }
    return (true);
}

function CheckClientName(theForm){
    if (theForm.txtClientName.value==""){
       	alert("Please enter Client Name!");
        return (false);
    }
    return (true);
}

function CheckArea(theForm){
    if (theForm.cbAreaId.value==""){
       	alert("Please select Area!");
        return (false);
    }
    return (true);
}
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
                                <div class="form-group">
                                    <label>Client Ip</label>
                                    <input class="form-control" id="txtClientIp" type="text" name="txtClientIp" />
                                </div>
                                <div class="form-group">
                                    <label>Client Name</label>
                                    <input class="form-control" id="txtClientName" type="text" name="txtClientName" />
                                </div>
								<div class="form-group">
                                    <label>Area Id</label>
                                    <select class="form-control" name ="cbAreaId">
										<option value = "-">-</option>
										<%if(aAreas != null){ %>
											<%for (int i = 0; i < aAreas.length; i++) { %>
												<option value = "<%=aAreas[i].getAREAID()%>"><%=aAreas[i].getAREANAME()%></option>
											<%} %>
										<%} %>
									</select>
                                </div>
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
