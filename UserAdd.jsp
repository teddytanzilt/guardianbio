<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sTableTitle = "Add New User"; %>
<% String sDataTitle = "User Detail"; %>
<% String sPageName = "UserAdd.jsp"; %>
<% String sBackPageName = "UserList.jsp" + CurrentParam; %>
<% String sProcessPageName = "processor/CreateUser.jsp"; %>

<%!TBUSER SessionUser; %>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		window.location = "<%=sBackPageName %>";
	});
});
function CheckForm(theform) {

	if (!CheckUserName(theform)){
		return (false);
	}
	
	if (!CheckName(theform)){
		return (false);
	}
	
	if (!CheckPassword(theform)){
		return (false);
	}

	if (!CheckConfirmPassword(theform)){
		return (false);
	}

	if (!CheckAndComparePassword(theform)){
	     return (false);
	  }
	  
	return (true);
}

function CheckAndComparePassword(theForm){
    if (theForm.txtPassword.value!=theForm.txtConfirmPassword.value){
       alert("Password must be same as confirm password!");
        return (false);
    }
    return (true);
}

function CheckUserName(theForm){
    if (theForm.txtUserName.value==""){
       	alert("Please enter User Name!");
        return (false);
    }
    return (true);
}

function CheckName(theForm){
    if (theForm.txtName.value==""){
       	alert("Please enter Name!");
        return (false);
    }
    return (true);
}

function CheckPassword(theForm){
    if (theForm.txtPassword.value==""){
       	alert("Please enter Password!");
        return (false);
    }
    return (true);
}

function CheckConfirmPassword(theForm){
    if (theForm.txtConfirmPassword.value==""){
       	alert("Please enter Confirm Password!");
        return (false);
    }
    return (true);
}

</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
                                <div class="form-group">
                                    <label>User Name</label>
                                    <input class="form-control" id="txtUserName" type="text" name="txtUserName" />
                                </div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" id="txtName" type="text" name="txtName" />
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" id="txtPassword" type="password" name="txtPassword" />
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input class="form-control" id="txtConfirmPassword" type="password" name="txtConfirmPassword" />
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
									<select class="form-control" name ="cbRole">
										<option value = "NORMAL">NORMAL</option>
										<option value = "ADMIN">ADMIN</option>
									</select>
                                </div>
                                <button type="submit" class="btn btn-info">Save </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>