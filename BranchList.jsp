<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBBRANCH"%>
<%@page import="util.DateFormatConverter"%>

<jsp:useBean id="AreaMgr" scope="page" class="Biometrics.AreaManager" />
<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<%
String CurrentParam = "?"+request.getQueryString();
request.getSession().setAttribute("CurrentParam", CurrentParam);
%>

<% String sTableTitle = "Branch Management"; %>
<% String sPageName = "BranchList.jsp"; %>
<% String sAddPageName = "BranchAdd.jsp"; %>
<% String sEditPageName = "BranchEdit.jsp"; %>
<% String sDeletePageName = "processor/DeleteBranch.jsp"; %>

<%!Biometrics.TBBRANCH[] aBranchs;%>
<%!Biometrics.TBAREA[] aAreas;%>
<%!String sBranchId; %>
<%!String sBranchName; %>
<%!String sAreaId; %>
<%!String sTotalRecord; %>
<%!String sOffset; %>
<%!int iPage; %>
<%!int iMaxPage; %>
<%!TBUSER SessionUser; %>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}

	UserMgr.setConnection(DBMgr.getConnection());
	AreaMgr.setConnection(DBMgr.getConnection());
	BranchMgr.setConnection(DBMgr.getConnection());
	
	SessionUser = UserMgr.getUserById(SessionUserId);
	aAreas = AreaMgr.getAreaAllNoOffset();
	sBranchId = request.getParameter("txtBranchId");
	sBranchName = request.getParameter("txtBranchName");
	sAreaId = request.getParameter("cbAreaId");
	sOffset = request.getParameter("txtOffset");
	
	if (sBranchId == null || sBranchId.trim().length() <= 0) {
		sBranchId = "";		
	}
	if (sBranchName == null || sBranchName.trim().length() <= 0) {
		sBranchName = "";		
	}
	if (sAreaId == null || sAreaId.trim().length() <= 0) {
		sAreaId = "";		
	}
	if (sOffset == null || sOffset.trim().length() <= 0) {
		sOffset = "0";		
	}
	
	if (sAreaId != "" || sBranchName != "") {
		aBranchs = BranchMgr.getBranchByFilter(sBranchId,sBranchName,sAreaId,sOffset);  
		sTotalRecord = BranchMgr.countBranchByFilter(sBranchId,sBranchName,sAreaId) + "";
	} else {
		aBranchs = BranchMgr.getBranchAll(sOffset);
		sTotalRecord = BranchMgr.countBranchAll() + "";
	}
	
	iPage = (Integer.parseInt(sOffset) / 30) + 1;
	if(Integer.parseInt(sTotalRecord) % 30 != 0){
		iMaxPage = (Integer.parseInt(sTotalRecord) / 30) + 1;
	} else {
		iMaxPage = (Integer.parseInt(sTotalRecord) / 30);
	}
	
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
}
%>

<script language="javascript" type="text/javascript">
$(function() {
    $(".pageNumber").click(function (){
    	var sNextOffset = (parseInt($(this).html()) - 1) * 30;
    	var TotalRecord = parseInt($("#TotalRecord").val());
    	
    	var sBranchId = $('#txtBranchId').val();
		var sUserName = $('#txtBranchName').val();
		var sAreaId = $('#cbAreaId').val();
		
		HidePaging();
    	$(this).attr("href", "<%=sPageName%>?txtBranchId=" + sBranchId + "&txtBranchName=" + sUserName + "&cbAreaId=" + sAreaId + "&txtOffset=" + sNextOffset + "");    	
     });
    
 });
 
function HidePaging(){
	$(".pageNumber").css("display","none");	
	$(".Next").css("display","none");	
	$(".Prev").css("display","none");	
	$(".currentPage").css("display","none");
	$(".minPageNumber").css("display","none");
	$(".maxPageNumber").css("display","none");
}
 
function IsConfirmedDelete(obj)
{
	return confirm("Are you sure want to delete?");	
}
function IsConfirmedImport(obj)
{
	return confirm("Are you sure want to import?");	
}
function IsConfirmedSearch(obj)
{	
	$("#thisForm").attr("action", "<%=sPageName%>");	
	$("#txtOffset").val("0");
	$("#thisForm").submit();
	return true;
}

function Next(obj)
{	
	var nextLink = document.getElementById("Next");
	var prevLink = document.getElementById("Prev");
		
	var sOffset = parseInt($("#txtOffset").val());
	var TotalRecord = parseInt($("#TotalRecord").val());	
	if(sOffset+10 <=  TotalRecord){
		HidePaging();
		
		var sBranchId = $('#txtBranchId').val();
		var sUserName = $('#txtBranchName').val();
		var sAreaId = $('#cbAreaId').val();
		
		sOffset = sOffset + 30;

		var targetHref="<%=sPageName%>?txtBranchId=" + sBranchId + "&txtBranchName=" + sUserName + "&cbAreaId=" + sAreaId + "&txtOffset=" + sOffset + "";
		$(".Next").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Prev(obj)
{	
	var nextLink = document.getElementById("Next");
	var prevLink = document.getElementById("Prev");
	
	var sOffset = parseInt($("#txtOffset").val());
	if(sOffset > 0){	
		HidePaging();
		
		var sBranchId = $('#txtBranchId').val();
		var sUserName = $('#txtBranchName').val();	
		var sAreaId = $('#cbAreaId').val();
		sOffset = sOffset - 30;

		var targetHref="<%=sPageName%>?txtBranchId=" + sBranchId + "&txtBranchName=" + sUserName + "&cbAreaId=" + sAreaId + "&txtOffset=" + sOffset + "";
		$(".Prev").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Max(obj)
{
	var sOffset = (parseInt($("#MaxPage").val()) - 1) * 30;
	var TotalRecord = parseInt($("#TotalRecord").val());	
	HidePaging();		
	var sBranchId = $('#txtBranchId').val();
	var sUserName = $('#txtBranchName').val();
	var sAreaId = $('#cbAreaId').val();
	var targetHref="<%=sPageName%>?txtBranchId=" + sBranchId + "&txtBranchName=" + sUserName + "&cbAreaId=" + sAreaId + "&txtOffset=" + sOffset + "";
	$(".maxPageNumber").attr("href", targetHref); 
	return true;
}

function Min(obj)
{
	var TotalRecord = parseInt($("#TotalRecord").val());	
	HidePaging();		
	var sBranchId = $('#txtBranchId').val();
	var sUserName = $('#txtBranchName').val();	
	var sAreaId = $('#cbAreaId').val();
	var targetHref="<%=sPageName%>?txtBranchId=" + sBranchId + "&txtBranchName=" + sUserName + "&cbAreaId=" + sAreaId + "&txtOffset=0";
	$(".minPageNumber").attr("href", targetHref); 
	return true;
}
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
				
	            <div class="row">
	                <div class="col-md-12">
	                    <!--    Striped Rows Table  -->
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                        	<Form id = "thisForm">
	                        	<input type = "hidden" id = "TotalRecord" value = "<%=sTotalRecord%>">
								<input type = "hidden" id = "txtOffset" name="txtOffset"  value="<%= sOffset == null ? "0" : sOffset%>">
								<input type = "hidden" id = "MaxPage" value = "<%=iMaxPage%>">
								<input type = "hidden" id = "txtBranchId" value = "">
								
	                            <div class="row">
									<div class="col-md-3">
			                            <div class="input-group">
			                            	<select class="form-control" name ="cbAreaId" id = "cbAreaId">
												<option value = "">-</option>
												<%if(aAreas != null){ %>
													<%for (int i = 0; i < aAreas.length; i++) { %>
														<option value = "<%=aAreas[i].getAREAID() %>" <%=sAreaId.equals(aAreas[i].getAREAID())?"selected":""%>>
															<%=aAreas[i].getAREANAME()%>
														</option>
													<%} %>
												<%} %>
											</select>
		                                </div>
		                            </div>
                    				<div class="col-md-3">
			                            <div class="input-group">
			                            	<input class="form-control" id = "txtBranchName" name="txtBranchName" type="text" placeholder="Branch Name"  value="<%= sBranchName == null ? "" : sBranchName%>" />
		                                </div>
		                            </div>
		                            <div class="col-md-1">
			                            <div class="input-group">
		                                    <span class="input-group-btn">
		                                        <button class="btn btn-success" id = "btnSearch" type="button" onclick="return IsConfirmedSearch(this);">Search</button>
		                                    </span>
		                                </div>
		                            </div>
		                      	</div>
		                      	</Form>
	                        </div>
	                        <div class="panel-body">
	                            <div class="table-responsive">
	                                <table class="table table-striped">
	                                    <thead>
	                                        <tr>
	                                            <th>#</th>
												<th>Branch Id</th>
	                                            <th>Branch Name</th>
												<th>Area</th>
	                                            <th>Created On</th>
	                                            <th>Modified On</th>
	                                            <th>Action</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    <%
											if (aBranchs != null) {
														
												for (int i = 0; i < aBranchs.length; i++) {
										%>
	                                        <tr>
	                                            <td><%=Integer.parseInt(sOffset)+i+1%></td>
												<td><%=aBranchs[i].getBRANCHID()%></td>
												<td><%=aBranchs[i].getBRANCHNAME() != null ? aBranchs[i].getBRANCHNAME() : "&nbsp;" %></td>
												<td><%=aBranchs[i].getAREANAME() != null ? aBranchs[i].getAREANAME() : "&nbsp;" %></td>
												<td><%=aBranchs[i].getCREATEDSTAMP() != null ? DateFormatConverter.format2(aBranchs[i].getCREATEDSTAMP(), "yyyy-MM-dd HH:mm:ss") : "&nbsp;" %></td>
												<td><%=aBranchs[i].getLASTUPDATETIMEDSTAMP() != null ? DateFormatConverter.format2(aBranchs[i].getLASTUPDATETIMEDSTAMP(), "yyyy-MM-dd HH:mm:ss") : "&nbsp;" %></td>
												<td >
													<a href="<%=sEditPageName %>?Id=<%=aBranchs[i].getID()%>">Edit</a> 	&nbsp; 	&nbsp;
													<a href="<%=sDeletePageName %>?Id=<%=aBranchs[i].getID()%>" onclick="return IsConfirmedDelete(this);">Delete</a>
												</td>
	                                        </tr>
	                                    <%
												}
											}
										%> 
	                                    </tbody>
	                                </table>
	                                <hr />
                            		<ul class="pagination" style = "float:right;">
                            		<%if(Integer.parseInt(sTotalRecord) != 0) { %>
										<%if(iPage > 1){ %>
											<li><a href="<%=sPageName %>" class = "minPageNumber" onclick = "return Min(this);">First</a></li>
									  		<li><a href="<%=sPageName %>" class = "Prev" onclick="return Prev(this);">&laquo;</a></li>
										<% } %>
										<%if(iPage == 1){ %>
				
										<%} else if(iPage == 2){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber" >1</a></li>
										<%} else { %>
										
										<%if(iPage == iMaxPage){ %>
											<% if(iPage-4 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-4; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else if(iPage == iMaxPage - 1){ %>
											<% if(iPage-3 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-3; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else { %>
											<% if(iPage-2 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-2; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} %>	
									<%} %>
									<li class="active"><a class = "currentPage"><%=iPage %><span class="sr-only">(current)</span></a></li>
									<%if(iPage == iMaxPage){ %>
				
									<%} else if(iPage == iMaxPage - 1){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber" ><%=iMaxPage %></a></li>
									<%} else { %>
										<% if(iPage == 1){  %>
											<% if(iMaxPage < 5){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 5; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else if(iPage == 2){  %>
											<% if(iMaxPage <= 4){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 4; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else { %>
											<%for(int x = iPage + 1; x < iPage + 3; x++) { %>
												<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
											<% } %>
										<% } %>
									<%} %>		
									
									<%if(iPage < iMaxPage){ %>
											<li><a href="<%=sPageName %>" class = "Next" onclick = "return Next(this);">&raquo;</a></li>
									  		<li><a href="<%=sPageName %>" class = "maxPageNumber" onclick="return Max(this);">Last</a></li>
										<% } %>
									<%} %>
									</ul>
	                            
	                            	<a class="btn btn-info" style = "float:left;"  href="<%=sAddPageName %>">Add New</a>
	                            </div>
	                            
	                            
	                            
	                        </div>
	                    </div>
	                    <!--  End  Striped Rows Table  -->
	                </div>
	                
	            </div>
                <!-- /. ROW  -->
           

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
	<%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>