<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "UserList.jsp" + CurrentParam; %>
<% String sBackPageName = "UserAdd.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		
		UserMgr.setConnection(DBMgr.getConnection());
		
		TBUSER sUser = new TBUSER();
		TBUSER SessionUser = new TBUSER();
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		String txtUserName = request.getParameter("txtUserName").toString().trim();;
		String txtPassword = request.getParameter("txtPassword").toString().trim();;
		String txtName = request.getParameter("txtName").toString().trim();;
		String cbRole = request.getParameter("cbRole").toString().trim();
		
		sUser.setUSERNAME(txtUserName);
		sUser.setPASSWORD(txtPassword);
		sUser.setNAME(txtName);
		sUser.setROLE(cbRole);
		sUser.setCREATEDBY(SessionUser.getUSERNAME());
		
		boolean addStatus = UserMgr.addUser(sUser);
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (addStatus)  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		else{
			request.getSession().setAttribute("LastErrMsg", UserMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
%>