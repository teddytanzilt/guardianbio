<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.OutputStream"%>

<jsp:useBean id="AttendanceMgr" scope="page" class="Biometrics.AttendanceManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String sSuccessPageName = "Export.jsp"; %>
<% String sBackPageName = "Export.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		
		String txtStaffId = String.valueOf(request.getParameter("txtStaffId")).trim();
		String cbBranchId = String.valueOf(request.getParameter("cbBranchId")).trim();
		
		String txtStartDate = String.valueOf(request.getParameter("txtStartDate")).trim();
		String txtStartMonth = String.valueOf(request.getParameter("txtStartMonth")).trim();
		String txtStartYear = String.valueOf(request.getParameter("txtStartYear")).trim();
		String txtEndDate = String.valueOf(request.getParameter("txtEndDate")).trim();
		String txtEndMonth = String.valueOf(request.getParameter("txtEndMonth")).trim();
		String txtEndYear = String.valueOf(request.getParameter("txtEndYear")).trim();
		
		String Start =  txtStartYear + txtStartMonth + txtStartDate;
		String End =  txtEndYear + txtEndMonth + txtEndDate;
		
		AttendanceMgr.setConnection(DBMgr.getConnection());
		
		String masterPath = request.getSession().getServletContext().getInitParameter("ExportOutputLocation");  	
	 	String outputName = AttendanceMgr.ExportAttendance(masterPath, Start, End, txtStaffId, cbBranchId);
	 
	 	if (outputName != "") {		
			String filePath = masterPath + outputName;
			File downloadFile = new File(filePath);
			FileInputStream inStream = new FileInputStream(downloadFile);

			ServletContext context = getServletContext();
			String mimeType = context.getMimeType(filePath);
			if (mimeType == null) {        
				 mimeType = "application/octet-stream";
			}
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			OutputStream outStream = response.getOutputStream();
			byte[] buffer = new byte[4096];
			int bytesRead = -1;
			while ((bytesRead = inStream.read(buffer)) != -1) {
			 outStream.write(buffer, 0, bytesRead);
			}
			inStream.close();
			outStream.close();      

			if (DBMgr.getConnection() != null){
				if (!DBMgr.getConnection().isClosed())
					DBMgr.closeConnection();
			}
		} else{ 
			if(AttendanceMgr.getErrorMessage() == null){
				if (DBMgr.getConnection() != null){
					if (!DBMgr.getConnection().isClosed())
						DBMgr.closeConnection();
				}
				request.getSession().setAttribute("LastErrMsg", "No data Generated");
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			} else {
				if (DBMgr.getConnection() != null){
					if (!DBMgr.getConnection().isClosed())
						DBMgr.closeConnection();
				}
				request.getSession().setAttribute("LastErrMsg", AttendanceMgr.getErrorMessage());
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			}
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	} 	
%>