<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBAREA"%>
<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="AreaMgr" scope="page" class="Biometrics.AreaManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "AreaList.jsp" + CurrentParam; %>
<% String sBackPageName = "AreaAdd.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		TBAREA sArea = new TBAREA();
		TBUSER SessionUser = new TBUSER();
	
		AreaMgr.setConnection(DBMgr.getConnection());
		UserMgr.setConnection(DBMgr.getConnection());	
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		String txtAreaId = request.getParameter("txtAreaId").toString().trim();
		String txtAreaName = request.getParameter("txtAreaName").toString().trim();
		
		sArea.setAREAID(txtAreaId);
		sArea.setAREANAME(txtAreaName);
		sArea.setCREATEDBY(SessionUser.getUSERNAME());
		
		boolean addStatus = AreaMgr.addArea(sArea);
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
			
		if (addStatus)  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		else{
			request.getSession().setAttribute("LastErrMsg", AreaMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
%>