<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />
<jsp:useBean id="UsrMgr" scope="page" class="Biometrics.UserManager" />

<%!String userId;%>
<%!String password;%>

<%	
	if (DBMgr.getConnection() == null) {
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}

	userId = request.getParameter("txtUserId");
	password = request.getParameter("txtPassword");

	UsrMgr.setConnection(DBMgr.getConnection());

	TBUSER oUser = UsrMgr.getUser(userId, password);

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
	
	if (oUser == null) {
		request.getSession().setAttribute("LastErrMsg", "Invalid username or password");
		response.sendRedirect(request.getContextPath()
				+ "/Login.jsp");
		return;
	}
	
	//Set Session
	request.getSession().setAttribute("SessionLoginID", oUser.getID());
	request.getSession().setAttribute("SessionLoginUSERNAME", oUser.getUSERNAME());
	request.getSession().setAttribute("SessionLoginROLE", oUser.getROLE());
	response.sendRedirect(request.getContextPath() + "/UserList.jsp");	
%>

