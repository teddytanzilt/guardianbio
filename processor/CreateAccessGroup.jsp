<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBACCESSGROUPDETAIL"%>
<%@page import="Biometrics.TBACCESSGROUP"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="AccessGroupMgr" scope="page" class="Biometrics.AccessGroupManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "AccessGroupList.jsp" + CurrentParam; %>
<% String sBackPageName = "AccessGroupAdd.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		TBACCESSGROUP sAccessGroup = new TBACCESSGROUP();
		TBUSER SessionUser = new TBUSER();
	
		AccessGroupMgr.setConnection(DBMgr.getConnection());
		UserMgr.setConnection(DBMgr.getConnection());	
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		String txtAccessGroupId = request.getParameter("txtAccessGroupId").toString().trim();
		String txtAccessGroupName = request.getParameter("txtAccessGroupName").toString().trim();
		String[] cBranches = request.getParameterValues("cBranches");
	
		sAccessGroup.setACCESSGROUPID(txtAccessGroupId);
		sAccessGroup.setACCESSGROUPNAME(txtAccessGroupName);
		sAccessGroup.setCREATEDBY(SessionUser.getUSERNAME());
		
		boolean addStatus = false;
		boolean serviceError = false;
		
		addStatus = AccessGroupMgr.addAccessGroup(sAccessGroup);
		if(addStatus){
			for(int i=0; i<cBranches.length; i++){
				TBACCESSGROUPDETAIL sAccessGroupDetail = new TBACCESSGROUPDETAIL();
				sAccessGroupDetail.setACCESSGROUPID(txtAccessGroupId);
				sAccessGroupDetail.setBRANCHID(cBranches[i].toString().trim());
				boolean insertStatus = AccessGroupMgr.addAccessGroupDetail(sAccessGroupDetail);
	    	}
		}
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
			
		if (addStatus) { 
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		} else {
			request.getSession().setAttribute("LastErrMsg", AccessGroupMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName);	
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
%>