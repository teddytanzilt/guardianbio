<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBSTAFF"%>
<%@page import="java.io.*"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="StaffMgr" scope="page" class="Biometrics.StaffManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "StaffList.jsp" + CurrentParam; %>
<% String sBackPageName = "StaffEdit.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
	
	int sId = 0;	
	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		
		UserMgr.setConnection(DBMgr.getConnection());
		StaffMgr.setConnection(DBMgr.getConnection());	
		
		TBUSER SessionUser = new TBUSER();
		SessionUser = UserMgr.getUserById(SessionUserId);
		sId = Integer.parseInt(String.valueOf(request.getParameter("hdnId")));
		String txtStaffId = request.getParameter("txtStaffId").toString().trim();
		String txtStaffName = request.getParameter("txtStaffName").toString().trim();
		String cbAccessGroupId = request.getParameter("cbAccessGroupId").toString().trim();
		String txtCardNo = request.getParameter("txtCardNo").toString().trim();
		String cbPrivilege = request.getParameter("cbPrivilege").toString().trim();
		String txtPassword = request.getParameter("txtPassword").toString().trim();
		
		TBSTAFF oldStaff = StaffMgr.getStaffById(sId);
		
		TBSTAFF currentStaff = StaffMgr.getStaffById(sId);
		String oldAccessGroupId = currentStaff.getACCESSGROUPID();
		currentStaff.setSTAFFID(txtStaffId);
		currentStaff.setSTAFFNAME(txtStaffName);
		currentStaff.setPRIVILEGE(cbPrivilege);
		currentStaff.setACCESSGROUPID(cbAccessGroupId);
		currentStaff.setCARDNO(txtCardNo);
		currentStaff.setPASSWORD(txtPassword);
		currentStaff.setLASTUPDATETIMEDBY(SessionUser.getUSERNAME());
		
		boolean changeAccessGroup = false;
		if(oldAccessGroupId.equals(cbAccessGroupId) == false){			
			changeAccessGroup = true;
		}
		
		String resp = "";
		boolean updateStatus = false;
		boolean serviceError = false;
		
		updateStatus = StaffMgr.updateStaff(currentStaff);
		if (updateStatus){
			if(changeAccessGroup){
				resp = DeviceControl.transferStaff(currentStaff.getSTAFFID(), oldAccessGroupId, cbAccessGroupId);
			} else {
				resp = DeviceControl.enrollStaff(currentStaff.getSTAFFID());	
			}
		
			if(resp == "OK" || resp.equals("OK")) {
				
			} else {
				serviceError = true;
				updateStatus = false;
			}
		}
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (updateStatus) {  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		} else {
			if(serviceError){
				request.getSession().setAttribute("LastErrMsg", resp);
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
			} else {
				request.getSession().setAttribute("LastErrMsg", StaffMgr.getErrorMessage());
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);	
			}
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
	} 
%>