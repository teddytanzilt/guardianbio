<%@page import="Biometrics.DeviceControl"%>
<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBDEVICE"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="java.io.*"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="DeviceMgr" scope="page" class="Biometrics.DeviceManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "DeviceList.jsp" + CurrentParam; %>
<% String sBackPageName = "DeviceAdd.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}

		TBDEVICE sDevice = new TBDEVICE();
		TBUSER SessionUser = new TBUSER();

		DeviceMgr.setConnection(DBMgr.getConnection());
		UserMgr.setConnection(DBMgr.getConnection());	
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		String txtDeviceId = request.getParameter("txtDeviceId").toString().trim();
		String txtDeviceName = request.getParameter("txtDeviceName").toString().trim();
		String cbBranchId = request.getParameter("cbBranchId").toString().trim();
		String txtIpAddress = request.getParameter("txtIpAddress").toString().trim();
		String txtPortNo = request.getParameter("txtPortNo").toString().trim();
		String cAllowBio = request.getParameter("cAllowBio");
		String cAllowPin = request.getParameter("cAllowPin");
		String cAllowCard = request.getParameter("cAllowCard");
		//String cbSvcInstance = request.getParameter("cbSvcInstance"); 
		String cbSvcInstance = "0";
		cbSvcInstance = DeviceMgr.getSvcInstance();
		
		if(cAllowBio == null){ cAllowBio = "0"; }
		if(cAllowPin == null){ cAllowPin = "0"; }
		if(cAllowCard == null){ cAllowCard = "0"; }
		
		sDevice.setDEVICEID(txtDeviceId);
		sDevice.setDEVICENAME(txtDeviceName);
		sDevice.setBRANCHID(cbBranchId);
		sDevice.setIPADDRESS(txtIpAddress);
		sDevice.setPORTNO(txtPortNo);
		sDevice.setALLOWBIOMETRIC(cAllowBio);
		sDevice.setALLOWPIN(cAllowPin);
		sDevice.setALLOWACCESSCARD(cAllowCard);
		sDevice.setCREATEDBY(SessionUser.getUSERNAME());
		sDevice.setSvcInstance(cbSvcInstance);
		
		String resp = "";
		boolean addStatus = false;
		boolean serviceError = false;
		
		if(cbSvcInstance != "0"){
			if(DeviceMgr.checkSvcInstance(cbSvcInstance)){
				addStatus = DeviceMgr.addDevice(sDevice);
				if(addStatus){
					
					resp = DeviceControl.addDevice(txtDeviceId,txtIpAddress,txtPortNo,cbBranchId);
					
					if(resp == "OK" || resp.equals("OK")) {
						//addStatus = DeviceMgr.addDevice(sDevice);
					} else {
						serviceError = true;
						addStatus = false;
						TBDEVICE newDevice = DeviceMgr.getDeviceByDeviceId(txtDeviceId);
						DeviceMgr.deleteDevice(newDevice.getID());
					}
				}
			} else {
				serviceError = true;
				resp = "Instance Full";
			}	
		} else {
			serviceError = true;
			resp = "All Instance Full";
		}
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
			
		if (addStatus) {  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		} else {
			if(serviceError){
				request.getSession().setAttribute("LastErrMsg", resp);
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			} else {
				request.getSession().setAttribute("LastErrMsg", DeviceMgr.getErrorMessage());
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			}
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}

%>