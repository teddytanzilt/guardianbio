<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="util.ProcessUtil"%>
<%@page import="java.io.*"%>

<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />
<jsp:useBean id="ProcessUtl" scope="session" class="util.ProcessUtil" />

<%
	if(request.getSession().getAttribute("SessionLoginID") == null){
		request.getSession().setAttribute("LastErrMsg", "Failed to get session");
		response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp");
		return;
	}
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp");
				return;
			}
		}
	
		int sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
		
		String syncProgName = request.getSession().getServletContext().getInitParameter("SyncProgName");
		String syncProgLocation = request.getSession().getServletContext().getInitParameter("SyncProgLocation");
		
		Process process = Runtime.getRuntime().exec(syncProgLocation + syncProgName + " " + sId + " ");		
		while(ProcessUtl.CheckRunning(process)){
			
			
		}
		
		BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line = "";
		while ((line = input.readLine()) != null) {
		  	
		}	
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		response.sendRedirect(request.getContextPath() + "/StaffList.jsp");		
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/StaffList.jsp");
	}
%>