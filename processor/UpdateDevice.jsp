<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBDEVICE"%>
<%@page import="java.io.*"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="DeviceMgr" scope="page" class="Biometrics.DeviceManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "DeviceList.jsp" + CurrentParam; %>
<% String sBackPageName = "DeviceEdit.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
	
	int sId = 0;
	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		
		UserMgr.setConnection(DBMgr.getConnection());
		DeviceMgr.setConnection(DBMgr.getConnection());	
		
		TBUSER SessionUser = new TBUSER();
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		sId = Integer.parseInt(String.valueOf(request.getParameter("hdnId")));
		String txtDeviceId = request.getParameter("txtDeviceId").toString().trim();
		String txtDeviceName = request.getParameter("txtDeviceName").toString().trim();
		String cbBranchId = request.getParameter("cbBranchId").toString().trim();
		String txtIpAddress = request.getParameter("txtIpAddress").toString().trim();
		String txtPortNo = request.getParameter("txtPortNo").toString().trim();
		String cAllowBio = request.getParameter("cAllowBio");
		String cAllowPin = request.getParameter("cAllowPin");
		String cAllowCard = request.getParameter("cAllowCard");
		//String cbSvcInstance = request.getParameter("cbSvcInstance");
		
		if(cAllowBio == null){ cAllowBio = "0"; }
		if(cAllowPin == null){ cAllowPin = "0"; }
		if(cAllowCard == null){ cAllowCard = "0"; }
		
		TBDEVICE oldDevice = DeviceMgr.getDeviceById(sId);
		
		TBDEVICE currentDevice = DeviceMgr.getDeviceById(sId);
		String oldBranchId = currentDevice.getBRANCHID();		
		String cbSvcInstance = currentDevice.getSvcInstance();
		
		currentDevice.setDEVICEID(txtDeviceId);
		currentDevice.setDEVICENAME(txtDeviceName);
		currentDevice.setBRANCHID(cbBranchId);
		currentDevice.setIPADDRESS(txtIpAddress);
		currentDevice.setPORTNO(txtPortNo);
		currentDevice.setALLOWBIOMETRIC(cAllowBio);
		currentDevice.setALLOWPIN(cAllowPin);
		currentDevice.setALLOWACCESSCARD(cAllowCard);
		currentDevice.setLASTUPDATETIMEDBY(SessionUser.getUSERNAME());
		currentDevice.setSvcInstance(cbSvcInstance);
		
		String resp = "";
		boolean updateStatus = false;
		boolean serviceError = false;
		
		resp = DeviceControl.deleteDevice(txtDeviceId);
		if(resp == "OK" || resp.equals("OK")) {
			updateStatus = DeviceMgr.updateDevice(currentDevice);
			if(updateStatus){
				resp = DeviceControl.addDevice(txtDeviceId, txtIpAddress, txtPortNo, cbBranchId);
				if(resp == "OK" || resp.equals("OK")) {
							
				} else {
					serviceError = true;
				}
			}
		} else {
			serviceError = true;
		}
		
		

		/*if(DeviceMgr.checkSvcInstance(cbSvcInstance)){
			resp = DeviceControl.deleteDevice(txtDeviceId);
			if(resp == "OK" || resp.equals("OK")) {
				updateStatus = DeviceMgr.updateDevice(currentDevice);
				if(updateStatus){
					resp = DeviceControl.addDevice(txtDeviceId, txtIpAddress, txtPortNo, cbBranchId);
					if(resp == "OK" || resp.equals("OK")) {
								
					} else {
						serviceError = true;
					}
				}
				
			} else {
				serviceError = true;
			}
		} else {
			serviceError = true;
			resp = "Instance Full";
		}*/
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (updateStatus) {
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		} else {
			if(serviceError) {
				request.getSession().setAttribute("LastErrMsg", resp);
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
			} else {
				request.getSession().setAttribute("LastErrMsg", DeviceMgr.getErrorMessage());
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
			}
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
	} 
%>