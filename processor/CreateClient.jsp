<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBCLIENT"%>
<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="ClientMgr" scope="page" class="Biometrics.ClientManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "ClientList.jsp" + CurrentParam; %>
<% String sBackPageName = "ClientAdd.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		TBCLIENT sClient = new TBCLIENT();
		TBUSER SessionUser = new TBUSER();
	
		ClientMgr.setConnection(DBMgr.getConnection());
		UserMgr.setConnection(DBMgr.getConnection());	
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		String txtClientIp = request.getParameter("txtClientIp").toString().trim();
		String txtClientName = request.getParameter("txtClientName").toString().trim();
		String cbAreaId = request.getParameter("cbAreaId").toString().trim();
		
		sClient.setCLIENTIP(txtClientIp);
		sClient.setCLIENTNAME(txtClientName);
		sClient.setAREAID(cbAreaId);
		sClient.setCREATEDBY(SessionUser.getUSERNAME());
		
		boolean addStatus = ClientMgr.addClient(sClient);
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
			
		if (addStatus)  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		else{
			request.getSession().setAttribute("LastErrMsg", ClientMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
%>