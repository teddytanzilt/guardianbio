<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBSTAFF"%>
<%@page import="util.ProcessUtil"%>
<%@page import="java.io.*"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="StaffMgr" scope="page" class="Biometrics.StaffManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "StaffList.jsp" + CurrentParam; %>
<% String sBackPageName = "StaffList.jsp" + CurrentParam; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
	
		int sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
		 
		StaffMgr.setConnection(DBMgr.getConnection());
		
		TBSTAFF currentStaff = StaffMgr.getStaffById(sId);		
		
		String resp = "";
		boolean suspendStatus = false;
		boolean serviceError = false;
		
		suspendStatus = StaffMgr.suspendStaff(sId);
		if (suspendStatus){
			//resp = DeviceControl.enrollStaff(currentStaff.getSTAFFID());
			resp = DeviceControl.deleteStaff(currentStaff.getSTAFFID());
		
			if(resp == "OK" || resp.equals("OK")) {
				
			} else {
				serviceError = true;
				suspendStatus = false;
				StaffMgr.unsuspendStaff(sId);
			}
		}
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (suspendStatus) {
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		} else { 
			if(serviceError){
				request.getSession().setAttribute("LastErrMsg", resp);
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			} else {
				request.getSession().setAttribute("LastErrMsg", StaffMgr.getErrorMessage());
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			}
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
%>