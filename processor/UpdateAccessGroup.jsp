<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBACCESSGROUP"%>
<%@page import="Biometrics.TBACCESSGROUPDETAIL"%>
<%@page import="util.ProcessUtil"%>
<%@page import="java.io.*"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="AccessGroupMgr" scope="page" class="Biometrics.AccessGroupManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "AccessGroupList.jsp" + CurrentParam; %>
<% String sBackPageName = "AccessGroupEdit.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
	
	String CurrentUrl = String.valueOf(request.getSession().getAttribute("CurrentUrl"));
	String PrevUrl = String.valueOf(request.getSession().getAttribute("PrevUrl"));

	int sId = 0;
	String oldBranchList = ""; String newBranchList = "";
	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		
		UserMgr.setConnection(DBMgr.getConnection());
		AccessGroupMgr.setConnection(DBMgr.getConnection());	
		
		TBUSER SessionUser = new TBUSER();
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		sId = Integer.parseInt(String.valueOf(request.getParameter("hdnId")));
		String txtAccessGroupId = request.getParameter("txtAccessGroupId").toString().trim();
		String txtAccessGroupName = request.getParameter("txtAccessGroupName").toString().trim();
		String[] cBranches = request.getParameterValues("cBranches");
		
		TBACCESSGROUP currentAccessGroup = AccessGroupMgr.getAccessGroupById(sId);
		currentAccessGroup.setACCESSGROUPID(txtAccessGroupId);
		currentAccessGroup.setACCESSGROUPNAME(txtAccessGroupName);
		currentAccessGroup.setLASTUPDATETIMEDBY(SessionUser.getUSERNAME());
		
		/*START PROCESS*/
		String[] oldBranches = AccessGroupMgr.getAccessGroupDetailBranch(txtAccessGroupId);
		
		String resp = "OK";
		boolean updateStatus = false;
		boolean serviceError = false;
		
		updateStatus = AccessGroupMgr.updateAccessGroup(currentAccessGroup); 
		if(updateStatus){
			TBACCESSGROUPDETAIL sAccessGroupDetail = new TBACCESSGROUPDETAIL();
			sAccessGroupDetail.setACCESSGROUPID(txtAccessGroupId);
			boolean deleteStatus = AccessGroupMgr.deleteAccessGroupDetail(sAccessGroupDetail);
				
			for(int i=0; i<cBranches.length; i++){
				sAccessGroupDetail.setBRANCHID(cBranches[i].toString().trim());
				boolean insertStatus = AccessGroupMgr.addAccessGroupDetail(sAccessGroupDetail);
		   	}
		
			// Identify which branch to be added and which to be deleted. 
			if (oldBranches != null && cBranches != null)
			{
				// Compare the 2 array and strike-out the common branch.
				for (int i=0; i < cBranches.length; i++)
				{
					// look for the new branch in the old list.
					for (int ix=0; ix < oldBranches.length; ix++)
					{
						if (cBranches[i].equals(oldBranches[ix]))
						{
							// strike-out this (common) element in both array.
							cBranches[i] = "";
							oldBranches[ix] = "";
							break;
						}
					}
				}
			}		
			
			

			// Delete old branches from the access group.
			if(oldBranches != null){ 
				for (int i = 0; i < oldBranches.length; i++) {
					if(oldBranchList.length() == 0){
						oldBranchList = oldBranches[i];	
					} else {
						oldBranchList = oldBranchList + "," + oldBranches[i];	
					}
				} 
				
				if (oldBranchList.length() > 0) {
					resp = DeviceControl.deleteAccessGrp(txtAccessGroupId, oldBranchList);
				}
			} 
			
			if(cBranches != null){ 
				for (int i = 0; i < cBranches.length; i++) {
					if(newBranchList.length() == 0){
						newBranchList = cBranches[i];
					} else {
						newBranchList = newBranchList + "," + cBranches[i];
					}
				} 
				
				if (newBranchList.length() > 0) {
					resp = DeviceControl.enrollAccessGrp(txtAccessGroupId, newBranchList);
				}
			} 		
		}
		
		if(resp == "OK" || resp.equals("OK")) {
			
		} else {
			serviceError = true;
			updateStatus = false;
		}
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (updateStatus) {  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		} else {
			if(serviceError){
				request.getSession().setAttribute("LastErrMsg", resp);
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
			} else {
				request.getSession().setAttribute("LastErrMsg", AccessGroupMgr.getErrorMessage());
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
			}
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
	} 	
%>