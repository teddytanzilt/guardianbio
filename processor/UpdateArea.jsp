<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBAREA"%>

<jsp:useBean id="AreaMgr" scope="page" class="Biometrics.AreaManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "AreaList.jsp" + CurrentParam; %>
<% String sBackPageName = "AreaEdit.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
	
	int sId = 0;
	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		
		UserMgr.setConnection(DBMgr.getConnection());
		AreaMgr.setConnection(DBMgr.getConnection());	
		
		TBUSER SessionUser = new TBUSER();
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		sId = Integer.parseInt(String.valueOf(request.getParameter("hdnId")));
		String txtAreaId = request.getParameter("txtAreaId").toString().trim();
		String txtAreaName = request.getParameter("txtAreaName").toString().trim();
		
		TBAREA currentArea = AreaMgr.getAreaById(sId);
		currentArea.setAREAID(txtAreaId);
		currentArea.setAREANAME(txtAreaName);
		currentArea.setLASTUPDATETIMEDBY(SessionUser.getUSERNAME());
		
		boolean updateStatus = AreaMgr.updateArea(currentArea);
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (updateStatus)  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		else{
			request.getSession().setAttribute("LastErrMsg", AreaMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
	} 	
%>