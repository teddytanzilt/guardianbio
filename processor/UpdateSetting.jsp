<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBSETTING"%>

<jsp:useBean id="SettingMgr" scope="page" class="Biometrics.SettingManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String sSuccessPageName = "Setting.jsp"; %>
<% String sBackPageName = "Setting.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
	boolean validation = true;
	
	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		
		UserMgr.setConnection(DBMgr.getConnection());
		SettingMgr.setConnection(DBMgr.getConnection());	

		TBUSER SessionUser = new TBUSER();
		SessionUser = UserMgr.getUserById(SessionUserId);

		TBSETTING[] aSettings = SettingMgr.getSettingAllNoOffset();
		
		for (int i = 0; i < aSettings.length; i++) {			
			if(validation){
				String txtSettingValue = request.getParameter("txt" + aSettings[i].getSETTINGID()).toString().trim();
				
				TBSETTING currentSetting = SettingMgr.getSettingBySettingId(aSettings[i].getSETTINGID());
				currentSetting.setSETTINGVALUE(txtSettingValue);
				currentSetting.setLASTUPDATETIMEDBY(SessionUser.getUSERNAME());
				
				boolean updateStatus = SettingMgr.updateSetting(currentSetting);
				if(!updateStatus){
					validation = false;
				}
			}
		}
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (validation)  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		else{
			request.getSession().setAttribute("LastErrMsg", SettingMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	} 
%>