<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBSTAFF"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="java.io.*"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="StaffMgr" scope="page" class="Biometrics.StaffManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "StaffList.jsp" + CurrentParam; %>
<% String sBackPageName = "StaffAdd.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		TBSTAFF sStaff = new TBSTAFF();
		TBUSER SessionUser = new TBUSER();
	
		StaffMgr.setConnection(DBMgr.getConnection());
		UserMgr.setConnection(DBMgr.getConnection());	
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		String txtStaffId = request.getParameter("txtStaffId").toString().trim();
		String txtStaffName = request.getParameter("txtStaffName").toString().trim();
		String cbAccessGroupId = request.getParameter("cbAccessGroupId").toString().trim();
		String txtCardNo = request.getParameter("txtCardNo").toString().trim();
		String cbPrivilege = request.getParameter("cbPrivilege").toString().trim();
		String txtPassword = request.getParameter("txtPassword").toString().trim();
		
		sStaff.setSTAFFID(txtStaffId);
		sStaff.setSTAFFNAME(txtStaffName);
		sStaff.setPRIVILEGE(cbPrivilege);
		sStaff.setACCESSGROUPID(cbAccessGroupId);
		sStaff.setCARDNO(txtCardNo);
		sStaff.setPASSWORD(txtPassword);
		sStaff.setCREATEDBY(SessionUser.getUSERNAME());
		
		String resp = "";
		boolean addStatus = false;
		boolean serviceError = false;
		
		addStatus = StaffMgr.addStaff(sStaff);
		if(addStatus){
			resp = DeviceControl.enrollStaff(sStaff.getSTAFFID());		
			if(resp == "OK" || resp.equals("OK")) {
				
			} else {
				serviceError = true;
				addStatus = false;
				TBSTAFF newStaff = StaffMgr.getStaffByStaffId(txtStaffId);
				StaffMgr.deleteStaff(newStaff.getSTAFFID());
			}
		}
		
		
		/*resp = DeviceControl.enrollStaff(sStaff.getSTAFFID());		
		if(resp == "OK" || resp.equals("OK")) {
			addStatus = StaffMgr.addStaff(sStaff);
		} else {
			serviceError = true;
		}*/
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
			
		if (addStatus) { 
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		} else {
			if(serviceError){
				request.getSession().setAttribute("LastErrMsg", resp);
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			} else {
				request.getSession().setAttribute("LastErrMsg", StaffMgr.getErrorMessage());
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			}
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
%>