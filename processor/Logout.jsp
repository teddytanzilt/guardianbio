<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<%	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}

	DBMgr = null;
	request.getSession().setAttribute("SessionLoginID", null);
	request.getSession().setAttribute("SessionLoginUSERNAME", null);
	request.getSession().setAttribute("SessionLoginROLE", null);
	
	response.sendRedirect(request.getContextPath() + "/Login.jsp");
%>

