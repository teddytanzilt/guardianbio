<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBBRANCH"%>
<%@page import="Biometrics.TBUSER"%>

<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "BranchList.jsp" + CurrentParam; %>
<% String sBackPageName = "BranchAdd.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		TBBRANCH sBranch = new TBBRANCH();
		TBUSER SessionUser = new TBUSER();
	
		BranchMgr.setConnection(DBMgr.getConnection());
		UserMgr.setConnection(DBMgr.getConnection());	
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		String txtBranchId = request.getParameter("txtBranchId").toString().trim();
		String txtBranchName = request.getParameter("txtBranchName").toString().trim();
		String cbAreaId = request.getParameter("cbAreaId").toString().trim();
		String txtEmailNotifyAddress = request.getParameter("txtEmailNotifyAddress").toString().trim();
		
		sBranch.setBRANCHID(txtBranchId);
		sBranch.setBRANCHNAME(txtBranchName);
		sBranch.setAREAID(cbAreaId);
		sBranch.setEMAILNOTIFYADDRESS(txtEmailNotifyAddress);
		sBranch.setCREATEDBY(SessionUser.getUSERNAME());
		
		boolean addStatus = BranchMgr.addBranch(sBranch);
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
			
		if (addStatus)  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		else{
			request.getSession().setAttribute("LastErrMsg", BranchMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
%>