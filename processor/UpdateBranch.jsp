<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBBRANCH"%>

<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "BranchList.jsp" + CurrentParam; %>
<% String sBackPageName = "BranchEdit.jsp"; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
	
	int sId = 0;
	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
		
		UserMgr.setConnection(DBMgr.getConnection());
		BranchMgr.setConnection(DBMgr.getConnection());	
		
		TBUSER SessionUser = new TBUSER();
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		sId = Integer.parseInt(String.valueOf(request.getParameter("hdnId")));
		String txtBranchId = request.getParameter("txtBranchId").toString().trim();
		String txtBranchName = request.getParameter("txtBranchName").toString().trim();
		String cbAreaId = request.getParameter("cbAreaId").toString().trim();
		String txtEmailNotifyAddress = request.getParameter("txtEmailNotifyAddress").toString().trim();
		
		TBBRANCH currentBranch = BranchMgr.getBranchById(sId);
		currentBranch.setBRANCHID(txtBranchId);
		currentBranch.setBRANCHNAME(txtBranchName);
		currentBranch.setAREAID(cbAreaId);
		currentBranch.setEMAILNOTIFYADDRESS(txtEmailNotifyAddress);
		currentBranch.setLASTUPDATETIMEDBY(SessionUser.getUSERNAME());
		
		boolean updateStatus = BranchMgr.updateBranch(currentBranch);
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (updateStatus)  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		else{
			request.getSession().setAttribute("LastErrMsg", BranchMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName + "?Id=" + sId);
	} 	
%>