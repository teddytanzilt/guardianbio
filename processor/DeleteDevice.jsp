<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="Biometrics.TBDEVICE"%>
<%@page import="util.ProcessUtil"%>
<%@page import="java.io.*"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="DeviceMgr" scope="page" class="Biometrics.DeviceManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "DeviceList.jsp" + CurrentParam; %>
<% String sBackPageName = "DeviceList.jsp" + CurrentParam; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
	
	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
	
		int sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
		 
		DeviceMgr.setConnection(DBMgr.getConnection());
		TBDEVICE sDevice = DeviceMgr.getDeviceById(sId);
		
		String resp = "";
		boolean deleteStatus = false;
		boolean serviceError = false;
		resp = DeviceControl.deleteDevice(sDevice.getDEVICEID());
		
		if(resp == "OK" || resp.equals("OK")) {
			deleteStatus = DeviceMgr.deleteDevice(sId); 
		} else {
			serviceError = true;
		}
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (deleteStatus) {
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		} else {
			if(serviceError){
				request.getSession().setAttribute("LastErrMsg", resp);
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			} else {
				request.getSession().setAttribute("LastErrMsg", DeviceMgr.getErrorMessage());
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			}
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
%>