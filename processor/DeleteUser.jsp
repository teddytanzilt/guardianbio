<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "UserList.jsp" + CurrentParam; %>
<% String sBackPageName = "UserList.jsp" + CurrentParam; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
	
	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/ErrorPage.jsp");
				return;
			}
		}
	
		int sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
		 
		UserMgr.setConnection(DBMgr.getConnection());
		
		boolean deleteStatus = UserMgr.deleteUser(sId);
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (deleteStatus)
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		else{ 
			request.getSession().setAttribute("LastErrMsg", UserMgr.getErrorMessage());
			response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}
%>