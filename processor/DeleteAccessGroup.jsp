<%@include file="CheckSession.jsp" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	
<%@page import="Biometrics.TBACCESSGROUP"%>
<%@page import="Biometrics.TBACCESSGROUPDETAIL"%>
<%@page import="Biometrics.DeviceControl"%>

<jsp:useBean id="AccessGroupMgr" scope="page" class="Biometrics.AccessGroupManager" />
<jsp:useBean id="DBMgr" scope="session" class="Biometrics.DatabaseManager" />

<% String CurrentParam = String.valueOf(request.getSession().getAttribute("CurrentParam")); %>
<% String sSuccessPageName = "AccessGroupList.jsp" + CurrentParam; %>
<% String sBackPageName = "AccessGroupList.jsp" + CurrentParam; %>

<%
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
	
	try {
		if (DBMgr.getConnection() == null){
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
	
		int sId = Integer.parseInt(String.valueOf(request.getParameter("Id")));
		
		AccessGroupMgr.setConnection(DBMgr.getConnection());
		
		TBACCESSGROUP sAccessGroup = AccessGroupMgr.getAccessGroupById(sId);
		
		String resp = "";
		boolean deleteStatus = false;
		boolean serviceError = false;
		if(AccessGroupMgr.checkAccessGroupUsed(sAccessGroup.getACCESSGROUPID())){
			String oldBranchList = "";
			String[] oldBranches = AccessGroupMgr.getAccessGroupDetailBranch(sAccessGroup.getACCESSGROUPID());
			if(oldBranches != null){ 
				for (int i = 0; i < oldBranches.length; i++) {
					if(oldBranchList==""){
						oldBranchList = oldBranches[i];	
					} else {
						oldBranchList = oldBranchList + "," + oldBranches[i];	
					}
				} 
			} 
			
			resp = DeviceControl.deleteAccessGrp(sAccessGroup.getACCESSGROUPID(),oldBranchList);
			
			if(resp == "OK" || resp.equals("OK")) {
				deleteStatus = AccessGroupMgr.deleteAccessGroup(sId); 
			} else {
				serviceError = true;
			}
			
			if(deleteStatus){
				TBACCESSGROUPDETAIL sAccessGroupDetail = new TBACCESSGROUPDETAIL();
				sAccessGroupDetail.setACCESSGROUPID(sAccessGroup.getACCESSGROUPID());
				AccessGroupMgr.deleteAccessGroupDetail(sAccessGroupDetail);
			}
		} else {
			serviceError = true;
			resp = "Access Group is bind to user.";
		}
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
		
		if (deleteStatus)  
			response.sendRedirect(request.getContextPath() + "/" + sSuccessPageName);
		else{
			if(serviceError){
				request.getSession().setAttribute("LastErrMsg", resp);
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			} else {
				request.getSession().setAttribute("LastErrMsg", AccessGroupMgr.getErrorMessage());
				response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
			}
		}
	} catch (Exception e) {
		request.getSession().setAttribute("LastErrMsg", e.getMessage());
		response.sendRedirect(request.getContextPath() + "/" + sBackPageName);
	}

%>