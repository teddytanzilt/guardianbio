
CREATE VIEW [dbo].[v_attendance_inout_accessgroup] AS
select 
v_attendance_inout.STAFFID,
TBSTAFF.ACCESSGROUPID,
v_attendance_inout.BRANCHID,
v_attendance_inout.ATTENDANCEDATE,
v_attendance_inout.CLOCKINTIME,
v_attendance_inout.CLOCKOUTTIME  
from v_attendance_inout
LEFT JOIN TBSTAFF ON (v_attendance_inout.STAFFID = TBSTAFF.STAFFID)
GO
