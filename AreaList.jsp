<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBAREA"%>
<%@page import="util.DateFormatConverter"%>

<jsp:useBean id="AreaMgr" scope="page" class="Biometrics.AreaManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<%
String CurrentParam = "?"+request.getQueryString();
request.getSession().setAttribute("CurrentParam", CurrentParam);
%>

<% String sTableTitle = "Area Management"; %>
<% String sPageName = "AreaList.jsp"; %>
<% String sAddPageName = "AreaAdd.jsp"; %>
<% String sEditPageName = "AreaEdit.jsp"; %>
<% String sDeletePageName = "processor/DeleteArea.jsp"; %>

<%!Biometrics.TBAREA[] aAreas;%>
<%!String sAreaId; %>
<%!String sAreaName; %>
<%!String sTotalRecord; %>
<%!String sOffset; %>
<%!int iPage; %>
<%!int iMaxPage; %>
<%!TBUSER SessionUser; %>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}

	UserMgr.setConnection(DBMgr.getConnection());
	AreaMgr.setConnection(DBMgr.getConnection());
	
	SessionUser = UserMgr.getUserById(SessionUserId);
	sAreaId = request.getParameter("txtAreaId");
	sAreaName = request.getParameter("txtAreaName");
	sOffset = request.getParameter("txtOffset");
	
	if (sAreaId == null || sAreaId.trim().length() <= 0) {
		sAreaId = "";		
	}
	if (sAreaName == null || sAreaName.trim().length() <= 0) {
		sAreaName = "";		
	}
	if (sOffset == null || sOffset.trim().length() <= 0) {
		sOffset = "0";		
	}
	
	if (sAreaId != "" || sAreaName != "") {
		aAreas = AreaMgr.getAreaByFilter(sAreaId,sAreaName,sOffset);  
		sTotalRecord = AreaMgr.countAreaByFilter(sAreaId,sAreaName) + "";
	} else {
		aAreas = AreaMgr.getAreaAll(sOffset);
		sTotalRecord = AreaMgr.countAreaAll() + "";
	}
	
	iPage = (Integer.parseInt(sOffset) / 30) + 1;
	if(Integer.parseInt(sTotalRecord) % 30 != 0){
		iMaxPage = (Integer.parseInt(sTotalRecord) / 30) + 1;
	} else {
		iMaxPage = (Integer.parseInt(sTotalRecord) / 30);
	}
	
	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
}
%>

<script language="javascript" type="text/javascript">
$(function() {
    $(".pageNumber").click(function (){
    	var sNextOffset = (parseInt($(this).html()) - 1) * 30;
    	var TotalRecord = parseInt($("#TotalRecord").val());
    	
    	var sAreaId = $('#txtAreaId').val();
		var sAreaName = $('#txtAreaName').val();
		HidePaging();
    	$(this).attr("href", "<%=sPageName%>?txtAreaId=" + sAreaId + "&txtAreaName=" + sAreaName + "&txtOffset=" + sNextOffset + "");    	
     });
    
 });
 
function HidePaging(){
	$(".pageNumber").css("display","none");	
	$(".Next").css("display","none");	
	$(".Prev").css("display","none");	
	$(".currentPage").css("display","none");
	$(".minPageNumber").css("display","none");
	$(".maxPageNumber").css("display","none");
}
 
function IsConfirmedDelete(obj)
{
	return confirm("Are you sure want to delete?");	
}
function IsConfirmedImport(obj)
{
	return confirm("Are you sure want to import?");	
}
function IsConfirmedSearch(obj)
{	
	$("#thisForm").attr("action", "<%=sPageName%>");	
	$("#txtOffset").val("0");
	$("#thisForm").submit();
	return true;
}

function Next(obj)
{	
	var nextLink = document.getElementById("Next");
	var prevLink = document.getElementById("Prev");
		
	var sOffset = parseInt($("#txtOffset").val());
	var TotalRecord = parseInt($("#TotalRecord").val());	
	if(sOffset+10 <=  TotalRecord){
		HidePaging();
		
		var sAreaId = $('#txtAreaId').val();
		var sAreaName = $('#txtAreaName').val();
		
		sOffset = sOffset + 30;

		var targetHref="<%=sPageName%>?txtAreaId=" + sAreaId + "&txtAreaName=" + sAreaName + "&txtOffset=" + sOffset + "";
		$(".Next").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Prev(obj)
{	
	var nextLink = document.getElementById("Next");
	var prevLink = document.getElementById("Prev");
	
	var sOffset = parseInt($("#txtOffset").val());
	if(sOffset > 0){	
		HidePaging();
		
		var sAreaId = $('#txtAreaId').val();
		var sAreaName = $('#txtAreaName').val();	
		sOffset = sOffset - 30;

		var targetHref="<%=sPageName%>?txtAreaId=" + sAreaId + "&txtAreaName=" + sAreaName + "&txtOffset=" + sOffset + "";
		$(".Prev").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Max(obj)
{
	var sOffset = (parseInt($("#MaxPage").val()) - 1) * 30;
	var TotalRecord = parseInt($("#TotalRecord").val());	
	HidePaging();		
	var sAreaId = $('#txtAreaId').val();
	var sAreaName = $('#txtAreaName').val();
	var targetHref="<%=sPageName%>?txtAreaId=" + sAreaId + "&txtAreaName=" + sAreaName + "&txtOffset=" + sOffset + "";
	$(".maxPageNumber").attr("href", targetHref); 
	return true;
}

function Min(obj)
{
	var TotalRecord = parseInt($("#TotalRecord").val());	
	HidePaging();		
	var sAreaId = $('#txtAreaId').val();
	var sAreaName = $('#txtAreaName').val();	
	var targetHref="<%=sPageName%>?txtAreaId=" + sAreaId + "&txtAreaName=" + sAreaName + "&txtOffset=0";
	$(".minPageNumber").attr("href", targetHref); 
	return true;
}

</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
				
	            <div class="row">
	                <div class="col-md-12">
	                    <!--    Striped Rows Table  -->
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                        	<Form id = "thisForm">
	                        	<input type = "hidden" id = "TotalRecord" value = "<%=sTotalRecord%>">
								<input type = "hidden" id = "txtOffset" name="txtOffset"  value="<%= sOffset == null ? "0" : sOffset%>">
								<input type = "hidden" id = "MaxPage" value = "<%=iMaxPage%>">
			
	                            <div class="row">
									<div class="col-md-3">
			                            <div class="input-group">
			                            	<input class="form-control" id = "txtAreaId" name="txtAreaId" type="text" placeholder="Area Id"  value="<%= sAreaId == null ? "" : sAreaId%>" />
		                                </div>
		                            </div>
                    				<div class="col-md-3">
			                            <div class="input-group">
			                            	<input class="form-control" id = "txtAreaName" name="txtAreaName" type="text" placeholder="Area Name"  value="<%= sAreaName == null ? "" : sAreaName%>" />
		                                </div>
		                            </div>
		                            <div class="col-md-1">
			                            <div class="input-group">
		                                    <span class="input-group-btn">
		                                        <button class="btn btn-success" id = "btnSearch" type="button" onclick="return IsConfirmedSearch(this);">Search</button>
		                                    </span>
		                                </div>
		                            </div>
		                      	</div>
		                      	</Form>
	                        </div>
	                        <div class="panel-body">
	                            <div class="table-responsive">
	                                <table class="table table-striped">
	                                    <thead>
	                                        <tr>
	                                            <th>#</th>
												<th>Area Id</th>
	                                            <th>Area Name</th>
	                                            <th>Created On</th>
	                                            <th>Modified On</th>
	                                            <th>Action</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    <%
											if (aAreas != null) {
														
												for (int i = 0; i < aAreas.length; i++) {
										%>
	                                        <tr>
	                                            <td><%=Integer.parseInt(sOffset)+i+1%></td>
												<td><%=aAreas[i].getAREAID()%></td>
												<td><%=aAreas[i].getAREANAME() != null ? aAreas[i].getAREANAME() : "&nbsp;" %></td>
												<td><%=aAreas[i].getCREATEDSTAMP() != null ? DateFormatConverter.format2(aAreas[i].getCREATEDSTAMP(), "yyyy-MM-dd HH:mm:ss") : "&nbsp;" %></td>
												<td><%=aAreas[i].getLASTUPDATETIMEDSTAMP() != null ? DateFormatConverter.format2(aAreas[i].getLASTUPDATETIMEDSTAMP(), "yyyy-MM-dd HH:mm:ss") : "&nbsp;" %></td>
												<td >
													<a href="<%=sEditPageName %>?Id=<%=aAreas[i].getID()%>">Edit</a> 	&nbsp; 	&nbsp;
													<a href="<%=sDeletePageName %>?Id=<%=aAreas[i].getID()%>" onclick="return IsConfirmedDelete(this);">Delete</a>
												</td>
	                                        </tr>
	                                    <%
												}
											}
										%> 
	                                    </tbody>
	                                </table>
	                                <hr />
                            		<ul class="pagination" style = "float:right;">
                            		<%if(Integer.parseInt(sTotalRecord) != 0) { %>
										<%if(iPage > 1){ %>
											<li><a href="<%=sPageName %>" class = "minPageNumber" onclick = "return Min(this);">First</a></li>
									  		<li><a href="<%=sPageName %>" class = "Prev" onclick="return Prev(this);">&laquo;</a></li>
										<% } %>
										<%if(iPage == 1){ %>
				
										<%} else if(iPage == 2){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber" >1</a></li>
										<%} else { %>
										
										<%if(iPage == iMaxPage){ %>
											<% if(iPage-4 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-4; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else if(iPage == iMaxPage - 1){ %>
											<% if(iPage-3 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-3; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else { %>
											<% if(iPage-2 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-2; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} %>	
									<%} %>
									<li class="active"><a class = "currentPage"><%=iPage %><span class="sr-only">(current)</span></a></li>
									<%if(iPage == iMaxPage){ %>
				
									<%} else if(iPage == iMaxPage - 1){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber" ><%=iMaxPage %></a></li>
									<%} else { %>
										<% if(iPage == 1){  %>
											<% if(iMaxPage < 5){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 5; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else if(iPage == 2){  %>
											<% if(iMaxPage <= 4){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 4; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else { %>
											<%for(int x = iPage + 1; x < iPage + 3; x++) { %>
												<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
											<% } %>
										<% } %>
									<%} %>		
									
									<%if(iPage < iMaxPage){ %>
											<li><a href="<%=sPageName %>" class = "Next" onclick = "return Next(this);">&raquo;</a></li>
									  		<li><a href="<%=sPageName %>" class = "maxPageNumber" onclick="return Max(this);">Last</a></li>
										<% } %>
									<%} %>
									</ul>
	                            
	                            	<a class="btn btn-info" style = "float:left;"  href="<%=sAddPageName %>">Add New</a>
	                            </div>
	                            
	                            
	                            
	                        </div>
	                    </div>
	                    <!--  End  Striped Rows Table  -->
	                </div>
	                
	            </div>
                <!-- /. ROW  -->
           

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
	<%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
