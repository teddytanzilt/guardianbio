<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckEmptySession.jsp" %>
<body style="background-color: #E2E2E2;">
    <div class="container">
        <div class="row text-center " style="padding-top:100px;">
            <div class="col-md-12">
                <img src="assets/img/logo.png" />
            </div>
        </div>
         <div class="row ">
	        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                  <div class="panel-body">
                  <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				  <% } %>
                  		<form role="form" action = "processor/Login.jsp">
                          <hr />
                          <h5>Enter Details to Login</h5>
                          <br />
                          <div class="form-group input-group">
                              <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                              <input type="text" class="form-control" placeholder="Your Username " name = "txtUserId" id = "txtUserId" />
                          </div>
                          <div class="form-group input-group">
                              <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                              <input type="password" class="form-control"  placeholder="Your Password" name = "txtPassword" id = "txtPassword"/>
                          </div>                                     
                          <input type="submit" value="Login" class="btn btn-primary" />
                          <hr />
                           &copy; 2017 Radiant Global (M) Sdn. Bhd. (2.1.0)
                          </form>
                   </div>
               </div>
        </div>
    </div>

</body>
<%@include file="layout/Footer.jsp" %>