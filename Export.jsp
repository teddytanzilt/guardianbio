<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBBRANCH"%>
<%@page import="Biometrics.TBATTENDANCE"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="util.ProcessUtil"%>
<%@page import="java.io.*"%>
<%@page import="util.DateFormatConverter"%>
<%@page import="java.util.Date"%>
<%@page import="Biometrics.REPORT1"%>


<jsp:useBean id="AttendanceMgr" scope="page" class="Biometrics.AttendanceManager" />
<jsp:useBean id="BranchMgr" scope="page" class="Biometrics.BranchManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />
<jsp:useBean id="ProcessUtl" scope="session" class="util.ProcessUtil" />

<% String sTableTitle = "Export"; %>
<% String sDataTitle = "Date Range"; %>
<% String sPageName = "Export.jsp"; %>
<% String sBackPageName = "Export.jsp"; %>
<% String sProcessPageName = "processor/ExportProcess.jsp"; %>

<%!TBUSER SessionUser; %>
<%!TBBRANCH[] aBranchs;%>
<%!String txtStaffId; %>
<%!String cbBranchId; %>
<%!String txtStartDate; %>
<%!String txtStartMonth; %>
<%!String txtStartYear; %>
<%!String txtEndDate; %>
<%!String txtEndMonth; %>
<%!String txtEndYear; %>
<%!String sCurrentError = ""; %>

<%
int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));
try {
	if (DBMgr.getConnection() == null) { 
		if (!DBMgr.openConnection()) {
			request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
			response.sendRedirect(request.getContextPath() + "/Login.jsp");
			return;
		}
	}
	
	UserMgr.setConnection(DBMgr.getConnection());
	BranchMgr.setConnection(DBMgr.getConnection());
	AttendanceMgr.setConnection(DBMgr.getConnection());
	SessionUser = UserMgr.getUserById(SessionUserId);
	
	aBranchs = BranchMgr.getBranchAllNoOffset();
	
	txtStaffId = request.getParameter("txtStaffId");
	cbBranchId = request.getParameter("cbBranchId");
	txtStartDate = request.getParameter("txtStartDate");
	txtStartMonth = request.getParameter("txtStartMonth");
	txtStartYear = request.getParameter("txtStartYear");
	txtEndDate = request.getParameter("txtEndDate");
	txtEndMonth = request.getParameter("txtEndMonth");
	txtEndYear = request.getParameter("txtEndYear");
	
	if (txtStaffId == null || txtStaffId.trim().length() <= 0) {
		txtStaffId = "";		
	}
	if (cbBranchId == null || cbBranchId.trim().length() <= 0) {
		cbBranchId = "";		
	}
	if (txtStartDate == null || txtStartDate.trim().length() <= 0) {
		txtStartDate = DateFormatConverter.format(new Date(), "dd");		
	}
	if (txtStartMonth == null || txtStartMonth.trim().length() <= 0) {
		txtStartMonth = DateFormatConverter.format(new Date(), "MM");	
	}
	if (txtStartYear == null || txtStartYear.trim().length() <= 0) {
		txtStartYear = DateFormatConverter.format(new Date(), "yyyy");		
	}
	if (txtEndDate == null || txtEndDate.trim().length() <= 0) {
		txtEndDate = DateFormatConverter.format(new Date(), "dd");		
	}
	if (txtEndMonth == null || txtEndMonth.trim().length() <= 0) {
		txtEndMonth = DateFormatConverter.format(new Date(), "MM");	
	}
	if (txtEndYear == null || txtEndYear.trim().length() <= 0) {
		txtEndYear = DateFormatConverter.format(new Date(), "yyyy");		
	}
	
	String Start =  txtStartYear + txtStartMonth + txtStartDate;
	String End =  txtEndYear + txtEndMonth + txtEndDate;

	if (DBMgr.getConnection() != null){
		if (!DBMgr.getConnection().isClosed())
			DBMgr.closeConnection();
	}
} catch (Exception e) {
	sCurrentError = e.getMessage();
} 	
%>

<script language="javascript" type="text/javascript">
$(function() {
	$( "#btnBack" ).click(function() {
		//window.history.back();
		window.location = "<%=sBackPageName %>";
	});
});
</script>

<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
	             <div class="row">
		            <div class="col-md-6 col-sm-6 col-xs-12">
		               <div class="panel panel-default">
                        <div class="panel-heading">
                           <%=sDataTitle %>
                        </div>
                   		<div class="panel-body">
                           <form action="<%=sProcessPageName %>" method="post" id="myform" role = "form" onsubmit="return CheckForm(this)" >
                                
                               	<div class="form-group">
                                    <label>Staff Id</label>
                                    <input class="form-control" id="txtStaffId" type="text" name="txtStaffId" value = "<%=txtStaffId%>" />
                                </div>
                                
                                <div class="form-group">
                                    <label>Branch</label>
                                    <select class="form-control selectpicker" data-live-search="true"  id ="cbBranchId" name ="cbBranchId">
										<option value = "">All Branch</option>
										<%if(aBranchs != null){ %>
											<%for (int i = 0; i < aBranchs.length; i++) { %>
												<option value = "<%=aBranchs[i].getBRANCHID()%>" <%=cbBranchId.equals(aBranchs[i].getBRANCHID().trim())?"selected":""%>><%=aBranchs[i].getBRANCHNAME()%></option>
											<%} %>
										<%} %>
									</select>
                                </div>
                                
                                <div class="form-group">
                                    <label>Start Date</label>
									<div class="row">
										<div class="col-md-3">
											<div class="input-group">
												<input class="form-control" id = "txtStartDate" name="txtStartDate" type="text" placeholder="DD" size="2" onkeypress="return isNumberKey(event)" value="<%= txtStartDate == null ? "" : txtStartDate%>" />
											</div>
										</div>
										<div class="col-md-3">
											<div class="input-group">
												<input class="form-control" id = "txtStartMonth" name="txtStartMonth" type="text" placeholder="MM" size="2" onkeypress="return isNumberKey(event)" value="<%= txtStartMonth == null ? "" : txtStartMonth%>" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<input class="form-control" id = "txtStartYear" name="txtStartYear" type="text" placeholder="YYYY" size="4" onkeypress="return isNumberKey(event)" value="<%= txtStartYear == null ? "" : txtStartYear%>" />
											</div>
										</div>
									</div>									
                                </div>
                                <div class="form-group">
                                    <label>End Date</label>
									<div class="row">
										<div class="col-md-3">
											<div class="input-group">
												<input class="form-control" id = "txtEndDate" name="txtEndDate" type="text" placeholder="DD" size="2" onkeypress="return isNumberKey(event)" value="<%= txtEndDate == null ? "" : txtEndDate%>" />
											</div>
										</div>
										<div class="col-md-3">
											<div class="input-group">
												<input class="form-control" id = "txtEndMonth" name="txtEndMonth" type="text" placeholder="MM" size="2" onkeypress="return isNumberKey(event)" value="<%= txtEndMonth == null ? "" : txtEndMonth%>" />
											</div>
										</div>
										<div class="col-md-6">
											<div class="input-group">
												<input class="form-control" id = "txtEndYear" name="txtEndYear" type="text" placeholder="YYYY" size="4" onkeypress="return isNumberKey(event)" value="<%= txtEndYear == null ? "" : txtEndYear%>" />
											</div>
										</div>
									</div>									
                                </div>
                                <button type="submit" class="btn btn-info">Download </button>
                                <button id="btnBack" class="btn btn-danger" type="button" onclick = "return false;">Back</button>
                            </form>
                          </div>     
                   		</div>
                 	</div>
        		</div>
            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>
