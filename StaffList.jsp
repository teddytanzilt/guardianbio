<%@include file="layout/Header.jsp" %>
<%@include file="processor/CheckSession.jsp" %>
<%@include file="processor/CheckRole.jsp" %>

<%@page import="Biometrics.UserManager"%>
<%@page import="Biometrics.TBUSER"%>
<%@page import="Biometrics.TBSTAFF"%>
<%@page import="util.DateFormatConverter"%>

<jsp:useBean id="AccessGroupMgr" scope="page" class="Biometrics.AccessGroupManager" />
<jsp:useBean id="StaffMgr" scope="page" class="Biometrics.StaffManager" />
<jsp:useBean id="UserMgr" scope="page" class="Biometrics.UserManager" />

<%
String CurrentParam = "?"+request.getQueryString();
request.getSession().setAttribute("CurrentParam", CurrentParam);
%>

<% String sTableTitle = "Staff Management"; %>
<% String sPageName = "StaffList.jsp"; %>
<% String sAddPageName = "StaffAdd.jsp"; %>
<% String sEditPageName = "StaffEdit.jsp"; %>
<% String sDeletePageName = "processor/DeleteStaff.jsp"; %>
<% String sSuspendPageName = "processor/SuspendStaff.jsp"; %>
<% String sUnsuspendPageName = "processor/UnsuspendStaff.jsp"; %>

<%!Biometrics.TBSTAFF[] aStaffs;%>
<%!Biometrics.TBACCESSGROUP[] aAccessGroups;%>
<%!String sStaffId; %>
<%!String sStaffName; %>
<%!String sCardNo; %>
<%!String sAccessGroupId; %>
<%!String sDepartmentId; %>
<%!String sRecordStatus; %>
<%!String sTotalRecord; %>
<%!String sOffset; %>
<%!int iPage; %>
<%!int iMaxPage; %>
<%!TBUSER SessionUser; %>
<%!String sCurrentError = ""; %>

<%	
	int SessionUserId = Integer.parseInt(String.valueOf(request.getSession().getAttribute("SessionLoginID")));

	try {
		if (DBMgr.getConnection() == null) { 
			if (!DBMgr.openConnection()) {
				request.getSession().setAttribute("LastErrMsg", "Failed to open connection");
				response.sendRedirect(request.getContextPath() + "/Login.jsp");
				return;
			}
		}
	
		UserMgr.setConnection(DBMgr.getConnection());
		AccessGroupMgr.setConnection(DBMgr.getConnection());
		StaffMgr.setConnection(DBMgr.getConnection());
		SessionUser = UserMgr.getUserById(SessionUserId);
		
		aAccessGroups = AccessGroupMgr.getAccessGroupAllNoOffset();
		
		sStaffId = request.getParameter("txtStaffId");
		sStaffName = request.getParameter("txtStaffName");
		sAccessGroupId = request.getParameter("cbAccessGroupId");
		sCardNo = request.getParameter("txtCardNo");
		sRecordStatus = request.getParameter("cbRecordStatus");
		sOffset = request.getParameter("txtOffset");
		
		if (sStaffId == null || sStaffId.trim().length() <= 0) {
			sStaffId = "";		
		}
		if (sStaffName == null || sStaffName.trim().length() <= 0) {
			sStaffName = "";		
		}
		if (sAccessGroupId == null || sAccessGroupId.trim().length() <= 0) {
			sAccessGroupId = "";		
		}
		if (sCardNo == null || sCardNo.trim().length() <= 0) {
			sCardNo = "";		
		}
		if (sRecordStatus == null || sRecordStatus.trim().length() <= 0) {
			sRecordStatus = "";		
		}
		if (sOffset == null || sOffset.trim().length() <= 0) {
			sOffset = "0";		
		}
		
		if (sStaffId != "" || sAccessGroupId != "" || sStaffName != "" || sRecordStatus != "") {
			aStaffs = StaffMgr.getStaffByFilter(sStaffId,sStaffName,sCardNo,sAccessGroupId,sRecordStatus,sOffset);  
			sTotalRecord = StaffMgr.countStaffByFilter(sStaffId,sStaffName,sCardNo,sAccessGroupId,sRecordStatus) + "";
		} else {
			aStaffs = StaffMgr.getStaffAll(sOffset);
			sTotalRecord = StaffMgr.countStaffAll() + "";
		}
		
		iPage = (Integer.parseInt(sOffset) / 30) + 1;
		if(Integer.parseInt(sTotalRecord) % 30 != 0){
			iMaxPage = (Integer.parseInt(sTotalRecord) / 30) + 1;
		} else {
			iMaxPage = (Integer.parseInt(sTotalRecord) / 30);
		}
		
		if (DBMgr.getConnection() != null){
			if (!DBMgr.getConnection().isClosed())
				DBMgr.closeConnection();
		}
	} catch (Exception e) {
		sCurrentError = e.getMessage();
	}
%>

<link href="assets/css/bootstrap-select.min.css" rel="stylesheet" />
<script src="assets/js/bootstrap-select.min.js"></script>

<script language="javascript" type="text/javascript">
$(function() {
    $(".pageNumber").click(function (){
    	var sNextOffset = (parseInt($(this).html()) - 1) * 30;
    	var TotalRecord = parseInt($("#TotalRecord").val());
    	
    	var sStaffId = $('#txtStaffId').val();
		var sUserName = $('#txtStaffName').val();
		var sAccessGroupId = $('#cbAccessGroupId').val();
		var sRecordStatus = $('#cbRecordStatus').val();
		
		HidePaging();
    	$(this).attr("href", "StaffList.jsp?txtStaffId=" + sStaffId + "&txtStaffName=" + sUserName + "&cbAccessGroupId=" + sAccessGroupId + "&cbRecordStatus=" + sRecordStatus + "&txtOffset=" + sNextOffset + "");    	
     });
    
 });
 
function HidePaging(){
	$(".pageNumber").css("display","none");	
	$(".Next").css("display","none");	
	$(".Prev").css("display","none");	
	$(".currentPage").css("display","none");
	$(".minPageNumber").css("display","none");
	$(".maxPageNumber").css("display","none");
}
 
function IsConfirmedDelete(obj)
{
	return confirm("Are you sure want to delete?");	
}
function IsConfirmedSuspend(obj)
{
	return confirm("Are you sure want to suspend?");	
}
function IsConfirmedUnsuspend(obj)
{
	return confirm("Are you sure want to activate?");	
}
function IsConfirmedImport(obj)
{
	return confirm("Are you sure want to import?");	
}
function IsConfirmedSearch(obj)
{	
	$("#thisForm").attr("action", "StaffList.jsp");
	$("#txtOffset").val("0");
	$("#thisForm").submit();
	return true;
}

function Next(obj)
{	
	var nextLink = document.getElementById("Next");
	var prevLink = document.getElementById("Prev");
		
	var sOffset = parseInt($("#txtOffset").val());
	var TotalRecord = parseInt($("#TotalRecord").val());	
	if(sOffset+10 <=  TotalRecord){
		HidePaging();
		
		var sStaffId = $('#txtStaffId').val();
		var sUserName = $('#txtStaffName').val();
		var sAccessGroupId = $('#cbAccessGroupId').val();
		var sRecordStatus = $('#cbRecordStatus').val();
		
		sOffset = sOffset + 30;

		var targetHref="StaffList.jsp?txtStaffId=" + sStaffId + "&txtStaffName=" + sUserName + "&cbAccessGroupId=" + sAccessGroupId + "&cbRecordStatus=" + sRecordStatus + "&txtOffset=" + sOffset + "";
		$(".Next").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Prev(obj)
{	
	var nextLink = document.getElementById("Next");
	var prevLink = document.getElementById("Prev");
	
	var sOffset = parseInt($("#txtOffset").val());
	if(sOffset > 0){	
		HidePaging();
		
		var sStaffId = $('#txtStaffId').val();
		var sUserName = $('#txtStaffName').val();
		var sAccessGroupId = $('#cbAccessGroupId').val();
		var sRecordStatus = $('#cbRecordStatus').val();
		sOffset = sOffset - 30;

		var targetHref="StaffList.jsp?txtStaffId=" + sStaffId + "&txtStaffName=" + sUserName + "&cbAccessGroupId=" + sAccessGroupId + "&cbRecordStatus=" + sRecordStatus + "&txtOffset=" + sOffset + "";
		$(".Prev").attr("href", targetHref);
		return true;		
	} else {
		return false;
	}
}

function Max(obj)
{
	var sOffset = (parseInt($("#MaxPage").val()) - 1) * 30;
	var TotalRecord = parseInt($("#TotalRecord").val());	
	HidePaging();		
	var sStaffId = $('#txtStaffId').val();
	var sUserName = $('#txtStaffName').val();
	var sAccessGroupId = $('#cbAccessGroupId').val();
	var sRecordStatus = $('#cbRecordStatus').val();
	var targetHref="StaffList.jsp?txtStaffId=" + sStaffId + "&txtStaffName=" + sUserName + "&cbAccessGroupId=" + sAccessGroupId + "&cbRecordStatus=" + sRecordStatus + "&txtOffset=" + sOffset + "";
	$(".maxPageNumber").attr("href", targetHref); 
	return true;
}

function Min(obj)
{
	var TotalRecord = parseInt($("#TotalRecord").val());	
	HidePaging();		
	var sStaffId = $('#txtStaffId').val();
	var sUserName = $('#txtStaffName').val();	
	var sAccessGroupId = $('#cbAccessGroupId').val();
	var sRecordStatus = $('#cbRecordStatus').val();
	var targetHref="StaffList.jsp?txtStaffId=" + sStaffId + "&txtStaffName=" + sUserName + "&cbAccessGroupId=" + sAccessGroupId + "&cbRecordStatus=" + sRecordStatus + "&txtOffset=0";
	$(".minPageNumber").attr("href", targetHref); 
	return true;
}

</script>


<body>
    <div id="wrapper">
        <%@include file="layout/NavTop.jsp" %>
        <%@include file="layout/NavSide.jsp" %>
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line"><%=sTableTitle %></h1>
                    </div>
                </div>
                <!-- /. ROW  -->
                
                <% if (request.getSession().getAttribute("LastErrMsg") != null) { %>
					<div class="alert alert-danger"><%= request.getSession().getAttribute("LastErrMsg") %></div>
					<% request.getSession().setAttribute("LastErrMsg", null); %>
				<% } else { %>
					<% if (sCurrentError != "") {%>
						<div class="alert alert-danger"><%= sCurrentError %></div>
					<% } else { %>
				
					<% } %>
				<% } %>
				
	            <div class="row">
	                <div class="col-md-12">
	                    <!--    Striped Rows Table  -->
	                    <div class="panel panel-default">
	                        <div class="panel-heading">
	                        	<Form id = "thisForm">
	                        	<input type = "hidden" id = "TotalRecord" value = "<%=sTotalRecord%>">
								<input type = "hidden" id = "txtOffset" name="txtOffset"  value="<%= sOffset == null ? "0" : sOffset%>">
								<input type = "hidden" id = "MaxPage" value = "<%=iMaxPage%>">
			
	                            <div class="row">
									<div class="col-md-2">
			                            <div class="input-group">
			                            	<input class="form-control" id = "txtStaffId" name="txtStaffId" type="text" placeholder="Staff Id"  value="<%= sStaffId == null ? "" : sStaffId%>" />
		                                </div>
		                            </div>
                    				<div class="col-md-3">
			                            <div class="input-group">
			                            	<input class="form-control" id = "txtStaffName" name="txtStaffName" type="text" placeholder="Staff Name"  value="<%= sStaffName == null ? "" : sStaffName%>" />
		                                </div>
		                            </div>
									<div class="col-md-3">
			                            <div class="form-group">
			                            	<select class="form-control selectpicker" data-live-search="true" name ="cbAccessGroupId" id = "cbAccessGroupId">
												<option value = "">-</option>
												<%if(aAccessGroups != null){ %>
													<%for (int i = 0; i < aAccessGroups.length; i++) { %>
														<option value = "<%=aAccessGroups[i].getACCESSGROUPID() %>" <%=sAccessGroupId.equals(aAccessGroups[i].getACCESSGROUPID())?"selected":""%>>
															<%=aAccessGroups[i].getACCESSGROUPNAME()%>
														</option>
													<%} %>
												<%} %>
											</select>
		                                </div>
		                            </div>
		                            
		                            <div class="col-md-2">
			                            <div class="form-group">
			                            	<select class="form-control selectpicker" data-live-search="true" name ="cbRecordStatus" id = "cbRecordStatus">
												<option value = "">All</option>
												<option value = "A" <%=sRecordStatus.equals("A")?"selected":""%>>Active</option>
												<option value = "S" <%=sRecordStatus.equals("S")?"selected":""%>>Suspended</option>
											</select>
		                                </div>
		                            </div>
		                            <div class="col-md-1">
			                            <div class="input-group">
		                                    <span class="input-group-btn">
		                                        <button class="btn btn-success" id = "btnSearch" type="button" onclick="return IsConfirmedSearch(this);">Search</button>
		                                    </span>
		                                </div>
		                            </div>
		                      	</div>
		                      	</Form>
	                        </div>
	                        <div class="panel-body">
	                            <div class="table-responsive">
	                                <table class="table table-striped">
	                                    <thead>
	                                        <tr>
	                                            <th>#</th>
												<th>Staff Id</th>
	                                            <th>Staff Name</th>
												<th>Access Group</th>
	                                            <th>Created On</th>
	                                            <th>Modified On</th>
	                                            <th>Action</th>
	                                        </tr>
	                                    </thead>
	                                    <tbody>
	                                    <%
											if (aStaffs != null) {
														
												for (int i = 0; i < aStaffs.length; i++) {
										%>
	                                        <tr>
	                                            <td><%=Integer.parseInt(sOffset)+i+1%></td>
												<td><%=aStaffs[i].getSTAFFID()%></td>
												<td><%=aStaffs[i].getSTAFFNAME() != null ? aStaffs[i].getSTAFFNAME() : "&nbsp;" %></td>
												<td><%=aStaffs[i].getACCESSGROUPNAME() != null ? aStaffs[i].getACCESSGROUPNAME() : "&nbsp;" %></td>
												<td><%=aStaffs[i].getCREATEDSTAMP() != null ? DateFormatConverter.format2(aStaffs[i].getCREATEDSTAMP(), "yyyy-MM-dd HH:mm:ss") : "&nbsp;" %></td>
												<td><%=aStaffs[i].getLASTUPDATETIMEDSTAMP() != null ? DateFormatConverter.format2(aStaffs[i].getLASTUPDATETIMEDSTAMP(), "yyyy-MM-dd HH:mm:ss") : "&nbsp;" %></td>
												<td >
													<a href="<%=sEditPageName %>?Id=<%=aStaffs[i].getID()%>">Edit</a> 	&nbsp; 	&nbsp;
													
													<%if(aStaffs[i].getRECORDSTATUS().equals("A")) { %>
													<a href="<%=sSuspendPageName %>?Id=<%=aStaffs[i].getID()%>" onclick="return IsConfirmedSuspend(this);">Suspend</a> &nbsp; 	&nbsp
													<% } else if(aStaffs[i].getRECORDSTATUS().equals("S")) { %>
													<a href="<%=sUnsuspendPageName %>?Id=<%=aStaffs[i].getID()%>" onclick="return IsConfirmedUnsuspend(this);">Activate</a> &nbsp; 	&nbsp
													<%} %>
													
													<a href="<%=sDeletePageName %>?Id=<%=aStaffs[i].getID()%>" onclick="return IsConfirmedDelete(this);">Delete</a>
												</td>
	                                        </tr>
	                                    <%
												}
											}
										%> 
	                                    </tbody>
	                                </table>
	                                <hr />
                            		<ul class="pagination" style = "float:right;">
                            		<%if(Integer.parseInt(sTotalRecord) != 0) { %>
										<%if(iPage > 1){ %>
											<li><a href="<%=sPageName %>" class = "minPageNumber" onclick = "return Min(this);">First</a></li>
									  		<li><a href="<%=sPageName %>" class = "Prev" onclick="return Prev(this);">&laquo;</a></li>
										<% } %>
										<%if(iPage == 1){ %>
				
										<%} else if(iPage == 2){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber" >1</a></li>
										<%} else { %>
										
										<%if(iPage == iMaxPage){ %>
											<% if(iPage-4 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-4; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else if(iPage == iMaxPage - 1){ %>
											<% if(iPage-3 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-3; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} else { %>
											<% if(iPage-2 < 1){  %>
												<%for(int x = 1; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } else {  %>
												<%for(int x = iPage-2; x < iPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>	
											<% } %>
										<%} %>	
									<%} %>
									<li class="active"><a class = "currentPage"><%=iPage %><span class="sr-only">(current)</span></a></li>
									<%if(iPage == iMaxPage){ %>
				
									<%} else if(iPage == iMaxPage - 1){ %>
										<li><a href="<%=sPageName %>" class = "pageNumber" ><%=iMaxPage %></a></li>
									<%} else { %>
										<% if(iPage == 1){  %>
											<% if(iMaxPage < 5){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 5; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else if(iPage == 2){  %>
											<% if(iMaxPage <= 4){  %>
												<%for(int x = iPage + 1; x <= iMaxPage; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } else {  %>
												<%for(int x = iPage + 1; x < iPage + 4; x++) { %>
													<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
												<% } %>
											<% } %>
										<% } else { %>
											<%for(int x = iPage + 1; x < iPage + 3; x++) { %>
												<li><a href="<%=sPageName %>" class = "pageNumber" ><%=x %></a></li>
											<% } %>
										<% } %>
									<%} %>		
									
									<%if(iPage < iMaxPage){ %>
											<li><a href="<%=sPageName %>" class = "Next" onclick = "return Next(this);">&raquo;</a></li>
									  		<li><a href="<%=sPageName %>" class = "maxPageNumber" onclick="return Max(this);">Last</a></li>
										<% } %>
									<%} %>
									</ul>
	                            
	                            	<a class="btn btn-info" style = "float:left;"  href="<%=sAddPageName %>">Add New</a>
	                            </div>
	                            
	                            
	                            
	                        </div>
	                    </div>
	                    <!--  End  Striped Rows Table  -->
	                </div>
	                
	            </div>
                <!-- /. ROW  -->
           

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
	<%@include file="layout/InnerFooter.jsp" %>
</body>
<%@include file="layout/Footer.jsp" %>