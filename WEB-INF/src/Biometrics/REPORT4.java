package Biometrics;

import java.util.Date;

public class REPORT4 {
	private String STAFFID;
	private String STAFFNAME;
	private String BRANCHID;
	private String BRANCHNAME;
	private String DAY1;
	private String DAT2;
	private String DAT3;
	private String DAY4;
	private String DAT5;
	private String DAT6;
	private String DAY7;
	private String DAT8;
	private String DAT9;
	private String DAY10;
	private String DAY11;
	private String DAT12;
	private String DAT13;
	private String DAY14;
	private String DAT15;
	private String DAT16;
	private String DAY17;
	private String DAT18;
	private String DAT19;
	private String DAY20;
	private String DAY21;
	private String DAT22;
	private String DAT23;
	private String DAY24;
	private String DAT25;
	private String DAT26;
	private String DAY27;
	private String DAT28;
	private String DAT29;
	private String DAY30;
	private String DAY31;
	
	public String getSTAFFID() {
		return STAFFID;
	}
	public void setSTAFFID(String sTAFFID) {
		STAFFID = sTAFFID;
	}
	public String getSTAFFNAME() {
		return STAFFNAME;
	}
	public void setSTAFFNAME(String sTAFFNAME) {
		STAFFNAME = sTAFFNAME;
	}
	public String getBRANCHID() {
		return BRANCHID;
	}
	public void setBRANCHID(String bRANCHID) {
		BRANCHID = bRANCHID;
	}
	public String getBRANCHNAME() {
		return BRANCHNAME;
	}
	public void setBRANCHNAME(String bRANCHNAME) {
		BRANCHNAME = bRANCHNAME;
	}
	public String getDAY1() {
		return DAY1;
	}
	public void setDAY1(String dAY1) {
		DAY1 = dAY1;
	}
	public String getDAT2() {
		return DAT2;
	}
	public void setDAT2(String dAT2) {
		DAT2 = dAT2;
	}
	public String getDAT3() {
		return DAT3;
	}
	public void setDAT3(String dAT3) {
		DAT3 = dAT3;
	}
	public String getDAY4() {
		return DAY4;
	}
	public void setDAY4(String dAY4) {
		DAY4 = dAY4;
	}
	public String getDAT5() {
		return DAT5;
	}
	public void setDAT5(String dAT5) {
		DAT5 = dAT5;
	}
	public String getDAT6() {
		return DAT6;
	}
	public void setDAT6(String dAT6) {
		DAT6 = dAT6;
	}
	public String getDAY7() {
		return DAY7;
	}
	public void setDAY7(String dAY7) {
		DAY7 = dAY7;
	}
	public String getDAT8() {
		return DAT8;
	}
	public void setDAT8(String dAT8) {
		DAT8 = dAT8;
	}
	public String getDAT9() {
		return DAT9;
	}
	public void setDAT9(String dAT9) {
		DAT9 = dAT9;
	}
	public String getDAY10() {
		return DAY10;
	}
	public void setDAY10(String dAY10) {
		DAY10 = dAY10;
	}
	public String getDAY11() {
		return DAY11;
	}
	public void setDAY11(String dAY11) {
		DAY11 = dAY11;
	}
	public String getDAT12() {
		return DAT12;
	}
	public void setDAT12(String dAT12) {
		DAT12 = dAT12;
	}
	public String getDAT13() {
		return DAT13;
	}
	public void setDAT13(String dAT13) {
		DAT13 = dAT13;
	}
	public String getDAY14() {
		return DAY14;
	}
	public void setDAY14(String dAY14) {
		DAY14 = dAY14;
	}
	public String getDAT15() {
		return DAT15;
	}
	public void setDAT15(String dAT15) {
		DAT15 = dAT15;
	}
	public String getDAT16() {
		return DAT16;
	}
	public void setDAT16(String dAT16) {
		DAT16 = dAT16;
	}
	public String getDAY17() {
		return DAY17;
	}
	public void setDAY17(String dAY17) {
		DAY17 = dAY17;
	}
	public String getDAT18() {
		return DAT18;
	}
	public void setDAT18(String dAT18) {
		DAT18 = dAT18;
	}
	public String getDAT19() {
		return DAT19;
	}
	public void setDAT19(String dAT19) {
		DAT19 = dAT19;
	}
	public String getDAY20() {
		return DAY20;
	}
	public void setDAY20(String dAY20) {
		DAY20 = dAY20;
	}
	public String getDAY21() {
		return DAY21;
	}
	public void setDAY21(String dAY21) {
		DAY21 = dAY21;
	}
	public String getDAT22() {
		return DAT22;
	}
	public void setDAT22(String dAT22) {
		DAT22 = dAT22;
	}
	public String getDAT23() {
		return DAT23;
	}
	public void setDAT23(String dAT23) {
		DAT23 = dAT23;
	}
	public String getDAY24() {
		return DAY24;
	}
	public void setDAY24(String dAY24) {
		DAY24 = dAY24;
	}
	public String getDAT25() {
		return DAT25;
	}
	public void setDAT25(String dAT25) {
		DAT25 = dAT25;
	}
	public String getDAT26() {
		return DAT26;
	}
	public void setDAT26(String dAT26) {
		DAT26 = dAT26;
	}
	public String getDAY27() {
		return DAY27;
	}
	public void setDAY27(String dAY27) {
		DAY27 = dAY27;
	}
	public String getDAT28() {
		return DAT28;
	}
	public void setDAT28(String dAT28) {
		DAT28 = dAT28;
	}
	public String getDAT29() {
		return DAT29;
	}
	public void setDAT29(String dAT29) {
		DAT29 = dAT29;
	}
	public String getDAY30() {
		return DAY30;
	}
	public void setDAY30(String dAY30) {
		DAY30 = dAY30;
	}
	public String getDAY31() {
		return DAY31;
	}
	public void setDAY31(String dAY31) {
		DAY31 = dAY31;
	}
	
}
