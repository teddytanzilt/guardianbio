package Biometrics;

import java.util.Date;

public class TBUSER {
	private int ID;
	private String USERNAME;
	private String PASSWORD;
	private String NAME;
	private String ROLE;
	private String RECORDSTATUS;
	private String CREATEDBY;
	private Date CREATEDSTAMP;
	private String LASTUPDATETIMEDBY;
	private Date LASTUPDATETIMEDSTAMP;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getUSERNAME() {return USERNAME;}
	public void setUSERNAME(String USERNAME) {this.USERNAME = USERNAME;}
	
	public String getPASSWORD() {return PASSWORD;}
	public void setPASSWORD(String PASSWORD) {this.PASSWORD = PASSWORD;}
	
	public String getNAME() {return NAME;}
	public void setNAME(String NAME) {this.NAME = NAME;}
	
	public String getROLE() {return ROLE;}
	public void setROLE(String ROLE) {this.ROLE = ROLE;}
	
	public String getRECORDSTATUS() {return RECORDSTATUS;}
	public void setRECORDSTATUS(String RECORDSTATUS) {this.RECORDSTATUS = RECORDSTATUS;}
	
	public String getCREATEDBY() {return CREATEDBY;}
	public void setCREATEDBY(String CREATEDBY) {this.CREATEDBY = CREATEDBY;}
	
	public Date getCREATEDSTAMP() {return CREATEDSTAMP;}
	public void setCREATEDSTAMP(Date CREATEDSTAMP) {this.CREATEDSTAMP = CREATEDSTAMP;}
	
	public String getLASTUPDATETIMEDBY() {return LASTUPDATETIMEDBY;}
	public void setLASTUPDATETIMEDBY(String LASTUPDATETIMEDBY) {this.LASTUPDATETIMEDBY = LASTUPDATETIMEDBY;}
	
	public Date getLASTUPDATETIMEDSTAMP() {return LASTUPDATETIMEDSTAMP;}
	public void setLASTUPDATETIMEDSTAMP(Date LASTUPDATETIMEDSTAMP) {this.LASTUPDATETIMEDSTAMP = LASTUPDATETIMEDSTAMP;}
	
}
