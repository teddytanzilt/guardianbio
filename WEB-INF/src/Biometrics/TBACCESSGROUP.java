package Biometrics;

import java.util.Date;

public class TBACCESSGROUP {
	private int ID;
	private String ACCESSGROUPID;
	private String ACCESSGROUPNAME;
	private String RECORDSTATUS;
	private String CREATEDBY;
	private Date CREATEDSTAMP;
	private String LASTUPDATETIMEDBY;
	private Date LASTUPDATETIMEDSTAMP;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getACCESSGROUPID() {return ACCESSGROUPID;}
	public void setACCESSGROUPID(String ACCESSGROUPID) {this.ACCESSGROUPID = ACCESSGROUPID;}
	
	public String getACCESSGROUPNAME() {return ACCESSGROUPNAME;}
	public void setACCESSGROUPNAME(String ACCESSGROUPNAME) {this.ACCESSGROUPNAME = ACCESSGROUPNAME;}
	
	public String getRECORDSTATUS() {return RECORDSTATUS;}
	public void setRECORDSTATUS(String RECORDSTATUS) {this.RECORDSTATUS = RECORDSTATUS;}
	
	public String getCREATEDBY() {return CREATEDBY;}
	public void setCREATEDBY(String CREATEDBY) {this.CREATEDBY = CREATEDBY;}
	
	public Date getCREATEDSTAMP() {return CREATEDSTAMP;}
	public void setCREATEDSTAMP(Date CREATEDSTAMP) {this.CREATEDSTAMP = CREATEDSTAMP;}
	
	public String getLASTUPDATETIMEDBY() {return LASTUPDATETIMEDBY;}
	public void setLASTUPDATETIMEDBY(String LASTUPDATETIMEDBY) {this.LASTUPDATETIMEDBY = LASTUPDATETIMEDBY;}
	
	public Date getLASTUPDATETIMEDSTAMP() {return LASTUPDATETIMEDSTAMP;}
	public void setLASTUPDATETIMEDSTAMP(Date LASTUPDATETIMEDSTAMP) {this.LASTUPDATETIMEDSTAMP = LASTUPDATETIMEDSTAMP;}
	
}
