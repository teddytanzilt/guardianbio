package Biometrics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class AttendanceManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public TBATTENDANCE getAttendanceById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE ID = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "A");
		
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		return alAttendance.get(0);		
	}
	
	public TBATTENDANCE getAttendanceByStaffId(String STAFFID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE UPPER(STAFFID) = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, STAFFID.toUpperCase());
		
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		return alAttendance.get(0);		
	}
	
	public TBATTENDANCE getAttendanceByBranchId(String BRANCHID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE UPPER(BRANCHID) = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, BRANCHID.toUpperCase());
		
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		return alAttendance.get(0);		
	}
	
	public TBATTENDANCE getAttendanceByDeviceId(String DEVICEID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBATTENDANCE WHERE UPPER(DEVICEID) = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, DEVICEID.toUpperCase());
		
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		return alAttendance.get(0);		
	}	
	
	public TBATTENDANCE[] getAttendanceAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBATTENDANCE";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    		
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ATTENDANCEDATE DESC, ATTENDANCETIME DESC) as RowNum " +
    					"FROM "+tableName+") TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		TBATTENDANCE[] areas = new TBATTENDANCE[alAttendance.size()];

		alAttendance.toArray(areas);

		return areas;
	}
	
	public int countAttendanceAll() throws SQLException {
    	String sqlStmt = "SELECT * FROM TBATTENDANCE ";        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBATTENDANCE[] getAttendanceByFilter(String STAFFID, String BRANCHID, String DEVICEID, String offset) throws SQLException {
    	String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String itemPerPage = "30";
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(DEVICEID) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	} 
    	String tableName = "TBATTENDANCE";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ATTENDANCEDATE DESC, ATTENDANCETIME DESC) as RowNum FROM "+tableName+" " +
    					"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + ") TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        
        ArrayList<TBATTENDANCE> alAttendance = new ArrayList<TBATTENDANCE>();
        alAttendance = this.getAttendanceData(pStmt);
        TBATTENDANCE[] areas = new TBATTENDANCE[alAttendance.size()];
		alAttendance.toArray(areas);
		return areas;
    }
	
	public int countAttendanceByFilter(String STAFFID, String BRANCHID, String DEVICEID) throws SQLException {
		String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(DEVICEID) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	} 
    	
        String sqlStmt = "SELECT * FROM TBATTENDANCE " + 
        				"WHERE " +  StaffIdFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public TBATTENDANCE[] getAttendanceAllNoOffset() throws SQLException {
    	String tableName = "TBATTENDANCE";
    	String sqlStmt = "SELECT * FROM "+tableName+" ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	
		ArrayList<TBATTENDANCE> alAttendance;

		alAttendance = this.getAttendanceData(pStmt);

		if (alAttendance.size() == 0)
			return null;

		TBATTENDANCE[] areas = new TBATTENDANCE[alAttendance.size()];

		alAttendance.toArray(areas);

		return areas;
	}

	public ArrayList<TBATTENDANCE> getAttendanceData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBATTENDANCE> alAttendance = new ArrayList<TBATTENDANCE>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBATTENDANCE oAttendance = new TBATTENDANCE();
			oAttendance.setID(srs.getInt("ID"));
			oAttendance.setDEVICEID(srs.getString("DEVICEID"));
			oAttendance.setBRANCHID(srs.getString("BRANCHID"));
			oAttendance.setSTAFFID(srs.getString("STAFFID"));
			oAttendance.setINOUT(srs.getString("INOUT"));
			oAttendance.setATTENDANCEDATE(srs.getString("ATTENDANCEDATE"));
			oAttendance.setATTENDANCETIME(srs.getString("ATTENDANCETIME"));
			alAttendance.add(oAttendance);
		}

		return alAttendance;
	}
	
	public TBREPLTRANS[] getLogByFilter(String STAFFID, String BRANCHID, String DEVICEID, String Action, String LogDate, String offset) throws SQLException {
    	String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String ActionFilter = "1 = 1";
    	String DateFilter = "1 = 1";
    	
    	String itemPerPage = "30";
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBREPLTRANS.StaffId) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBREPLTRANS.DeviceId) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	}
    	if(Action != "" && Action.trim().length() > 0){
    		ActionFilter = "TBREPLTRANS.RType = '" + Action + "'";	
    	}  
    	if(LogDate != "" && LogDate.trim().length() > 0){
    		DateFilter = " CONVERT(VARCHAR, Logdate , 112) = '" + LogDate + "'";	
    	}
    	
    	String tableName = "TBREPLTRANS";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select TBREPLTRANS.*, TBDEVICE.BRANCHID, ROW_NUMBER() over (order by Logdate DESC) as RowNum FROM "+tableName+" " +
    					"LEFT JOIN TBDEVICE ON (TBDEVICE.DEVICEID = TBREPLTRANS.DeviceId) " +
    					"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + ActionFilter + " AND " + DateFilter + ") TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        
        ArrayList<TBREPLTRANS> alAttendance = new ArrayList<TBREPLTRANS>();
        alAttendance = this.getLogData(pStmt);
        TBREPLTRANS[] areas = new TBREPLTRANS[alAttendance.size()];
		alAttendance.toArray(areas);
		return areas;
    }
	
	public int countLogByFilter(String STAFFID, String BRANCHID, String DEVICEID, String Action, String LogDate) throws SQLException {
		String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String DeviceIdFilter = "1 = 1";
    	String ActionFilter = "1 = 1";
    	String DateFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBREPLTRANS.StaffId) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) LIKE ('%" + BRANCHID.toUpperCase() + "%')";	
    	} 
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBREPLTRANS.DeviceId) LIKE ('%" + DEVICEID.toUpperCase() + "%')";	
    	} 
    	if(Action != "" && Action.trim().length() > 0){
    		ActionFilter = "TBREPLTRANS.RType = '" + Action + "'";	
    	}  
    	if(LogDate != "" && LogDate.trim().length() > 0){
    		DateFilter = " CONVERT(VARCHAR, Logdate , 112) = '" + LogDate + "'";
    	}
    	
        String sqlStmt = "SELECT TBREPLTRANS.*, TBDEVICE.BRANCHID FROM TBREPLTRANS " + 
        				"LEFT JOIN TBDEVICE ON (TBDEVICE.DEVICEID = TBREPLTRANS.DeviceId) " + 
        				"WHERE " +  StaffIdFilter + " AND " + BranchIdFilter + " AND " + DeviceIdFilter + " AND " + ActionFilter + " AND " + DateFilter + " ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public ArrayList<TBREPLTRANS> getLogData(PreparedStatement pStmt) throws SQLException {
		
		ArrayList<TBREPLTRANS> alAttendance = new ArrayList<TBREPLTRANS>();
		
		ResultSet srs = pStmt.executeQuery();
		
		while (srs.next()) {
			TBREPLTRANS oAttendance = new TBREPLTRANS();
			oAttendance.setRecordId(srs.getInt("RecordId"));
			oAttendance.setDeviceId(srs.getString("DeviceId"));
			oAttendance.setStaffId(srs.getString("StaffId"));
			oAttendance.setFingerIndex(srs.getInt("FingerIndex"));
			oAttendance.setRType(srs.getString("RType"));
			oAttendance.setLogdate(srs.getString("Logdate"));
			oAttendance.setExtInfo(srs.getString("ExtInfo"));
			oAttendance.setBRANCHID(srs.getString("BRANCHID"));
			
			alAttendance.add(oAttendance);
		}
		
		return alAttendance;
	}
	
	public REPORT1[] genReport1View(String STAFFID, String BRANCHID, String START, String END, String offset) throws SQLException {
		String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	String itemPerPage = "30";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBACCESSGROUPDETAIL.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    			
    	} 
    	if(END != "" && END.trim().length() > 0){
    			
    	} 
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
					"(SELECT ROW_NUMBER() over (order by TBSTAFF.STAFFID, TBSTAFF.STAFFNAME, ISNULL(THUMB.THUMBCOUNT,''), TBSTAFF.RECORDSTATUS) as RowNum, " + 
					"TBSTAFF.STAFFID, TBSTAFF.STAFFNAME, ISNULL(THUMB.THUMBCOUNT,'') AS THUMBCOUNT, TBSTAFF.RECORDSTATUS, TBACCESSGROUPDETAIL.BRANCHID " + 
					"FROM TBSTAFF " + 
					"LEFT JOIN (SELECT STAFFID, COUNT(*) THUMBCOUNT FROM TBSTAFFTHUMB GROUP BY STAFFID) THUMB ON (THUMB.STAFFID = TBSTAFF.STAFFID)" +
					"LEFT JOIN TBACCESSGROUPDETAIL ON (TBACCESSGROUPDETAIL.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID) " + 
					"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + StartFilter + " AND " + EndFilter + ") TableName " + 
		"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";

    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);        
        ArrayList<REPORT1> alReport = new ArrayList<REPORT1>();
        ResultSet srs = pStmt.executeQuery();
        
		while (srs.next()) {
			REPORT1 oReport = new REPORT1();
			oReport.setSTAFFID(srs.getString("STAFFID"));
			oReport.setSTAFFNAME(srs.getString("STAFFNAME"));
			oReport.setREGISTERED(srs.getString("THUMBCOUNT"));
			oReport.setSTATUS(srs.getString("RECORDSTATUS"));
			alReport.add(oReport);
		}

		REPORT1[] reports = new REPORT1[alReport.size()];
		alReport.toArray(reports);
		return reports;
	}
	
	public int countReport1View(String STAFFID, String BRANCHID, String START, String END) throws SQLException {
		String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBACCESSGROUPDETAIL.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    			
    	} 
    	if(END != "" && END.trim().length() > 0){
    			
    	} 
    	
        String sqlStmt = "SELECT " + 
					"TBSTAFF.STAFFID, TBSTAFF.STAFFNAME, ISNULL(THUMB.THUMBCOUNT,'') AS THUMBCOUNT, TBSTAFF.RECORDSTATUS, TBACCESSGROUPDETAIL.BRANCHID " + 
					"FROM TBSTAFF " + 
					"LEFT JOIN (SELECT STAFFID, COUNT(*) THUMBCOUNT FROM TBSTAFFTHUMB GROUP BY STAFFID) THUMB ON (THUMB.STAFFID = TBSTAFF.STAFFID)" +
					"LEFT JOIN TBACCESSGROUPDETAIL ON (TBACCESSGROUPDETAIL.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID) " + 
					"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + StartFilter + " AND " + EndFilter + "";

        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public REPORT2[] genReport2View(String STAFFID, String BRANCHID, String START, String END, String offset) throws SQLException {
		String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	String itemPerPage = "30";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "ATTENDANCEDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "ATTENDANCEDATE <= " + END + "";
    	} 
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
	    	"(SELECT " +
		    	"ROW_NUMBER() over (order by ATTENDANCEDATE,ATTENDANCETIME DESC) as RowNum , * " +
		    	"FROM " +
		    	"( " +
		    		"SELECT " +
		    		"TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " +
		    		"TBATTENDANCE.DEVICEID, TBDEVICE.DEVICENAME, " +
		    		"TBATTENDANCE.BRANCHID, TBBRANCH.BRANCHNAME, " +
		    		"TBATTENDANCE.ATTENDANCEDATE, " +
		    		"TBATTENDANCE.ATTENDANCETIME, " +
		    		"TBATTENDANCE.INOUT " +
		    		"FROM TBATTENDANCE " +
		    		"LEFT JOIN TBSTAFF ON (TBATTENDANCE.STAFFID = TBSTAFF.STAFFID AND TBSTAFF.RECORDSTATUS = 'A') " + 
		    		"LEFT JOIN TBDEVICE ON (TBATTENDANCE.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " + 
		    		"LEFT JOIN TBBRANCH ON (TBATTENDANCE.BRANCHID = TBBRANCH.BRANCHID AND TBBRANCH.RECORDSTATUS = 'A') " +
		    	") AS TheTables " +
		    	"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + StartFilter + " AND " + EndFilter +
		    ") TableName " +			
		"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";

    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);        
        ArrayList<REPORT2> alReport = new ArrayList<REPORT2>();
        ResultSet srs = pStmt.executeQuery();
        
		while (srs.next()) {
			REPORT2 oReport = new REPORT2();
			
			oReport.setSTAFFID(srs.getString("STAFFID"));
			oReport.setSTAFFNAME(srs.getString("STAFFNAME"));
			oReport.setDEVICEID(srs.getString("DEVICEID"));
			oReport.setDEVICENAME(srs.getString("DEVICENAME"));
			oReport.setBRANCHID(srs.getString("BRANCHID"));
			oReport.setBRANCHNAME(srs.getString("BRANCHNAME"));
			oReport.setATTENDANCEDATE(srs.getString("ATTENDANCEDATE"));
			oReport.setATTENDANCETIME(srs.getString("ATTENDANCETIME"));
			oReport.setINOUT(srs.getString("INOUT"));
			alReport.add(oReport);
		}

		REPORT2[] reports = new REPORT2[alReport.size()];
		alReport.toArray(reports);
		return reports;
	}
	
	public int countReport2View(String STAFFID, String BRANCHID, String START, String END) throws SQLException {
		String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBATTENDANCE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "ATTENDANCEDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "ATTENDANCEDATE <= " + END + "";
    	}
    	
    	String sqlStmt = "SELECT " +
		    		"TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " +
		    		"TBATTENDANCE.DEVICEID, TBDEVICE.DEVICENAME, " +
		    		"TBATTENDANCE.BRANCHID, TBBRANCH.BRANCHNAME, " +
		    		"TBATTENDANCE.ATTENDANCEDATE, " +
		    		"TBATTENDANCE.ATTENDANCETIME, " +
		    		"TBATTENDANCE.INOUT " +
		    		"FROM TBATTENDANCE " +
		    		"LEFT JOIN TBSTAFF ON (TBATTENDANCE.STAFFID = TBSTAFF.STAFFID AND TBSTAFF.RECORDSTATUS = 'A') " + 
		    		"LEFT JOIN TBDEVICE ON (TBATTENDANCE.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " + 
		    		"LEFT JOIN TBBRANCH ON (TBATTENDANCE.BRANCHID = TBBRANCH.BRANCHID AND TBBRANCH.RECORDSTATUS = 'A') " +
		    		"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + StartFilter + " AND " + EndFilter + "";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public REPORT3[] genReport3View(String STAFFID, String BRANCHID, String START, String END, String offset) throws SQLException {
		String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	String itemPerPage = "30";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    			
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "TBDEVICEDOWNTIME.EVENTDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "TBDEVICEDOWNTIME.EVENTDATE <= " + END + "";
    	} 
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
					"(SELECT " +
					"ROW_NUMBER() over (order by TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME DESC) as RowNum, " + 
					"TBDEVICEDOWNTIME.DEVICEID, TBDEVICE.DEVICENAME, " +
					"TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME, " + 
					"TBDEVICEDOWNTIME.STATUS " +
					"FROM TBDEVICEDOWNTIME " +
					"LEFT JOIN TBDEVICE ON (TBDEVICEDOWNTIME.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " + 
					"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + StartFilter + " AND " + EndFilter + ") TableName " + 
		"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";

    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);        
        ArrayList<REPORT3> alReport = new ArrayList<REPORT3>();
        ResultSet srs = pStmt.executeQuery();
        
		while (srs.next()) {
			REPORT3 oReport = new REPORT3();
			oReport.setDEVICEID(srs.getString("DEVICEID"));
			oReport.setDEVICENAME(srs.getString("DEVICENAME"));
			oReport.setEVENTDATE(srs.getString("EVENTDATE"));
			oReport.setEVENTTIME(srs.getString("EVENTTIME"));
			oReport.setSTATUS(srs.getString("STATUS"));
			alReport.add(oReport);
		}

		REPORT3[] reports = new REPORT3[alReport.size()];
		alReport.toArray(reports);
		return reports;
	}
	
	public int countReport3View(String STAFFID, String BRANCHID, String START, String END) throws SQLException {
		String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    			
    	}
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	} 
    	if(START != "" && START.trim().length() > 0){
    		StartFilter = "TBDEVICEDOWNTIME.EVENTDATE >= " + START + "";
    	} 
    	if(END != "" && END.trim().length() > 0){
    		EndFilter = "TBDEVICEDOWNTIME.EVENTDATE <= " + END + "";
    	} 
    	
        String sqlStmt = "SELECT " +
					"TBDEVICEDOWNTIME.DEVICEID, TBDEVICE.DEVICENAME, " +
					"TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME, " + 
					"TBDEVICEDOWNTIME.STATUS " +
					"FROM TBDEVICEDOWNTIME " +
					"LEFT JOIN TBDEVICE ON (TBDEVICEDOWNTIME.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " + 
					"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + StartFilter + " AND " + EndFilter + "";

        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public String ExportReport(String masterPath, String reportType, String STAFFID, String BRANCHID, String START, String END) throws SQLException {
   	 	String outputName = "";
   	 	DataOutputStream outData = null;     
   	 	Calendar cal = Calendar.getInstance();
   	 	cal.add(Calendar.DATE, -1);
        Date BackupDate = cal.getTime();
        String sExportId = util.DateFormatConverter.format(BackupDate, "yyyyMMdd"); 
        //String sOutputFolder = masterPath+"\\";
        String sOutputFolder = masterPath;
        
        String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
        
        try {
            this.createFolder(new File(sOutputFolder));        
            String sqlStmt = "";
            
            if(reportType.equals("1")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		BranchIdFilter = "UPPER(TBACCESSGROUPDETAIL.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
            	} 
            	if(START != "" && START.trim().length() > 0){
            			
            	} 
            	if(END != "" && END.trim().length() > 0){
            			
            	} 
            	sqlStmt = "SELECT TBSTAFF.STAFFID, TBSTAFF.STAFFNAME, ISNULL(THUMB.THUMBCOUNT,'') AS THUMBCOUNT, TBSTAFF.RECORDSTATUS FROM TBSTAFF LEFT JOIN (SELECT STAFFID, COUNT(*) THUMBCOUNT FROM TBSTAFFTHUMB GROUP BY STAFFID) THUMB ON (THUMB.STAFFID = TBSTAFF.STAFFID) " + 
            			"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + StartFilter + " AND " + EndFilter + " ";
            } else if (reportType.equals("2")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            		StaffIdFilter = "UPPER(TBATTENDANCE.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		BranchIdFilter = "UPPER(TBATTENDANCE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
            	} 
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "ATTENDANCEDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "ATTENDANCEDATE <= " + END + "";
            	} 
            	
            	sqlStmt = "SELECT TBATTENDANCE.STAFFID, TBSTAFF.STAFFNAME, " + 
						"TBATTENDANCE.DEVICEID, TBDEVICE.DEVICENAME, " +				
						"TBATTENDANCE.BRANCHID, TBBRANCH.BRANCHNAME, " +
						"TBATTENDANCE.ATTENDANCEDATE, " +
						"TBATTENDANCE.ATTENDANCETIME, " +
						"TBATTENDANCE.INOUT " +
						"FROM TBATTENDANCE  " +
						"LEFT JOIN TBSTAFF ON (TBATTENDANCE.STAFFID = TBSTAFF.STAFFID AND TBSTAFF.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBDEVICE ON (TBATTENDANCE.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " +
						"LEFT JOIN TBBRANCH ON (TBATTENDANCE.BRANCHID = TBBRANCH.BRANCHID AND TBBRANCH.RECORDSTATUS = 'A') " +
						"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
						"ORDER BY TBATTENDANCE.ATTENDANCEDATE,TBATTENDANCE.ATTENDANCETIME ";
            } else if (reportType.equals("3")){
            	if(STAFFID != "" && STAFFID.trim().length() > 0){
            			
            	}
            	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
            		
            	} 
            	if(START != "" && START.trim().length() > 0){
            		StartFilter = "TBDEVICEDOWNTIME.EVENTDATE >= " + START + "";
            	} 
            	if(END != "" && END.trim().length() > 0){
            		EndFilter = "TBDEVICEDOWNTIME.EVENTDATE <= " + END + "";
            	} 
            	
            	sqlStmt = "SELECT TBDEVICEDOWNTIME.DEVICEID, TBDEVICE.DEVICENAME, " + 				
						"TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME, " + 
						"TBDEVICEDOWNTIME.STATUS " +  
						"FROM TBDEVICEDOWNTIME " + 
						"LEFT JOIN TBDEVICE ON (TBDEVICEDOWNTIME.DEVICEID = TBDEVICE.DEVICEID AND TBDEVICE.RECORDSTATUS = 'A') " +
						"WHERE " + StaffIdFilter + " AND " + BranchIdFilter + " AND " + StartFilter + " AND " + EndFilter + " " +
						"ORDER BY TBDEVICEDOWNTIME.EVENTDATE, TBDEVICEDOWNTIME.EVENTTIME";
            }
            
            PreparedStatement pSelectStmt = this.connection.prepareStatement(sqlStmt);
            //pSelectStmt.setInt(1, 1);
            ResultSet srs = pSelectStmt.executeQuery();
            
            FileOutputStream outfstreamData = null;
            
            if(reportType.equals("1")){
            	outfstreamData = new FileOutputStream(sOutputFolder + "Staff Register Report " + sExportId + ".csv");
            	outputName = "Staff Register Report " + sExportId + ".csv";
          		outData = new DataOutputStream(outfstreamData);
          		
                outData.writeBytes("" + "STAFF ID" + ","  + 
            			"STAFF NAME" + "," + 
            			"REGISTERED" + "," + 
            			"STATUS" + "\r\n");
                
                while (srs.next()) {
    	           	 String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
    	           	 String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
    	           	 String registered = "No";
    	           	 if(srs.getInt("THUMBCOUNT") > 0){
    	           		registered = "Yes";
    	           	 }
    	           	 String status = (srs.getString("RECORDSTATUS") == null)?"-":srs.getString("RECORDSTATUS");
    	           	 
    	       		 outData.writeBytes("" + staff_id + "," + 
    	       				staff_name + "," + 
    	       				registered + "," +
    	       				status + "\r\n");
                }
            } else if (reportType.equals("2")){
            	outfstreamData = new FileOutputStream(sOutputFolder + "Attendance Report " + sExportId + ".csv");
            	outputName = "Attendance Report " + sExportId + ".csv";
          		outData = new DataOutputStream(outfstreamData);
          		
                outData.writeBytes("" + "STAFF ID" + ","  + 
            			"STAFF NAME" + "," + 
            			"DEVICE ID" + "," + 
            			"DEVICE NAME" + "," +
            			"BRANCH ID" + "," + 
            			"BRANCH NAME" + "," + 
            			"DATE" + "," + 
            			"TIME" + "," + 
            			"IN OUT" + "\r\n");
                
                while (srs.next()) {
    	           	 String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
    	           	 String staff_name = (srs.getString("STAFFNAME") == null)?"-":srs.getString("STAFFNAME");
    	           	 String device_id = (srs.getString("DEVICEID") == null)?"-":srs.getString("DEVICEID");
    	           	 String device_name = (srs.getString("DEVICENAME") == null)?"-":srs.getString("DEVICENAME");
    	           	 String branch_id = (srs.getString("BRANCHID") == null)?"-":srs.getString("BRANCHID");
    	           	 String branch_name = (srs.getString("BRANCHNAME") == null)?"-":srs.getString("BRANCHNAME");
    	           	 String atendance_date = (srs.getString("ATTENDANCEDATE") == null)?"-":srs.getString("ATTENDANCEDATE");
    	           	 String atendance_time = (srs.getString("ATTENDANCETIME") == null)?"0":srs.getString("ATTENDANCETIME");
    	           	 String in_out = (srs.getString("INOUT") == null)?"-":srs.getString("INOUT");
    	           	 
    	       		 outData.writeBytes("" + staff_id + "," + 
    	       				staff_name + "," + 
    	       				device_id + "," +
    	       				device_name + "," + 
    	       				branch_id + "," + 
    	       				branch_name + "," + 
    	       				atendance_date + "," + 
    	       				atendance_time + "," + 
    	       				in_out + "\r\n");
                }
            } else if (reportType.equals("3")){
            	outfstreamData = new FileOutputStream(sOutputFolder + "Device Downtime Report " + sExportId + ".csv");
            	outputName = "Device Downtime Report " + sExportId + ".csv";
          		outData = new DataOutputStream(outfstreamData);
          		
          		outData.writeBytes("" + "DEVICE ID" + ","  + 
            			"DEVICE NAME" + "," +
            			"DATE" + "," + 
            			"TIME" + "," + 
            			"STATUS" + "\r\n");
                
                while (srs.next()) {
    	           	 String device_id = (srs.getString("DEVICEID") == null)?"-":srs.getString("DEVICEID");
    	           	 String device_name = (srs.getString("DEVICENAME") == null)?"-":srs.getString("DEVICENAME");
    	           	 String event_date = (srs.getString("EVENTDATE") == null)?"-":srs.getString("EVENTDATE");
    	           	 String event_time = (srs.getString("EVENTTIME") == null)?"-":srs.getString("EVENTTIME");
    	           	 String status = (srs.getString("STATUS") == null)?"-":srs.getString("STATUS");
    	           	 if(status.equals("1")){
    	           		status = "Up";
    	           	 } else {
    	           		 status = "Down";    	           		 
    	           	 }
    	           	 
    	       		 outData.writeBytes("" + device_id + "," + 
    	       				device_name + "," + 
    	       				event_date + "," + 
    	       				event_time + "," + 
    	       				status + "\r\n");
                }
            }
            
            if(outData != null ){
           	 	outData.flush();        
            	outData.close();
            }
        }catch (Exception e){//Catch exception if any       
       	 errorMessage = e.getMessage();
       	 outputName = "";
        }    	
        return outputName;
   }

	public String ExportAttendance(String masterPath, String START, String END, String STAFFID, String BRANCHID) throws SQLException {
   	 	String outputName = "";
   	 	DataOutputStream outData = null;     
   	 	Calendar cal = Calendar.getInstance();
   	 	cal.add(Calendar.DATE, -1);
        Date BackupDate = cal.getTime();
        String sExportId = util.DateFormatConverter.format(BackupDate, "ddMMyyyy"); 
        //String sOutputFolder = masterPath+"\\";
        String sOutputFolder = masterPath;
        
    	String StartFilter = "1 = 1";
    	String EndFilter = "1 = 1";
    	String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
        
        try {
            this.createFolder(new File(sOutputFolder));        
            String sqlStmt = "";

        	if(START != "" && START.trim().length() > 0){
        		StartFilter = "ATTENDANCEDATE >= " + START + "";
        	} 
        	if(END != "" && END.trim().length() > 0){
        		EndFilter = "ATTENDANCEDATE <= " + END + "";
        	}  
        	if(STAFFID != "" && STAFFID.trim().length() > 0){
        		StaffIdFilter = "STAFFID = '" + STAFFID + "'";
        	} 
        	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
        		BranchIdFilter = "BRANCHID = '" + BRANCHID + "'";
        	} 
        	sqlStmt = "SELECT STAFFID, DEVICEID, BRANCHID, INOUT, ATTENDANCEDATE, ATTENDANCETIME FROM TBATTENDANCE " + 
        			"WHERE " + StartFilter + " AND " + EndFilter + " AND " + StaffIdFilter + " AND " + BranchIdFilter + " ORDER BY ATTENDANCEDATE, ATTENDANCETIME ";
        	
            PreparedStatement pSelectStmt = this.connection.prepareStatement(sqlStmt);
            ResultSet srs = pSelectStmt.executeQuery();
            
            FileOutputStream outfstreamData = null;
            
            outfstreamData = new FileOutputStream(sOutputFolder + sExportId + ".txt");
        	outputName = sExportId + ".txt";
      		outData = new DataOutputStream(outfstreamData);
      		
            while (srs.next()) {
            	 String StartChar = "00";
            	 String MiddleChar = "000";
            	 String StaffId = (srs.getString("STAFFID") == null)?"":srs.getString("STAFFID").trim(); 
            	 String Inout = (srs.getString("INOUT") == null)?"":srs.getString("INOUT").trim();
            	 String AttDate = (srs.getString("ATTENDANCEDATE") == null)?"":srs.getString("ATTENDANCEDATE").trim();
            	 String AttDateShow = AttDate.substring(6, 8) + AttDate.substring(4, 6) + AttDate.substring(0, 4);
                 String AttTime = (srs.getString("ATTENDANCETIME") == null)?"":srs.getString("ATTENDANCETIME").trim();
                 String AttTimeShow = AttTime.substring(0, 4);
                 outData.writeBytes("" + StartChar +  
                		 AttDateShow +  
                		 AttTimeShow +
                		 Inout + 
                		 MiddleChar + 
                		 StaffId + "\r\n");
            }
            
            if(outData != null ){
           	 	outData.flush();        
            	outData.close();
            }
        }catch (Exception e){//Catch exception if any       
       	 errorMessage = e.getMessage();
       	 outputName = "";
        }    	
        return outputName;
   }
	
	public String ExportMonthlyReport(String masterPath, String STAFFID, String BRANCHID, String genMonth, String ACCESSGROUPID) throws SQLException {
   	 	String outputName = "";
   	 	DataOutputStream outData = null;     
   	 	Calendar cal = Calendar.getInstance();
   	 	cal.add(Calendar.DATE, -1);
        Date BackupDate = cal.getTime();
        String sExportId = util.DateFormatConverter.format(BackupDate, "yyyyMMdd"); 
        //String sOutputFolder = masterPath+"\\";
        String sOutputFolder = masterPath;
        
        String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String MonthFilter = "1 = 1";
    	String AccessGroupIdFilter = "1 = 1";
        
        try {
            this.createFolder(new File(sOutputFolder));
            
            String sqlStmt = "";
            
            String STAFFNAME = "";
            String sqlStmt2 = "SELECT * FROM TBSTAFF WHERE UPPER(STAFFID) = '" + STAFFID.toUpperCase() + "' AND RECORDSTATUS = 'A'";	
            PreparedStatement pSelectStmt2 = this.connection.prepareStatement(sqlStmt2);
            ResultSet srs2 = pSelectStmt2.executeQuery();
            while (srs2.next()) {
            	STAFFNAME = (srs2.getString("STAFFNAME") == null)?"-":srs2.getString("STAFFNAME");
            }
            
        	if(STAFFID != "" && STAFFID.trim().length() > 0){
        		StaffIdFilter = "UPPER(STAFFID) = '" + STAFFID.toUpperCase() + "'";	
        	}
        	/*if(BRANCHID != "" && BRANCHID.trim().length() > 0){
        		BranchIdFilter = "UPPER(BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
        	} */
        	if(ACCESSGROUPID != "" && ACCESSGROUPID.trim().length() > 0){
        		AccessGroupIdFilter = "UPPER(ACCESSGROUPID) = '" + ACCESSGROUPID.toUpperCase() + "'";	
        	} 
        	if(genMonth != "" && genMonth.trim().length() > 0){
        		MonthFilter = "ATTENDANCEDATE LIKE '" + genMonth + "%'";
        	} 
        	
        	sqlStmt = "SELECT BRANCHID, " +  
					"[01], [02], [03], [04], [05], [06], [07], [08], [09], [10], [11], [12], [13], [14], [15], [16], [17], [18], [19], [20], [21], [22], [23], [24], [25], [26], [27], [28], [29], [30], [31] " + 
					"FROM " +  
					"( " +
						"SELECT BRANCHID, SUBSTRING(ATTENDANCEDATE, 7, 2) AS ATTENDANCEDAY, CLOCKINTIME+'-'+CLOCKOUTTIME AS WORKHOUR FROM " + 
						"v_attendance_inout_accessgroup " +
						"WHERE " + MonthFilter + " " +
						"AND " + StaffIdFilter + " " +
						"AND " + BranchIdFilter + " " +
						"AND " + AccessGroupIdFilter + " " +
					") AS SourceTable " +  
					"PIVOT " +  
					"( "  +
						"MIN(WORKHOUR) " + 
						"FOR ATTENDANCEDAY IN ([01], [02], [03], [04], [05], [06], [07], [08], [09], [10], [11], [12], [13], [14], [15], [16], [17], [18], [19], [20], [21], [22], [23], [24], [25], [26], [27], [28], [29], [30], [31]) " +  
					") AS PivotTable";
        			
        	System.out.println(sqlStmt);
            
            PreparedStatement pSelectStmt = this.connection.prepareStatement(sqlStmt);
            ResultSet srs = pSelectStmt.executeQuery();
            
            FileOutputStream outfstreamData = null;
            
        	outfstreamData = new FileOutputStream(sOutputFolder + "Monthly Report " + sExportId + ".csv");
        	outputName = "Monthly Report " + sExportId + ".csv";
      		outData = new DataOutputStream(outfstreamData);
      		
      		outData.writeBytes("" + "STAFF ID" + ","  + ":" + "," + STAFFID + "," + "\r\n");
      		outData.writeBytes("" + "STAFF NAME" + ","  + ":" + "," + STAFFNAME + "," + "\r\n");
      		
      		outData.writeBytes("\r\n");
      		
            outData.writeBytes("" + "BRANCH ID" + ","  + 
        			"01" + "," + "02" + "," +  "03" + "," +  "04" + "," +  "05" + "," +
        			"06" + "," + "07" + "," +  "08" + "," +  "09" + "," +  "10" + "," +
        			"11" + "," + "12" + "," +  "13" + "," +  "14" + "," +  "15" + "," +
        			"16" + "," + "17" + "," +  "18" + "," +  "19" + "," +  "20" + "," +
        			"21" + "," + "22" + "," +  "23" + "," +  "24" + "," +  "25" + "," +
        			"26" + "," + "27" + "," +  "28" + "," +  "29" + "," +  "30" + "," +
        			"31" + "," + "\r\n");
            
            while (srs.next()) {
	           	 String branch_id = (srs.getString("BRANCHID") == null)?"-":srs.getString("BRANCHID");
	           	 String day_1 = (srs.getString("01") == null)?"-":srs.getString("01");
	           	 String day_2 = (srs.getString("02") == null)?"-":srs.getString("02");
	           	 String day_3 = (srs.getString("03") == null)?"-":srs.getString("03");
	           	 String day_4 = (srs.getString("04") == null)?"-":srs.getString("04");
	           	 String day_5 = (srs.getString("05") == null)?"-":srs.getString("05");
	           	 String day_6 = (srs.getString("06") == null)?"-":srs.getString("06");
	           	 String day_7 = (srs.getString("07") == null)?"-":srs.getString("07");
	           	 String day_8 = (srs.getString("08") == null)?"-":srs.getString("08");
	           	 String day_9 = (srs.getString("09") == null)?"-":srs.getString("09");
	           	 String day_10 = (srs.getString("10") == null)?"-":srs.getString("10");
	           	 String day_11 = (srs.getString("11") == null)?"-":srs.getString("11");
	           	 String day_12 = (srs.getString("12") == null)?"-":srs.getString("12");
	           	 String day_13 = (srs.getString("13") == null)?"-":srs.getString("13");
	           	 String day_14 = (srs.getString("14") == null)?"-":srs.getString("14");
	           	 String day_15 = (srs.getString("15") == null)?"-":srs.getString("15");
	           	 String day_16 = (srs.getString("16") == null)?"-":srs.getString("16");
	           	 String day_17 = (srs.getString("17") == null)?"-":srs.getString("17");
	           	 String day_18 = (srs.getString("18") == null)?"-":srs.getString("18");
	           	 String day_19 = (srs.getString("19") == null)?"-":srs.getString("19");
	           	 String day_20 = (srs.getString("20") == null)?"-":srs.getString("20");
	           	 String day_21 = (srs.getString("21") == null)?"-":srs.getString("21");
	           	 String day_22 = (srs.getString("22") == null)?"-":srs.getString("22");
	           	 String day_23 = (srs.getString("23") == null)?"-":srs.getString("23");
	           	 String day_24 = (srs.getString("24") == null)?"-":srs.getString("24");
	           	 String day_25 = (srs.getString("25") == null)?"-":srs.getString("25");
	           	 String day_26 = (srs.getString("26") == null)?"-":srs.getString("26");
	           	 String day_27 = (srs.getString("27") == null)?"-":srs.getString("27");
	           	 String day_28 = (srs.getString("28") == null)?"-":srs.getString("28");
	           	 String day_29 = (srs.getString("29") == null)?"-":srs.getString("29");
	           	 String day_30 = (srs.getString("30") == null)?"-":srs.getString("30");
	           	 String day_31 = (srs.getString("31") == null)?"-":srs.getString("31");

	           	if(day_1 != "-"){
	           		String[] time = day_1.split("-"); 
	           		day_1 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_2 != "-"){
	           		String[] time = day_2.split("-"); 
	           		day_2 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_3 != "-"){
	           		String[] time = day_3.split("-"); 
	           		day_3 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_4 != "-"){
	           		String[] time = day_4.split("-"); 
	           		day_4 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_5 != "-"){
	           		String[] time = day_5.split("-"); 
	           		day_5 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_6 != "-"){
	           		String[] time = day_6.split("-"); 
	           		day_6 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_7 != "-"){
	           		String[] time = day_7.split("-"); 
	           		day_7 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_8 != "-"){
	           		String[] time = day_8.split("-"); 
	           		day_8 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_9 != "-"){
	           		String[] time = day_9.split("-"); 
	           		day_9 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_10 != "-"){
	           		String[] time = day_10.split("-"); 
	           		day_10 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_11 != "-"){
	           		String[] time = day_11.split("-"); 
	           		day_11 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_12 != "-"){
	           		String[] time = day_12.split("-"); 
	           		day_12 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_13 != "-"){
	           		String[] time = day_13.split("-"); 
	           		day_13 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_14 != "-"){
	           		String[] time = day_14.split("-"); 
	           		day_14 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_15 != "-"){
	           		String[] time = day_15.split("-"); 
	           		day_15 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_16 != "-"){
	           		String[] time = day_16.split("-"); 
	           		day_16 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_17 != "-"){
	           		String[] time = day_17.split("-"); 
	           		day_17 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_18 != "-"){
	           		String[] time = day_18.split("-"); 
	           		day_18 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_19 != "-"){
	           		String[] time = day_19.split("-"); 
	           		day_19 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_20 != "-"){
	           		String[] time = day_20.split("-"); 
	           		day_20 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_21 != "-"){
	           		String[] time = day_21.split("-"); 
	           		day_21 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_22 != "-"){
	           		String[] time = day_22.split("-"); 
	           		day_22 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_23 != "-"){
	           		String[] time = day_23.split("-"); 
	           		day_23 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_24 != "-"){
	           		String[] time = day_24.split("-"); 
	           		day_24 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_25 != "-"){
	           		String[] time = day_25.split("-"); 
	           		day_25 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_26 != "-"){
	           		String[] time = day_26.split("-"); 
	           		day_26 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_27 != "-"){
	           		String[] time = day_27.split("-"); 
	           		day_27 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_28 != "-"){
	           		String[] time = day_28.split("-"); 
	           		day_28 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_29 != "-"){
	           		String[] time = day_29.split("-"); 
	           		day_29 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_30 != "-"){
	           		String[] time = day_30.split("-"); 
	           		day_30 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_31 != "-"){
	           		String[] time = day_31.split("-"); 
	           		day_31 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	
	       		 outData.writeBytes("" + branch_id + "," + 
	       				day_1 + "," + day_2 + "," + day_3 + "," + day_4 + "," + day_5 + "," +
	       				day_6 + "," + day_7 + "," + day_8 + "," + day_9 + "," + day_10 + "," +
	       				day_11 + "," + day_12 + "," + day_13 + "," + day_14 + "," + day_15 + "," +
	       				day_16 + "," + day_17 + "," + day_18 + "," + day_19 + "," + day_20 + "," +
	       				day_21 + "," + day_22 + "," + day_23 + "," + day_24 + "," + day_25 + "," +
	       				day_26 + "," + day_27 + "," + day_28 + "," + day_29 + "," + day_30 + "," +
	       				day_31 + "\r\n");
            }
        
            
            if(outData != null ){
           	 	outData.flush();        
            	outData.close();
            }
        }catch (Exception e){//Catch exception if any       
       	 errorMessage = e.getMessage();
       	 outputName = "";
        }    	
        return outputName;
   }
	
	public String ExportMonthlyReportWithStaff(String masterPath, String STAFFID, String BRANCHID, String genMonth, String ACCESSGROUPID) throws SQLException {
   	 	String outputName = "";
   	 	DataOutputStream outData = null;     
   	 	Calendar cal = Calendar.getInstance();
   	 	cal.add(Calendar.DATE, -1);
        Date BackupDate = cal.getTime();
        String sExportId = util.DateFormatConverter.format(BackupDate, "yyyyMMdd"); 
        //String sOutputFolder = masterPath+"\\";
        String sOutputFolder = masterPath;
        
        String StaffIdFilter = "1 = 1";
    	String BranchIdFilter = "1 = 1";
    	String MonthFilter = "1 = 1";
    	String AccessGroupIdFilter = "1 = 1";
        
        try {
            this.createFolder(new File(sOutputFolder));
            
            String sqlStmt = "";
            
            String STAFFNAME = "";
            String sqlStmt2 = "SELECT * FROM TBSTAFF WHERE UPPER(STAFFID) = '" + STAFFID.toUpperCase() + "' AND RECORDSTATUS = 'A'";	
            PreparedStatement pSelectStmt2 = this.connection.prepareStatement(sqlStmt2);
            ResultSet srs2 = pSelectStmt2.executeQuery();
            while (srs2.next()) {
            	STAFFNAME = (srs2.getString("STAFFNAME") == null)?"-":srs2.getString("STAFFNAME");
            }
            
        	if(STAFFID != "" && STAFFID.trim().length() > 0){
        		StaffIdFilter = "UPPER(STAFFID) = '" + STAFFID.toUpperCase() + "'";	
        	}
        	/*if(BRANCHID != "" && BRANCHID.trim().length() > 0){
        		BranchIdFilter = "UPPER(BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
        	} */
        	if(ACCESSGROUPID != "" && ACCESSGROUPID.trim().length() > 0){
        		AccessGroupIdFilter = "UPPER(ACCESSGROUPID) = '" + ACCESSGROUPID.toUpperCase() + "'";	
        	} 
        	if(genMonth != "" && genMonth.trim().length() > 0){
        		MonthFilter = "ATTENDANCEDATE LIKE '" + genMonth + "%'";
        	} 
        	
        	sqlStmt = "SELECT STAFFID, BRANCHID, " +  
					"[01], [02], [03], [04], [05], [06], [07], [08], [09], [10], [11], [12], [13], [14], [15], [16], [17], [18], [19], [20], [21], [22], [23], [24], [25], [26], [27], [28], [29], [30], [31] " + 
					"FROM " +  
					"( " +
						"SELECT STAFFID, BRANCHID, SUBSTRING(ATTENDANCEDATE, 7, 2) AS ATTENDANCEDAY, CLOCKINTIME+'-'+CLOCKOUTTIME AS WORKHOUR FROM " + 
						"v_attendance_inout_accessgroup " +
						"WHERE " + MonthFilter + " " +
						"AND " + StaffIdFilter + " " +
						"AND " + BranchIdFilter + " " +
						"AND " + AccessGroupIdFilter + " " +
					") AS SourceTable " +  
					"PIVOT " +  
					"( "  +
						"MIN(WORKHOUR) " + 
						"FOR ATTENDANCEDAY IN ([01], [02], [03], [04], [05], [06], [07], [08], [09], [10], [11], [12], [13], [14], [15], [16], [17], [18], [19], [20], [21], [22], [23], [24], [25], [26], [27], [28], [29], [30], [31]) " +  
					") AS PivotTable " + 
					"ORDER BY STAFFID ";
        			
        	//System.out.println(sqlStmt);
            
            PreparedStatement pSelectStmt = this.connection.prepareStatement(sqlStmt);
            ResultSet srs = pSelectStmt.executeQuery();
            
            FileOutputStream outfstreamData = null;
            
        	outfstreamData = new FileOutputStream(sOutputFolder + "Monthly Report " + sExportId + ".csv");
        	outputName = "Monthly Report " + sExportId + ".csv";
      		outData = new DataOutputStream(outfstreamData);
      		
      		outData.writeBytes("" + "STAFF ID" + ","  + ":" + "," + STAFFID + "," + "\r\n");
      		outData.writeBytes("" + "ACCESS GROUP ID" + ","  + ":" + "," + ACCESSGROUPID + "," + "\r\n");
      		
      		outData.writeBytes("\r\n");
      		
            outData.writeBytes("" + "STAFF ID" + "," + "BRANCH ID" + ","  + 
        			"01" + "," + "02" + "," +  "03" + "," +  "04" + "," +  "05" + "," +
        			"06" + "," + "07" + "," +  "08" + "," +  "09" + "," +  "10" + "," +
        			"11" + "," + "12" + "," +  "13" + "," +  "14" + "," +  "15" + "," +
        			"16" + "," + "17" + "," +  "18" + "," +  "19" + "," +  "20" + "," +
        			"21" + "," + "22" + "," +  "23" + "," +  "24" + "," +  "25" + "," +
        			"26" + "," + "27" + "," +  "28" + "," +  "29" + "," +  "30" + "," +
        			"31" + "," + "\r\n");
            
            while (srs.next()) {
	           	 String staff_id = (srs.getString("STAFFID") == null)?"-":srs.getString("STAFFID");
	           	 String branch_id = (srs.getString("BRANCHID") == null)?"-":srs.getString("BRANCHID");
	           	 String day_1 = (srs.getString("01") == null)?"-":srs.getString("01");
	           	 String day_2 = (srs.getString("02") == null)?"-":srs.getString("02");
	           	 String day_3 = (srs.getString("03") == null)?"-":srs.getString("03");
	           	 String day_4 = (srs.getString("04") == null)?"-":srs.getString("04");
	           	 String day_5 = (srs.getString("05") == null)?"-":srs.getString("05");
	           	 String day_6 = (srs.getString("06") == null)?"-":srs.getString("06");
	           	 String day_7 = (srs.getString("07") == null)?"-":srs.getString("07");
	           	 String day_8 = (srs.getString("08") == null)?"-":srs.getString("08");
	           	 String day_9 = (srs.getString("09") == null)?"-":srs.getString("09");
	           	 String day_10 = (srs.getString("10") == null)?"-":srs.getString("10");
	           	 String day_11 = (srs.getString("11") == null)?"-":srs.getString("11");
	           	 String day_12 = (srs.getString("12") == null)?"-":srs.getString("12");
	           	 String day_13 = (srs.getString("13") == null)?"-":srs.getString("13");
	           	 String day_14 = (srs.getString("14") == null)?"-":srs.getString("14");
	           	 String day_15 = (srs.getString("15") == null)?"-":srs.getString("15");
	           	 String day_16 = (srs.getString("16") == null)?"-":srs.getString("16");
	           	 String day_17 = (srs.getString("17") == null)?"-":srs.getString("17");
	           	 String day_18 = (srs.getString("18") == null)?"-":srs.getString("18");
	           	 String day_19 = (srs.getString("19") == null)?"-":srs.getString("19");
	           	 String day_20 = (srs.getString("20") == null)?"-":srs.getString("20");
	           	 String day_21 = (srs.getString("21") == null)?"-":srs.getString("21");
	           	 String day_22 = (srs.getString("22") == null)?"-":srs.getString("22");
	           	 String day_23 = (srs.getString("23") == null)?"-":srs.getString("23");
	           	 String day_24 = (srs.getString("24") == null)?"-":srs.getString("24");
	           	 String day_25 = (srs.getString("25") == null)?"-":srs.getString("25");
	           	 String day_26 = (srs.getString("26") == null)?"-":srs.getString("26");
	           	 String day_27 = (srs.getString("27") == null)?"-":srs.getString("27");
	           	 String day_28 = (srs.getString("28") == null)?"-":srs.getString("28");
	           	 String day_29 = (srs.getString("29") == null)?"-":srs.getString("29");
	           	 String day_30 = (srs.getString("30") == null)?"-":srs.getString("30");
	           	 String day_31 = (srs.getString("31") == null)?"-":srs.getString("31");

	           	if(day_1 != "-"){
	           		String[] time = day_1.split("-"); 
	           		day_1 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_2 != "-"){
	           		String[] time = day_2.split("-"); 
	           		day_2 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_3 != "-"){
	           		String[] time = day_3.split("-"); 
	           		day_3 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_4 != "-"){
	           		String[] time = day_4.split("-"); 
	           		day_4 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_5 != "-"){
	           		String[] time = day_5.split("-"); 
	           		day_5 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_6 != "-"){
	           		String[] time = day_6.split("-"); 
	           		day_6 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_7 != "-"){
	           		String[] time = day_7.split("-"); 
	           		day_7 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_8 != "-"){
	           		String[] time = day_8.split("-"); 
	           		day_8 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_9 != "-"){
	           		String[] time = day_9.split("-"); 
	           		day_9 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_10 != "-"){
	           		String[] time = day_10.split("-"); 
	           		day_10 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_11 != "-"){
	           		String[] time = day_11.split("-"); 
	           		day_11 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_12 != "-"){
	           		String[] time = day_12.split("-"); 
	           		day_12 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_13 != "-"){
	           		String[] time = day_13.split("-"); 
	           		day_13 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_14 != "-"){
	           		String[] time = day_14.split("-"); 
	           		day_14 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_15 != "-"){
	           		String[] time = day_15.split("-"); 
	           		day_15 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_16 != "-"){
	           		String[] time = day_16.split("-"); 
	           		day_16 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_17 != "-"){
	           		String[] time = day_17.split("-"); 
	           		day_17 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_18 != "-"){
	           		String[] time = day_18.split("-"); 
	           		day_18 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_19 != "-"){
	           		String[] time = day_19.split("-"); 
	           		day_19 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_20 != "-"){
	           		String[] time = day_20.split("-"); 
	           		day_20 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_21 != "-"){
	           		String[] time = day_21.split("-"); 
	           		day_21 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_22 != "-"){
	           		String[] time = day_22.split("-"); 
	           		day_22 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_23 != "-"){
	           		String[] time = day_23.split("-"); 
	           		day_23 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	if(day_24 != "-"){
	           		String[] time = day_24.split("-"); 
	           		day_24 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_25 != "-"){
	           		String[] time = day_25.split("-"); 
	           		day_25 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_26 != "-"){
	           		String[] time = day_26.split("-"); 
	           		day_26 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_27 != "-"){
	           		String[] time = day_27.split("-"); 
	           		day_27 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_28 != "-"){
	           		String[] time = day_28.split("-"); 
	           		day_28 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_29 != "-"){
	           		String[] time = day_29.split("-"); 
	           		day_29 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_30 != "-"){
	           		String[] time = day_30.split("-"); 
	           		day_30 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	 if(day_31 != "-"){
	           		String[] time = day_31.split("-"); 
	           		day_31 = time[0].substring(0,4) + '-' + time[1].substring(0,4); 
	           	 }
	           	
	       		 outData.writeBytes("" + branch_id + "," + staff_id + "," +
	       				day_1 + "," + day_2 + "," + day_3 + "," + day_4 + "," + day_5 + "," +
	       				day_6 + "," + day_7 + "," + day_8 + "," + day_9 + "," + day_10 + "," +
	       				day_11 + "," + day_12 + "," + day_13 + "," + day_14 + "," + day_15 + "," +
	       				day_16 + "," + day_17 + "," + day_18 + "," + day_19 + "," + day_20 + "," +
	       				day_21 + "," + day_22 + "," + day_23 + "," + day_24 + "," + day_25 + "," +
	       				day_26 + "," + day_27 + "," + day_28 + "," + day_29 + "," + day_30 + "," +
	       				day_31 + "\r\n");
            }
        
            
            if(outData != null ){
           	 	outData.flush();        
            	outData.close();
            }
        }catch (Exception e){//Catch exception if any       
       	 errorMessage = e.getMessage();
       	 outputName = "";
        }    	
        return outputName;
   }

	private void createFolder(File file) throws IOException {
        boolean exists = file.exists();
        if (!exists) {
            file.mkdirs();
        }
    }
    
    public String zipping(String masterPath,String filename1,String filename2){
    	String zipFile = masterPath + "ReportArchive.zip";
    	String[] srcFiles = { masterPath + filename1, masterPath + filename2};
    	try {
    		byte[] buffer = new byte[1024];
    		FileOutputStream fos = new FileOutputStream(zipFile);
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		for (int i=0; i < srcFiles.length; i++) {
    			File srcFile = new File(srcFiles[i]);
    			FileInputStream fis = new FileInputStream(srcFile);
    			zos.putNextEntry(new ZipEntry(srcFile.getName()));
    			int length;
    			while ((length = fis.read(buffer)) > 0) {
    				zos.write(buffer, 0, length);
    			}
    			zos.closeEntry();
    			fis.close();
    		}
    		zos.close();
    		return "ReportArchive.zip";
    	} catch (IOException ioe) {
    		System.out.println("Error creating zip file: " + ioe);
    		return "";
    	}
    }
}
