package Biometrics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DeviceManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addDevice(TBDEVICE oDevice) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBDEVICE WHERE DEVICEID = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oDevice.getDEVICEID());
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "Device [" + oDevice.getDEVICEID()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBDEVICE " + 
			"( DEVICEID , DEVICENAME, IPADDRESS, PORTNO, BRANCHID, ALLOWBIOMETRIC, ALLOWPIN, ALLOWACCESSCARD, RECORDSTATUS, CREATEDBY, CREATEDSTAMP, SvcInstance, Enabled, Activated) " + 
			"VALUES (? , ?, ?, ?, ?, ?, ?, ?, ?, ?, '" + currentDateTime + "', ?, '1', '1') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oDevice.getDEVICEID());
			pStmt.setString(2, oDevice.getDEVICENAME());
			pStmt.setString(3, oDevice.getIPADDRESS());
			pStmt.setString(4, oDevice.getPORTNO());
			pStmt.setString(5, oDevice.getBRANCHID());
			pStmt.setString(6, oDevice.getALLOWBIOMETRIC());
			pStmt.setString(7, oDevice.getALLOWPIN());
			pStmt.setString(8, oDevice.getALLOWACCESSCARD());
			pStmt.setString(9, "A");
			pStmt.setString(10, oDevice.getCREATEDBY());
			pStmt.setString(11, oDevice.getSvcInstance());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateDevice(TBDEVICE oDevice) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBDEVICE WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oDevice.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Device [" + oDevice.getDEVICEID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBDEVICE SET "
								+ "  DEVICEID = ? " 
								+ ", DEVICENAME = ? "
								+ ", IPADDRESS = ? "
								+ ", PORTNO = ? "
								+ ", BRANCHID = ? "
								+ ", ALLOWBIOMETRIC = ? "
								+ ", ALLOWPIN = ? "
								+ ", ALLOWACCESSCARD = ? "
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
								+ ", SvcInstance = ? "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oDevice.getDEVICEID());
			pStmt.setString(2, oDevice.getDEVICENAME());
			pStmt.setString(3, oDevice.getIPADDRESS());
			pStmt.setString(4, oDevice.getPORTNO());
			pStmt.setString(5, oDevice.getBRANCHID());
			pStmt.setString(6, oDevice.getALLOWBIOMETRIC());
			pStmt.setString(7, oDevice.getALLOWPIN());
			pStmt.setString(8, oDevice.getALLOWACCESSCARD());
			pStmt.setString(9, oDevice.getLASTUPDATETIMEDBY());
			pStmt.setString(10, oDevice.getSvcInstance());
			pStmt.setInt(11, oDevice.getID());
			
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public String getSvcInstance() {
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT SvcInstance, COUNT(*) AS COUNTED FROM TBDEVICE WHERE RECORDSTATUS = ? GROUP BY SvcInstance HAVING COUNT(*) < 150";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			String SvcInstance = "0";
			while (rsSelect.next()) {
				SvcInstance = rsSelect.getString("SvcInstance");
				break;
			}
			
			return SvcInstance;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return "0";
		}
	}
	
	public boolean checkSvcInstance(String sSvcInstance) {
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT COUNT(*) AS COUNTED FROM TBDEVICE WHERE SvcInstance = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, sSvcInstance);
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			int counted = 0;
			while (rsSelect.next()) {
				counted = rsSelect.getInt("COUNTED");
			}
			
			if (counted >= 150) {
				return false;
			}

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public TBDEVICE getDeviceById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBDEVICE WHERE ID = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "A");
		
		ArrayList<TBDEVICE> alDevice;

		alDevice = this.getDeviceData(pStmt);

		if (alDevice.size() == 0)
			return null;

		return alDevice.get(0);		
	}
	
	public TBDEVICE getDeviceByDeviceId(String DEVICEID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBDEVICE WHERE UPPER(DEVICEID) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, DEVICEID.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBDEVICE> alDevice;

		alDevice = this.getDeviceData(pStmt);

		if (alDevice.size() == 0)
			return null;

		return alDevice.get(0);		
	}
	
	public TBDEVICE getDeviceByDeviceName(String DEVICENAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBDEVICE WHERE UPPER(DEVICENAME) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, DEVICENAME.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBDEVICE> alDevice;

		alDevice = this.getDeviceData(pStmt);

		if (alDevice.size() == 0)
			return null;

		return alDevice.get(0);		
	}
	
	public TBDEVICE[] getDeviceAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	

    	String sqlStmt = "SELECT * FROM (" + 
    	"Select TBDEVICE.*,TBBRANCH.BRANCHNAME, ROW_NUMBER() over (order by TBDEVICE.ID) as RowNum " +
    	"FROM TBDEVICE " +
    	"LEFT JOIN TBBRANCH ON (TBBRANCH.BRANCHID = TBDEVICE.BRANCHID AND TBBRANCH.RECORDSTATUS = ?) " + 
    	"WHERE TBDEVICE.RECORDSTATUS = ? ) TableName " +  
    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	pStmt.setString(2, "A");
    	
		ArrayList<TBDEVICE> alDevice;

		alDevice = this.getDeviceDataComplex(pStmt);

		if (alDevice.size() == 0)
			return null;

		TBDEVICE[] users = new TBDEVICE[alDevice.size()];

		alDevice.toArray(users);

		return users;
	}
	
	public int countDeviceAll() throws SQLException {
		String sqlStmt = "Select TBDEVICE.*,TBBRANCH.BRANCHNAME " +
		"FROM TBDEVICE " +
		"LEFT JOIN TBBRANCH ON (TBBRANCH.BRANCHID = TBDEVICE.BRANCHID AND TBBRANCH.RECORDSTATUS = ?) " + 
		"WHERE TBDEVICE.RECORDSTATUS = ? ";
		
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        pSelectStmt.setString(2, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBDEVICE[] getDeviceByFilter(String DEVICEID, String DEVICENAME, String BRANCHID, String offset) throws SQLException {
    	String DeviceIdFilter = "1=1";
    	String DeviceNameFilter = "1=1";
    	String BranchIdFilter = "1=1";
    	String itemPerPage = "30";
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBDEVICE.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";	
    	}
    	if(DEVICENAME != "" && DEVICENAME.trim().length() > 0){
    		DeviceNameFilter = "UPPER(TBDEVICE.DEVICENAME) LIKE ('%" + DEVICENAME.toUpperCase() + "%')";	
    	} 
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	}

    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM (" + 
    	"Select TBDEVICE.*,TBBRANCH.BRANCHNAME, ROW_NUMBER() over (order by TBDEVICE.ID) as RowNum " +
    	"FROM TBDEVICE " +
    	"LEFT JOIN TBBRANCH ON (TBBRANCH.BRANCHID = TBDEVICE.BRANCHID AND TBBRANCH.RECORDSTATUS = ?) " + 
    	"WHERE " + DeviceIdFilter + " AND " + DeviceNameFilter + " AND " + BranchIdFilter + " AND TBDEVICE.RECORDSTATUS = ? ) TableName " +  
    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        pStmt.setString(2, "A");
        
        ArrayList<TBDEVICE> alDevice = new ArrayList<TBDEVICE>();
        alDevice = this.getDeviceDataComplex(pStmt);
        TBDEVICE[] users = new TBDEVICE[alDevice.size()];
		alDevice.toArray(users);
		return users;
    }
	
	public int countDeviceByFilter(String DEVICEID, String DEVICENAME, String BRANCHID) throws SQLException {
		String DeviceIdFilter = "1=1";
    	String DeviceNameFilter = "1=1";
    	String BranchIdFilter = "1=1";

    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBDEVICE.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";	
    	}
    	if(DEVICENAME != "" && DEVICENAME.trim().length() > 0){
    		DeviceNameFilter = "UPPER(TBDEVICE.DEVICENAME) LIKE ('%" + DEVICENAME.toUpperCase() + "%')";	
    	} 
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	}
    	
    	String sqlStmt = "Select TBDEVICE.*,TBBRANCH.BRANCHNAME " +
		"FROM TBDEVICE " +
		"LEFT JOIN TBBRANCH ON (TBBRANCH.BRANCHID = TBDEVICE.BRANCHID AND TBBRANCH.RECORDSTATUS = ?) " + 
		"WHERE " + DeviceIdFilter + " AND " + DeviceNameFilter + " AND " + BranchIdFilter + " AND TBDEVICE.RECORDSTATUS = ? ";

    	String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        pSelectStmt.setString(2, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public TBDEVICE[] getDeviceMonitorByFilter(String DEVICEID, String DEVICENAME, String BRANCHID, String STATUS, String offset) throws SQLException {
    	String DeviceIdFilter = "1=1";
    	String DeviceNameFilter = "1=1";
    	String BranchIdFilter = "1=1";
    	String StatusFilter = "1=1";
    	String itemPerPage = "30";
    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBDEVICE.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";	
    	}
    	if(DEVICENAME != "" && DEVICENAME.trim().length() > 0){
    		DeviceNameFilter = "UPPER(TBDEVICE.DEVICENAME) LIKE ('%" + DEVICENAME.toUpperCase() + "%')";	
    	} 
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	}
    	if(STATUS != "" && STATUS.trim().length() > 0){
    		StatusFilter = "TBDEVICE.STATUS = " + STATUS + "";	
    	}
    	
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM (" + 
    	"Select TBDEVICE.*,TBBRANCH.BRANCHNAME, ROW_NUMBER() over (order by TBDEVICE.ID) as RowNum " +
    	"FROM TBDEVICE " +
    	"LEFT JOIN TBBRANCH ON (TBBRANCH.BRANCHID = TBDEVICE.BRANCHID AND TBBRANCH.RECORDSTATUS = ?) " + 
    	"WHERE " + DeviceIdFilter + " AND " + DeviceNameFilter + " AND " + BranchIdFilter + " AND " + StatusFilter + " AND TBDEVICE.RECORDSTATUS = ? ) TableName " +  
    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        pStmt.setString(2, "A");
        
        ArrayList<TBDEVICE> alDevice = new ArrayList<TBDEVICE>();
        alDevice = this.getDeviceDataComplex(pStmt);
        TBDEVICE[] users = new TBDEVICE[alDevice.size()];
		alDevice.toArray(users);
		return users;
    }
	
	public int countDeviceMonitorByFilter(String DEVICEID, String DEVICENAME, String BRANCHID, String STATUS) throws SQLException {
		String DeviceIdFilter = "1=1";
    	String DeviceNameFilter = "1=1";
    	String BranchIdFilter = "1=1";
    	String StatusFilter = "1=1";

    	if(DEVICEID != "" && DEVICEID.trim().length() > 0){
    		DeviceIdFilter = "UPPER(TBDEVICE.DEVICEID) = '" + DEVICEID.toUpperCase() + "'";	
    	}
    	if(DEVICENAME != "" && DEVICENAME.trim().length() > 0){
    		DeviceNameFilter = "UPPER(TBDEVICE.DEVICENAME) LIKE ('%" + DEVICENAME.toUpperCase() + "%')";	
    	} 
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBDEVICE.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	}
    	if(STATUS != "" && STATUS.trim().length() > 0){
    		StatusFilter = "TBDEVICE.STATUS = " + STATUS + "";	
    	}
    	
    	String sqlStmt = "Select TBDEVICE.*,TBBRANCH.BRANCHNAME " +
		"FROM TBDEVICE " +
		"LEFT JOIN TBBRANCH ON (TBBRANCH.BRANCHID = TBDEVICE.BRANCHID AND TBBRANCH.RECORDSTATUS = ?) " + 
		"WHERE " + DeviceIdFilter + " AND " + DeviceNameFilter + " AND " + BranchIdFilter + " AND " + StatusFilter + " AND TBDEVICE.RECORDSTATUS = ? ";

    	String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        pSelectStmt.setString(2, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteDevice(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBDEVICE WHERE ID = ? ";
			//String sqlStmt = "UPDATE TBDEVICE SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);

			pStmt.setInt(1, ID);
			
			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}

	public boolean deleteDevice(String DEVICEID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBDEVICE WHERE DEVICEID = ? AND RECORDSTATUS = ?";
			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, DEVICEID);
			pStmt.setString(2, "A");

			
			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public ArrayList<TBDEVICE> getDeviceData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBDEVICE> alDevice = new ArrayList<TBDEVICE>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBDEVICE oDevice = new TBDEVICE();
			oDevice.setID(srs.getInt("ID"));
			oDevice.setDEVICEID(srs.getString("DEVICEID"));
			oDevice.setDEVICENAME(srs.getString("DEVICENAME"));
			oDevice.setIPADDRESS(srs.getString("IPADDRESS"));
			oDevice.setPORTNO(srs.getString("PORTNO"));
			oDevice.setBRANCHID(srs.getString("BRANCHID"));
			oDevice.setALLOWBIOMETRIC(srs.getString("ALLOWBIOMETRIC"));
			oDevice.setALLOWPIN(srs.getString("ALLOWPIN"));
			oDevice.setALLOWACCESSCARD(srs.getString("ALLOWACCESSCARD"));
			oDevice.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oDevice.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oDevice.setCREATEDBY(srs.getString("CREATEDBY"));
			oDevice.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oDevice.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			oDevice.setStatus(srs.getString("Status"));

			oDevice.setSvcInstance(srs.getString("SvcInstance"));
			alDevice.add(oDevice);
		}

		return alDevice;
	}
	
	public ArrayList<TBDEVICE> getDeviceDataComplex(PreparedStatement pStmt)
			throws SQLException {
		
		ArrayList<TBDEVICE> alDevice = new ArrayList<TBDEVICE>();
		
		ResultSet srs = pStmt.executeQuery();
		
		while (srs.next()) {
			TBDEVICE oDevice = new TBDEVICE();
			oDevice.setID(srs.getInt("ID"));
			oDevice.setDEVICEID(srs.getString("DEVICEID"));
			oDevice.setDEVICENAME(srs.getString("DEVICENAME"));
			oDevice.setIPADDRESS(srs.getString("IPADDRESS"));
			oDevice.setPORTNO(srs.getString("PORTNO"));
			oDevice.setBRANCHID(srs.getString("BRANCHID"));
			oDevice.setBRANCHNAME(srs.getString("BRANCHNAME"));
			oDevice.setALLOWBIOMETRIC(srs.getString("ALLOWBIOMETRIC"));
			oDevice.setALLOWPIN(srs.getString("ALLOWPIN"));
			oDevice.setALLOWACCESSCARD(srs.getString("ALLOWACCESSCARD"));
			oDevice.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oDevice.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oDevice.setCREATEDBY(srs.getString("CREATEDBY"));
			oDevice.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oDevice.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			oDevice.setStatus(srs.getString("Status"));
			
			oDevice.setSvcInstance(srs.getString("SvcInstance"));
			alDevice.add(oDevice);
		}
		
		return alDevice;
	}
}
