package Biometrics;

/*
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.Platform;
import com.sun.jna.*;
*/
public interface RGDeviceServiceAPI /*extends Library*/ {
	/*
	public static final String JNA_LIBRARY_NAME = "RGDeviceSvcLib";
	
	public static final NativeLibrary JNA_NATIVE_LIB = NativeLibrary.getInstance(RGDeviceServiceAPI.JNA_LIBRARY_NAME);
	
	public static final RGDeviceServiceAPI INSTANCE = (RGDeviceServiceAPI) Native.loadLibrary(
			RGDeviceServiceAPI.JNA_LIBRARY_NAME, RGDeviceServiceAPI.class);
	
	public interface DeviceActionInfo extends Library {
		public static final String JNA_LIBRARY_ACTION_INFO = "RGDeviceSvcLib.DeviceActionInfo";
		
		public static final NativeLibrary JNA_NATIVE_LIB = NativeLibrary.getInstance(RGDeviceServiceAPI.JNA_LIBRARY_NAME);
		
		public static final RGDeviceServiceAPI ACTION_INFO_INSTANCE = (RGDeviceServiceAPI) Native.loadLibrary(
				RGDeviceServiceAPI.DeviceActionInfo.JNA_LIBRARY_ACTION_INFO, RGDeviceServiceAPI.DeviceActionInfo.class);
		
		public enum ActionCode
	    {
			UnknownAction ,		//0
			AddDevice , 		//1
			Disabled ,			//2
			Enabled,			//3
			Restart ,			//4
			RestoreMast ,		//5
			EnrollStaff ,		//6
			DeleteStaff ,		//7
			DeleteFingerPrint ,	//8
			DeleteDevice , 		//9
			TransferStaff ,		//10
			EnrollAccessgrp ,	//11
			DeleteAccessgrp    	//12
	    }
		//public ActionCode actcode;
		//public String targetId;
		//public String param;
		
		
		//public DeviceActionInfo() {
			//public ActionCode actcode;
			//public String targetId;
			//public String param;
		//}
	}
	
	public interface DeviceActor extends Library {
		public static final String JNA_LIBRARY_ACTOR = "RGDeviceSvcLib.DeviceActor";
		
		public static final RGDeviceServiceAPI ACTOR_INSTANCE = (RGDeviceServiceAPI) Native.loadLibrary(
				RGDeviceServiceAPI.DeviceActor.JNA_LIBRARY_ACTOR, RGDeviceServiceAPI.DeviceActor.class);
		
		String SubmitAction(DeviceActionInfo act);
		
		
	}	
	*/
}