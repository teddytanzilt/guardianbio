package Biometrics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ClientManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addClient(TBCLIENT oClient) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBCLIENT WHERE CLIENTIP = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oClient.getCLIENTIP());
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "Client [" + oClient.getCLIENTIP()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBCLIENT " + 
			"( CLIENTIP , CLIENTNAME, AREAID, RECORDSTATUS, CREATEDBY, CREATEDSTAMP) " + 
			"VALUES (?, ?, ?, ?, ?, '" + currentDateTime + "') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oClient.getCLIENTIP());
			pStmt.setString(2, oClient.getCLIENTNAME());
			pStmt.setString(3, oClient.getAREAID());
			pStmt.setString(4, "A");
			pStmt.setString(5, oClient.getCREATEDBY());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateClient(TBCLIENT oClient) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBCLIENT WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oClient.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Client [" + oClient.getCLIENTIP()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBCLIENT SET "
								+ "  CLIENTIP = ? " 
								+ ", CLIENTNAME = ? "
								+ ", AREAID = ? "
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oClient.getCLIENTIP());
			pStmt.setString(2, oClient.getCLIENTNAME());
			pStmt.setString(3, oClient.getAREAID());
			pStmt.setString(4, oClient.getLASTUPDATETIMEDBY());
			pStmt.setInt(5, oClient.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public TBCLIENT getClientById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBCLIENT WHERE ID = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "A");
		
		ArrayList<TBCLIENT> alClient;

		alClient = this.getClientData(pStmt);

		if (alClient.size() == 0)
			return null;

		return alClient.get(0);		
	}
	
	public TBCLIENT getClientByClientId(String CLIENTIP) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBCLIENT WHERE UPPER(CLIENTIP) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, CLIENTIP.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBCLIENT> alClient;

		alClient = this.getClientData(pStmt);

		if (alClient.size() == 0)
			return null;

		return alClient.get(0);		
	}
	
	public TBCLIENT getClientByClientName(String CLIENTNAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBCLIENT WHERE UPPER(CLIENTNAME) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, CLIENTNAME.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBCLIENT> alClient;

		alClient = this.getClientData(pStmt);

		if (alClient.size() == 0)
			return null;

		return alClient.get(0);		
	}
	
	public TBCLIENT[] getClientAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBCLIENT";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum " +
    					"FROM "+tableName+" WHERE RECORDSTATUS = ? ) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBCLIENT> alClient;

		alClient = this.getClientData(pStmt);

		if (alClient.size() == 0)
			return null;

		TBCLIENT[] users = new TBCLIENT[alClient.size()];

		alClient.toArray(users);

		return users;
	}
	
	public int countClientAll() throws SQLException {
    	String sqlStmt = "SELECT * FROM TBCLIENT WHERE RECORDSTATUS = ? ";        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBCLIENT[] getClientByFilter(String CLIENTIP, String CLIENTNAME, String AREAID, String offset) throws SQLException {
    	String ClientIpFilter = "1=1";
    	String ClientNameFilter = "1=1";
    	String AreaIdFilter = "1=1";
    	String itemPerPage = "30";
    	if(CLIENTIP != "" && CLIENTIP.trim().length() > 0){
    		ClientIpFilter = "UPPER(CLIENTIP) = '" + CLIENTIP.toUpperCase() + "'";	
    	}
    	if(CLIENTNAME != "" && CLIENTNAME.trim().length() > 0){
    		ClientNameFilter = "UPPER(CLIENTNAME) LIKE ('%" + CLIENTNAME.toUpperCase() + "%')";	
    	} 
    	if(AREAID != "" && AREAID.trim().length() > 0){
    		AreaIdFilter = "UPPER(AREAID) = '" + AREAID.toUpperCase() + "'";	
    	}
    	
    	String tableName = "TBCLIENT";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum FROM " + tableName + " " +
    					"WHERE " + ClientIpFilter + " AND " + ClientNameFilter + " " +
    					"AND " + AreaIdFilter + " AND RECORDSTATUS = ? ) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        
        ArrayList<TBCLIENT> alClient = new ArrayList<TBCLIENT>();
        alClient = this.getClientData(pStmt);
        TBCLIENT[] users = new TBCLIENT[alClient.size()];
		alClient.toArray(users);
		return users;
    }
	
	public int countClientByFilter(String CLIENTIP, String CLIENTNAME, String AREAID) throws SQLException {
		String ClientIpFilter = "1=1";
    	String ClientNameFilter = "1=1";
    	String AreaIdFilter = "1=1";

    	if(CLIENTIP != "" && CLIENTIP.trim().length() > 0){
    		ClientIpFilter = "UPPER(CLIENTIP) = '" + CLIENTIP.toUpperCase() + "'";	
    	}
    	if(CLIENTNAME != "" && CLIENTNAME.trim().length() > 0){
    		ClientNameFilter = "UPPER(CLIENTNAME) LIKE ('%" + CLIENTNAME.toUpperCase() + "%')";	
    	} 
    	if(AREAID != "" && AREAID.trim().length() > 0){
    		AreaIdFilter = "UPPER(AREAID) = '" + AREAID.toUpperCase() + "'";	
    	}  	
    	
        String sqlStmt = "SELECT * FROM TBCLIENT " + 
        				"WHERE " +  ClientIpFilter + " AND " + ClientNameFilter + " " +
        				"AND " + AreaIdFilter + " " +
        				"AND RECORDSTATUS = ? ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteClient(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBCLIENT WHERE ID = ? ";
			//String sqlStmt = "UPDATE TBCLIENT SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);

			pStmt.setInt(1, ID);
			
			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}

	public ArrayList<TBCLIENT> getClientData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBCLIENT> alClient = new ArrayList<TBCLIENT>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBCLIENT oClient = new TBCLIENT();
			oClient.setID(srs.getInt("ID"));
			oClient.setCLIENTIP(srs.getString("CLIENTIP"));
			oClient.setCLIENTNAME(srs.getString("CLIENTNAME"));
			oClient.setAREAID(srs.getString("AREAID"));
			oClient.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oClient.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oClient.setCREATEDBY(srs.getString("CREATEDBY"));
			oClient.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oClient.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alClient.add(oClient);
		}

		return alClient;
	}
}
