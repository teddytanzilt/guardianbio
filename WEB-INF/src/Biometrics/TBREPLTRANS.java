package Biometrics;

import java.util.Date;

public class TBREPLTRANS {
	private int RecordId;
	private String DeviceId;
	private String StaffId;
	private int FingerIndex;
	private String RType;
	private String Logdate;
	private String ExtInfo;
	
	private String BRANCHID;
	
	public int getRecordId() {return RecordId;}
	public void setRecordId(int RecordId) {this.RecordId = RecordId;}
	
	public String getDeviceId() {return DeviceId;}
	public void setDeviceId(String DeviceId) {this.DeviceId = DeviceId;}
	
	public String getStaffId() {return StaffId;}
	public void setStaffId(String StaffId) {this.StaffId = StaffId;}
	
	public int getFingerIndex() {return FingerIndex;}
	public void setFingerIndex(int FingerIndex) {this.FingerIndex = FingerIndex;}
	
	public String getRType() {return RType;}
	public void setRType(String RType) {this.RType = RType;}
	
	public String getLogdate() {return Logdate;}
	public void setLogdate(String Logdate) {this.Logdate = Logdate;}
	
	public String getExtInfo() {return ExtInfo;}
	public void setExtInfo(String ExtInfo) {this.ExtInfo = ExtInfo;}
	
	public String getBRANCHID() {return BRANCHID;}
	public void setBRANCHID(String BRANCHID) {this.BRANCHID = BRANCHID;}
}
