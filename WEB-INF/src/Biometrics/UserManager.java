package Biometrics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
//import java.sql.Statement;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UserManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addUser(TBUSER oUser) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBUSER WHERE USERNAME = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oUser.getUSERNAME());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "User Name [" + oUser.getUSERNAME()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBUSER " + 
			"( USERNAME , PASSWORD, NAME, ROLE, RECORDSTATUS, CREATEDBY, CREATEDSTAMP) " + 
			"VALUES (?, ?, ?, ?, ?, ?, '" + currentDateTime + "') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oUser.getUSERNAME());
			pStmt.setString(2, oUser.getPASSWORD());
			pStmt.setString(3, oUser.getNAME());
			pStmt.setString(4, oUser.getROLE());
			pStmt.setString(5, "A");
			pStmt.setString(6, oUser.getCREATEDBY());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateUser(TBUSER oUser) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBUSER WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oUser.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "User Name [" + oUser.getUSERNAME()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBUSER SET "
								+ "  USERNAME = ? " 
								+ ", PASSWORD = ? "
								+ ", NAME = ? " 
								+ ", ROLE = ? " 
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oUser.getUSERNAME());
			pStmt.setString(2, oUser.getPASSWORD());
			pStmt.setString(3, oUser.getNAME());
			pStmt.setString(4, oUser.getROLE());
			pStmt.setString(5, oUser.getLASTUPDATETIMEDBY());
			pStmt.setInt(6, oUser.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public TBUSER getUser(String USERNAME, String PASSWORD) throws SQLException {
		 
		String sqlStmt = "SELECT * FROM TBUSER WHERE UPPER(USERNAME) = ? AND RECORDSTATUS = ? ";
		
		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
		
		pStmt.setString(1, USERNAME.toUpperCase());
		pStmt.setString(2, "A");

		ArrayList<TBUSER> alUser;

		alUser = this.getUserData(pStmt);

		if (alUser.size() == 0)
			return null;
		
		if (!alUser.get(0).getPASSWORD().equals(PASSWORD))
			return null;

		return alUser.get(0);
	}
	
	public TBUSER getUserById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBUSER WHERE ID = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "A");
		
		ArrayList<TBUSER> alUser;

		alUser = this.getUserData(pStmt);

		if (alUser.size() == 0)
			return null;

		return alUser.get(0);		
	}
	
	public TBUSER getUserByUserName(String USERNAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBUSER WHERE UPPER(USERNAME) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, USERNAME.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBUSER> alUser;

		alUser = this.getUserData(pStmt);

		if (alUser.size() == 0)
			return null;

		return alUser.get(0);		
	}
	
	public TBUSER[] getUserAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBUSER";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by USERNAME) as RowNum " +
    					"FROM "+tableName+" WHERE RECORDSTATUS = ? ) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBUSER> alUser;

		alUser = this.getUserData(pStmt);

		if (alUser.size() == 0)
			return null;

		TBUSER[] users = new TBUSER[alUser.size()];

		alUser.toArray(users);

		return users;
	}
	
	public int countUserAll() throws SQLException {
    	String sqlStmt = "SELECT * FROM TBUSER WHERE RECORDSTATUS = ? ";        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public TBUSER[] getUserFilterByUserName(String USERNAME) throws SQLException {
        String sqlStmt = "SELECT * FROM TBUSER WHERE UPPER(USERNAME) = ? AND RECORDSTATUS = ? ";
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, USERNAME.toUpperCase());
        pStmt.setString(2, "A");
        ArrayList<TBUSER> alUser = new ArrayList<TBUSER>();
        alUser = this.getUserData(pStmt);
        TBUSER[] users = new TBUSER[alUser.size()];
		alUser.toArray(users);
		return users;
    }
	
	public TBUSER[] getUserByFilter(String USERNAME, String NAME, String offset) throws SQLException {
    	String idFilter = "1 = 1";
    	String nameFilter = "1 = 1";
    	String itemPerPage = "30";
    	if(USERNAME != "" && USERNAME.trim().length() > 0){
    		idFilter = "UPPER(USERNAME) = '" + USERNAME.toUpperCase() + "'";	
    	}
    	if(NAME != "" && NAME.trim().length() > 0){
    		nameFilter = "UPPER(NAME) LIKE ('%" + NAME.toUpperCase() + "%')";	
    	}  
    	String tableName = "TBUSER";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum FROM "+tableName+ " " +
    					"WHERE " + idFilter + " AND " + nameFilter + " AND RECORDSTATUS = ? ) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " "; 
    					
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        
        ArrayList<TBUSER> alUser = new ArrayList<TBUSER>();
        alUser = this.getUserData(pStmt);
        TBUSER[] users = new TBUSER[alUser.size()];
		alUser.toArray(users);
		return users;
    }
	
	public int countUserByFilter(String USERNAME, String NAME) throws SQLException {
    	String idFilter = "1=1";
    	String nameFilter = "1=1";
    	
    	if(USERNAME != "" && USERNAME.trim().length() > 0){
    		idFilter = "UPPER(TBUSER.USERNAME) = '" + USERNAME.toUpperCase() + "'";	
    	}
    	if(NAME != "" && NAME.trim().length() > 0){
    		nameFilter = "UPPER(TBUSER.NAME) LIKE ('%" + NAME.toUpperCase() + "%')";	
    	}    	
    	
        String sqlStmt = "SELECT * FROM TBUSER " + 
        				"WHERE " +  idFilter + " AND " + nameFilter + " AND RECORDSTATUS = ? ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteUser(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBUSER WHERE ID = ? ";
			//String sqlStmt = "UPDATE TBUSER SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);
			
			pStmt.setInt(1, ID);

			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}

	public ArrayList<TBUSER> getUserData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBUSER> alUser = new ArrayList<TBUSER>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBUSER oUser = new TBUSER();
			oUser.setID(srs.getInt("ID"));
			oUser.setUSERNAME(srs.getString("USERNAME"));
			oUser.setPASSWORD(srs.getString("PASSWORD"));
			oUser.setNAME(srs.getString("NAME"));
			oUser.setROLE(srs.getString("ROLE"));
			oUser.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oUser.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oUser.setCREATEDBY(srs.getString("CREATEDBY"));
			oUser.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oUser.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alUser.add(oUser);
		}

		return alUser;
	}
}
