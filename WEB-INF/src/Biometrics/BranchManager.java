package Biometrics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class BranchManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addBranch(TBBRANCH oBranch) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBBRANCH WHERE BRANCHID = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oBranch.getBRANCHID());
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "Branch [" + oBranch.getBRANCHID()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBBRANCH " + 
			"( BRANCHID , BRANCHNAME, AREAID, EMAILNOTIFYADDRESS , RECORDSTATUS, CREATEDBY, CREATEDSTAMP) " + 
			"VALUES (?, ?, ?, ?, ?, ?, '" + currentDateTime + "') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oBranch.getBRANCHID());
			pStmt.setString(2, oBranch.getBRANCHNAME());
			pStmt.setString(3, oBranch.getAREAID());
			pStmt.setString(4, oBranch.getEMAILNOTIFYADDRESS());
			pStmt.setString(5, "A");
			pStmt.setString(6, oBranch.getCREATEDBY());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateBranch(TBBRANCH oBranch) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBBRANCH WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oBranch.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Branch [" + oBranch.getBRANCHID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBBRANCH SET "
								+ "  BRANCHID = ? " 
								+ ", BRANCHNAME = ? "
								+ ", AREAID = ? "
								+ ", EMAILNOTIFYADDRESS = ? " 
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oBranch.getBRANCHID());
			pStmt.setString(2, oBranch.getBRANCHNAME());
			pStmt.setString(3, oBranch.getAREAID());
			pStmt.setString(4, oBranch.getEMAILNOTIFYADDRESS());
			pStmt.setString(5, oBranch.getLASTUPDATETIMEDBY());
			pStmt.setInt(6, oBranch.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public TBBRANCH getBranchById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBBRANCH WHERE ID = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "A");
		
		ArrayList<TBBRANCH> alBranch;

		alBranch = this.getBranchData(pStmt);

		if (alBranch.size() == 0)
			return null;

		return alBranch.get(0);		
	}
	
	public TBBRANCH getBranchByBranchId(String BRANCHID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBBRANCH WHERE UPPER(BRANCHID) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, BRANCHID.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBBRANCH> alBranch;

		alBranch = this.getBranchData(pStmt);

		if (alBranch.size() == 0)
			return null;

		return alBranch.get(0);		
	}
	
	public TBBRANCH getBranchByBranchName(String BRANCHNAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBBRANCH WHERE UPPER(BRANCHNAME) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, BRANCHNAME.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBBRANCH> alBranch;

		alBranch = this.getBranchData(pStmt);

		if (alBranch.size() == 0)
			return null;

		return alBranch.get(0);		
	}
	
	public TBBRANCH[] getBranchAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM (" + 
    	"Select TBBRANCH.*,TBAREA.AREANAME, ROW_NUMBER() over (order by TBBRANCH.ID) as RowNum " +
    	"FROM TBBRANCH " +
    	"LEFT JOIN TBAREA ON (TBAREA.AREAID = TBBRANCH.AREAID AND TBAREA.RECORDSTATUS = ?) " + 
    	"WHERE TBBRANCH.RECORDSTATUS = ? ) TableName " +  
    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	pStmt.setString(2, "A");
    	
		ArrayList<TBBRANCH> alBranch;

		alBranch = this.getBranchDataComplex(pStmt);

		if (alBranch.size() == 0)
			return null;

		TBBRANCH[] users = new TBBRANCH[alBranch.size()];

		alBranch.toArray(users);

		return users;
	}
	
	public int countBranchAll() throws SQLException {
		String sqlStmt = "Select TBBRANCH.*,TBAREA.AREANAME " +
		"FROM TBBRANCH " +
		"LEFT JOIN TBAREA ON (TBAREA.AREAID = TBBRANCH.AREAID AND TBAREA.RECORDSTATUS = ?) " + 
		"WHERE TBBRANCH.RECORDSTATUS = ? ";
		     
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        pSelectStmt.setString(2, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBBRANCH[] getBranchByFilter(String BRANCHID, String BRANCHNAME, String AREAID, String offset) throws SQLException {
    	String BranchIdFilter = "1=1";
    	String BranchNameFilter = "1=1";
    	String AreaIdFilter = "1=1";
    	String itemPerPage = "30";
    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBBRANCH.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	}
    	if(BRANCHNAME != "" && BRANCHNAME.trim().length() > 0){
    		BranchNameFilter = "UPPER(TBBRANCH.BRANCHNAME) LIKE ('%" + BRANCHNAME.toUpperCase() + "%')";	
    	} 
    	if(AREAID != "" && AREAID.trim().length() > 0){
    		AreaIdFilter = "UPPER(TBBRANCH.AREAID) = '" + AREAID.toUpperCase() + "'";	
    	}
    	    	
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM (" + 
    	"Select TBBRANCH.*,TBAREA.AREANAME, ROW_NUMBER() over (order by TBBRANCH.ID) as RowNum " +
    	"FROM TBBRANCH " +
    	"LEFT JOIN TBAREA ON (TBAREA.AREAID = TBBRANCH.AREAID AND TBAREA.RECORDSTATUS = ?) " + 
    	"WHERE " + BranchIdFilter + " AND " + BranchNameFilter + " AND " + AreaIdFilter + " AND TBBRANCH.RECORDSTATUS = ? ) TableName " +  
    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        pStmt.setString(2, "A");
        
        ArrayList<TBBRANCH> alBranch = new ArrayList<TBBRANCH>();
        alBranch = this.getBranchDataComplex(pStmt);
        TBBRANCH[] users = new TBBRANCH[alBranch.size()];
		alBranch.toArray(users);
		return users;
    }
	
	public int countBranchByFilter(String BRANCHID, String BRANCHNAME, String AREAID) throws SQLException {
		String BranchIdFilter = "1=1";
    	String BranchNameFilter = "1=1";
    	String AreaIdFilter = "1=1";

    	if(BRANCHID != "" && BRANCHID.trim().length() > 0){
    		BranchIdFilter = "UPPER(TBBRANCH.BRANCHID) = '" + BRANCHID.toUpperCase() + "'";	
    	}
    	if(BRANCHNAME != "" && BRANCHNAME.trim().length() > 0){
    		BranchNameFilter = "UPPER(TBBRANCH.BRANCHNAME) LIKE ('%" + BRANCHNAME.toUpperCase() + "%')";	
    	} 
    	if(AREAID != "" && AREAID.trim().length() > 0){
    		AreaIdFilter = "UPPER(TBBRANCH.AREAID) = '" + AREAID.toUpperCase() + "'";	
    	} 	
    	
    	String sqlStmt = "Select TBBRANCH.*,TBAREA.AREANAME " +
		"FROM TBBRANCH " +
		"LEFT JOIN TBAREA ON (TBAREA.AREAID = TBBRANCH.AREAID AND TBAREA.RECORDSTATUS = ?) " + 
		"WHERE " +  BranchIdFilter + " AND " + BranchNameFilter + " AND " + AreaIdFilter + " AND TBBRANCH.RECORDSTATUS = ? ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        pSelectStmt.setString(2, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteBranch(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBBRANCH WHERE ID = ? ";
			//String sqlStmt = "UPDATE TBBRANCH SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);

			pStmt.setInt(1, ID);
			
			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public TBBRANCH[] getBranchAllNoOffset() throws SQLException {
    	String tableName = "TBBRANCH";
    	String sqlStmt = "SELECT * FROM "+tableName+" WHERE RECORDSTATUS = ? ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBBRANCH> alBranch;

		alBranch = this.getBranchData(pStmt);

		if (alBranch.size() == 0)
			return null;

		TBBRANCH[] branchs = new TBBRANCH[alBranch.size()];

		alBranch.toArray(branchs);

		return branchs;
	}

	public ArrayList<TBBRANCH> getBranchData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBBRANCH> alBranch = new ArrayList<TBBRANCH>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBBRANCH oBranch = new TBBRANCH();
			oBranch.setID(srs.getInt("ID"));
			oBranch.setBRANCHID(srs.getString("BRANCHID"));
			oBranch.setBRANCHNAME(srs.getString("BRANCHNAME"));
			oBranch.setAREAID(srs.getString("AREAID"));
			oBranch.setEMAILNOTIFYADDRESS(srs.getString("EMAILNOTIFYADDRESS"));
			oBranch.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oBranch.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oBranch.setCREATEDBY(srs.getString("CREATEDBY"));
			oBranch.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oBranch.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alBranch.add(oBranch);
		}

		return alBranch;
	}
	
	public ArrayList<TBBRANCH> getBranchDataComplex(PreparedStatement pStmt)
			throws SQLException {
		
		ArrayList<TBBRANCH> alBranch = new ArrayList<TBBRANCH>();
		
		ResultSet srs = pStmt.executeQuery();
		
		while (srs.next()) {
			TBBRANCH oBranch = new TBBRANCH();
			oBranch.setID(srs.getInt("ID"));
			oBranch.setBRANCHID(srs.getString("BRANCHID"));
			oBranch.setBRANCHNAME(srs.getString("BRANCHNAME"));
			oBranch.setAREAID(srs.getString("AREAID"));
			oBranch.setAREANAME(srs.getString("AREANAME"));
			oBranch.setEMAILNOTIFYADDRESS(srs.getString("EMAILNOTIFYADDRESS"));
			oBranch.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oBranch.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oBranch.setCREATEDBY(srs.getString("CREATEDBY"));
			oBranch.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oBranch.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alBranch.add(oBranch);
		}
		
		return alBranch;
	}
}
