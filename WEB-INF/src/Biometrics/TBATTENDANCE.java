package Biometrics;

import java.util.Date;

public class TBATTENDANCE {
	private int ID;
	private String STAFFID;
	private String DEVICEID;
	private String BRANCHID;
	private String INOUT;
	private String ATTENDANCEDATE;
	private String ATTENDANCETIME;
	private String REASONID;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getSTAFFID() {return STAFFID;}
	public void setSTAFFID(String STAFFID) {this.STAFFID = STAFFID;}
	
	public String getDEVICEID() {return DEVICEID;}
	public void setDEVICEID(String DEVICEID) {this.DEVICEID = DEVICEID;}
	
	public String getBRANCHID() {return BRANCHID;}
	public void setBRANCHID(String BRANCHID) {this.BRANCHID = BRANCHID;}
	
	public String getINOUT() {return INOUT;}
	public void setINOUT(String INOUT) {this.INOUT = INOUT;}
	
	public String getATTENDANCEDATE() {return ATTENDANCEDATE;}
	public void setATTENDANCEDATE(String ATTENDANCEDATE) {this.ATTENDANCEDATE = ATTENDANCEDATE;}
	
	public String getATTENDANCETIME() {return ATTENDANCETIME;}
	public void setATTENDANCETIME(String ATTENDANCETIME) {this.ATTENDANCETIME = ATTENDANCETIME;}
	
	public String getREASONID() {return REASONID;}
	public void setREASONID(String REASONID) {this.REASONID = REASONID;}
}
