package Biometrics;

import java.util.Date;

public class REPORT2 {
	private String STAFFID;
	private String STAFFNAME;
	private String DEVICEID;
	private String DEVICENAME;
	private String BRANCHID;
	private String BRANCHNAME;
	private String ATTENDANCEDATE;
	private String ATTENDANCETIME;
	private String INOUT;
	
	public String getSTAFFID() {return STAFFID;}
	public void setSTAFFID(String STAFFID) {this.STAFFID = STAFFID;}
	
	public String getSTAFFNAME() {return STAFFNAME;}
	public void setSTAFFNAME(String STAFFNAME) {this.STAFFNAME = STAFFNAME;}
	
	public String getDEVICEID() {return DEVICEID;}
	public void setDEVICEID(String DEVICEID) {this.DEVICEID = DEVICEID;}
	
	public String getDEVICENAME() {return DEVICENAME;}
	public void setDEVICENAME(String DEVICENAME) {this.DEVICENAME = DEVICENAME;}
	
	public String getBRANCHID() {return BRANCHID;}
	public void setBRANCHID(String BRANCHID) {this.BRANCHID = BRANCHID;}
	
	public String getBRANCHNAME() {return BRANCHNAME;}
	public void setBRANCHNAME(String BRANCHNAME) {this.BRANCHNAME = BRANCHNAME;}
	
	public String getATTENDANCEDATE() {return ATTENDANCEDATE;}
	public void setATTENDANCEDATE(String ATTENDANCEDATE) {this.ATTENDANCEDATE = ATTENDANCEDATE;}
	
	public String getATTENDANCETIME() {return ATTENDANCETIME;}
	public void setATTENDANCETIME(String ATTENDANCETIME) {this.ATTENDANCETIME = ATTENDANCETIME;}

	public String getINOUT() {return INOUT;}
	public void setINOUT(String INOUT) {this.INOUT = INOUT;}
	
}
