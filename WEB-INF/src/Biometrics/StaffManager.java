package Biometrics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class StaffManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addStaff(TBSTAFF oStaff) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBSTAFF WHERE STAFFID = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oStaff.getSTAFFID());
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "Staff [" + oStaff.getSTAFFID()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBSTAFF " + 
			"( STAFFID , STAFFNAME, CARDNO, PRIVILEGE, PASSWORD, ACCESSGROUPID, RECORDSTATUS, CREATEDBY, CREATEDSTAMP) " + 
			"VALUES (?, ?, ?, ?, ?, ?, ?, ?, '" + currentDateTime + "') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oStaff.getSTAFFID());
			pStmt.setString(2, oStaff.getSTAFFNAME());
			pStmt.setString(3, oStaff.getCARDNO());
			pStmt.setString(4, oStaff.getPRIVILEGE());
			pStmt.setString(5, oStaff.getPASSWORD());
			pStmt.setString(6, oStaff.getACCESSGROUPID());
			pStmt.setString(7, "A");
			pStmt.setString(8, oStaff.getCREATEDBY());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateStaff(TBSTAFF oStaff) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBSTAFF WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oStaff.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Staff [" + oStaff.getSTAFFID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBSTAFF SET "
								+ "  STAFFID = ? " 
								+ ", STAFFNAME = ? "
								+ ", CARDNO = ? "
								+ ", PRIVILEGE = ? "
								+ ", PASSWORD = ? "
								+ ", ACCESSGROUPID = ? " 
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oStaff.getSTAFFID());
			pStmt.setString(2, oStaff.getSTAFFNAME());
			pStmt.setString(3, oStaff.getCARDNO());
			pStmt.setString(4, oStaff.getPRIVILEGE());
			pStmt.setString(5, oStaff.getPASSWORD());
			pStmt.setString(6, oStaff.getACCESSGROUPID());
			pStmt.setString(7, oStaff.getLASTUPDATETIMEDBY());
			pStmt.setInt(8, oStaff.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public TBSTAFF getStaffById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBSTAFF WHERE ID = ? AND RECORDSTATUS != ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "D");
		
		ArrayList<TBSTAFF> alStaff;

		alStaff = this.getStaffData(pStmt);

		if (alStaff.size() == 0)
			return null;

		return alStaff.get(0);		
	}
	
	public TBSTAFF getStaffByStaffId(String STAFFID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBSTAFF WHERE UPPER(STAFFID) = ? AND RECORDSTATUS != ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, STAFFID.toUpperCase());
		pStmt.setString(2, "D");
		
		ArrayList<TBSTAFF> alStaff;

		alStaff = this.getStaffData(pStmt);

		if (alStaff.size() == 0)
			return null;

		return alStaff.get(0);		
	}
	
	public TBSTAFF getStaffByStaffName(String STAFFNAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBSTAFF WHERE UPPER(STAFFNAME) = ? AND RECORDSTATUS != ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, STAFFNAME.toUpperCase());
		pStmt.setString(2, "D");
		
		ArrayList<TBSTAFF> alStaff;

		alStaff = this.getStaffData(pStmt);

		if (alStaff.size() == 0)
			return null;

		return alStaff.get(0);		
	}
	
	public TBSTAFF[] getStaffAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM (" + 
    	"Select TBSTAFF.*,TBACCESSGROUP.ACCESSGROUPNAME, ROW_NUMBER() over (order by TBSTAFF.ID) as RowNum " +
    	"FROM TBSTAFF " +
    	"LEFT JOIN TBACCESSGROUP ON (TBACCESSGROUP.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID AND TBACCESSGROUP.RECORDSTATUS = ?) " + 
    	"WHERE TBSTAFF.RECORDSTATUS != ? ) TableName " +  
    	"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	pStmt.setString(2, "D");
    	
		ArrayList<TBSTAFF> alStaff;

		alStaff = this.getStaffDataComplex(pStmt);

		if (alStaff.size() == 0)
			return null;

		TBSTAFF[] staffs = new TBSTAFF[alStaff.size()];

		alStaff.toArray(staffs);

		return staffs;
	}
	
	public int countStaffAll() throws SQLException {
		String sqlStmt = "Select TBSTAFF.*,TBACCESSGROUP.ACCESSGROUPNAME " +
		"FROM TBSTAFF " +
		"LEFT JOIN TBACCESSGROUP ON (TBACCESSGROUP.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID AND TBACCESSGROUP.RECORDSTATUS = ?) " + 
		"WHERE TBSTAFF.RECORDSTATUS != ? ";
		
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        pSelectStmt.setString(2, "D");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBSTAFF[] getStaffByFilter(String STAFFID, String STAFFNAME, String CARDNO, String ACCESSGROUPID, String RECORDSTATUS, String offset) throws SQLException {
    	String StaffIdFilter = "1=1";
    	String StaffNameFilter = "1=1";
    	String CardNoFilter = "1=1";
    	String AccessGroupIdFilter = "1=1";
    	String RecordStatusFilter = "1=1";
    	String itemPerPage = "30";
    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE ('%" + STAFFNAME.toUpperCase() + "%')";	
    	} 
    	if(CARDNO != "" && CARDNO.trim().length() > 0){
    		CardNoFilter = "UPPER(TBSTAFF.CARDNO) = '" + CARDNO.toUpperCase() + "'";	
    	}
    	if(ACCESSGROUPID != "" && ACCESSGROUPID.trim().length() > 0){
    		AccessGroupIdFilter = "UPPER(TBSTAFF.ACCESSGROUPID) = '" + ACCESSGROUPID.toUpperCase() + "'";
    	}   	
    	if(RECORDSTATUS != "" && RECORDSTATUS.trim().length() > 0){
    		RecordStatusFilter = "UPPER(TBSTAFF.RECORDSTATUS) = '" + RECORDSTATUS.toUpperCase() + "'";
    	}   	
    	
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM ("+
    	"Select TBSTAFF.*,TBACCESSGROUP.ACCESSGROUPNAME,ROW_NUMBER() over (order by TBSTAFF.ID) as RowNum " +
		"FROM TBSTAFF " +
		"LEFT JOIN TBACCESSGROUP ON (TBACCESSGROUP.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID AND TBACCESSGROUP.RECORDSTATUS = ?) " +
		"WHERE " + StaffIdFilter + " AND " + StaffNameFilter + " " +
		"AND " + CardNoFilter + " AND " + AccessGroupIdFilter + " AND " + RecordStatusFilter + " AND TBSTAFF.RECORDSTATUS != ? ) TableName " + 
		"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";

        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        pStmt.setString(2, "D");
        
        ArrayList<TBSTAFF> alStaff = new ArrayList<TBSTAFF>();
        alStaff = this.getStaffDataComplex(pStmt);
        TBSTAFF[] staffs = new TBSTAFF[alStaff.size()];
		alStaff.toArray(staffs);
		return staffs;
    }
	
	public int countStaffByFilter(String STAFFID, String STAFFNAME, String CARDNO, String ACCESSGROUPID, String RECORDSTATUS) throws SQLException {
		String StaffIdFilter = "1=1";
    	String StaffNameFilter = "1=1";
    	String CardNoFilter = "1=1";
    	String AccessGroupIdFilter = "1=1";
    	String RecordStatusFilter = "1=1";

    	if(STAFFID != "" && STAFFID.trim().length() > 0){
    		StaffIdFilter = "UPPER(TBSTAFF.STAFFID) = '" + STAFFID.toUpperCase() + "'";	
    	}
    	if(STAFFNAME != "" && STAFFNAME.trim().length() > 0){
    		StaffNameFilter = "UPPER(TBSTAFF.STAFFNAME) LIKE ('%" + STAFFNAME.toUpperCase() + "%')";	
    	} 
    	if(CARDNO != "" && CARDNO.trim().length() > 0){
    		CardNoFilter = "UPPER(TBSTAFF.CARDNO) = '" + CARDNO.toUpperCase() + "'";	
    	}
    	if(ACCESSGROUPID != "" && ACCESSGROUPID.trim().length() > 0){
    		AccessGroupIdFilter = "UPPER(TBSTAFF.ACCESSGROUPID) = '" + ACCESSGROUPID.toUpperCase() + "'";	
    	}   	
    	if(RECORDSTATUS != "" && RECORDSTATUS.trim().length() > 0){
    		RecordStatusFilter = "UPPER(TBSTAFF.RECORDSTATUS) = '" + RECORDSTATUS.toUpperCase() + "'";
    	}
    	
		String sqlStmt = "Select TBSTAFF.*,TBACCESSGROUP.ACCESSGROUPNAME " +
    					"FROM TBSTAFF " +
    					"LEFT JOIN TBACCESSGROUP ON (TBACCESSGROUP.ACCESSGROUPID = TBSTAFF.ACCESSGROUPID AND TBACCESSGROUP.RECORDSTATUS = ?) " + 
    					"WHERE " +  StaffIdFilter + " AND " + StaffNameFilter + " " +
        				"AND " + CardNoFilter + " AND " + AccessGroupIdFilter + " " +
        				"AND " + RecordStatusFilter + " " +
        				"AND TBSTAFF.RECORDSTATUS != ? ";
		
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        pSelectStmt.setString(2, "D");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteStaff(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBSTAFF WHERE ID = ? ";
			//String sqlStmt = "UPDATE TBSTAFF SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			/*pStmt.setString(1, "D");
			pStmt.setInt(2, ID);*/
			
			pStmt.setInt(1, ID);

			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean deleteStaff(String STAFFID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBSTAFF WHERE STAFFID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, STAFFID);
			
			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);
			
			//pStmt.setInt(1, ID);

			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean deleteStaffThumb(String STAFFID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBSTAFFTHUMB WHERE STAFFID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, STAFFID);
			
			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);
			
			//pStmt.setInt(1, ID);

			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean suspendStaff(int ID) {
		try {
			connection.setAutoCommit(false);

			//String sqlStmt = "DELETE FROM TBSTAFF WHERE ID = ? ";
			String sqlStmt = "UPDATE TBSTAFF SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, "S");
			pStmt.setInt(2, ID);
			
			//pStmt.setInt(1, ID);

			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean unsuspendStaff(int ID) {
		try {
			connection.setAutoCommit(false);

			//String sqlStmt = "DELETE FROM TBSTAFF WHERE ID = ? ";
			String sqlStmt = "UPDATE TBSTAFF SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, "A");
			pStmt.setInt(2, ID);
			
			//pStmt.setInt(1, ID);

			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete user master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}

	public ArrayList<TBSTAFF> getStaffData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBSTAFF> alStaff = new ArrayList<TBSTAFF>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBSTAFF oStaff = new TBSTAFF();
			oStaff.setID(srs.getInt("ID"));
			oStaff.setSTAFFID(srs.getString("STAFFID"));
			oStaff.setSTAFFNAME(srs.getString("STAFFNAME"));
			oStaff.setCARDNO(srs.getString("CARDNO"));
			oStaff.setPRIVILEGE(srs.getString("PRIVILEGE"));
			oStaff.setPASSWORD(srs.getString("PASSWORD"));
			oStaff.setACCESSGROUPID(srs.getString("ACCESSGROUPID"));
			oStaff.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oStaff.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oStaff.setCREATEDBY(srs.getString("CREATEDBY"));
			oStaff.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oStaff.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alStaff.add(oStaff);
		}

		return alStaff;
	}
	
	public ArrayList<TBSTAFF> getStaffDataComplex(PreparedStatement pStmt)
			throws SQLException {
		
		ArrayList<TBSTAFF> alStaff = new ArrayList<TBSTAFF>();
		
		ResultSet srs = pStmt.executeQuery();
		
		while (srs.next()) {
			TBSTAFF oStaff = new TBSTAFF();
			oStaff.setID(srs.getInt("ID"));
			oStaff.setSTAFFID(srs.getString("STAFFID"));
			oStaff.setSTAFFNAME(srs.getString("STAFFNAME"));
			oStaff.setCARDNO(srs.getString("CARDNO"));
			oStaff.setPRIVILEGE(srs.getString("PRIVILEGE"));
			oStaff.setPASSWORD(srs.getString("PASSWORD"));
			oStaff.setACCESSGROUPID(srs.getString("ACCESSGROUPID"));
			oStaff.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oStaff.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oStaff.setCREATEDBY(srs.getString("CREATEDBY"));
			oStaff.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oStaff.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			oStaff.setACCESSGROUPNAME(srs.getString("ACCESSGROUPNAME"));
			alStaff.add(oStaff);
		}
		
		return alStaff;
	}

}
