package Biometrics;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class AreaManager {

	private String errorMessage = "";
	private Connection connection;

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public boolean addArea(TBAREA oArea) {

		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {

			connection.setAutoCommit(false);

			String sqlSelectStmt = "SELECT * FROM TBAREA WHERE AREAID = ? AND RECORDSTATUS = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setString(1, oArea.getAREAID());
			pSelectStmt.setString(2, "A");

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (rsSelect.next()) {
				errorMessage = "Area [" + oArea.getAREAID()
						+ "] has been exists";
				return false;
			}

			String sqlStmt = "INSERT INTO TBAREA " + 
			"( AREAID , AREANAME, RECORDSTATUS, CREATEDBY, CREATEDSTAMP) " + 
			"VALUES (?, ?, ?, ?, '" + currentDateTime + "') ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oArea.getAREAID());
			pStmt.setString(2, oArea.getAREANAME());
			pStmt.setString(3, "A");
			pStmt.setString(4, oArea.getCREATEDBY());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public boolean updateArea(TBAREA oArea) {
		
		Date now = Calendar.getInstance().getTime();
		String currentDateTime = util.DateFormatConverter.format2(now, "yyyy-MM-dd HH:mm:ss.SSS");
		
		try {
			connection.setAutoCommit(false);
			
			String sqlSelectStmt = "SELECT * FROM TBAREA WHERE ID = ? ";

			PreparedStatement pSelectStmt = connection
					.prepareStatement(sqlSelectStmt);

			pSelectStmt.setInt(1, oArea.getID());

			ResultSet rsSelect = pSelectStmt.executeQuery();

			if (!rsSelect.next()) {
				errorMessage = "Area [" + oArea.getAREAID()
						+ "] not found";
				return false;
			}

			String sqlStmt = "UPDATE TBAREA SET "
								+ "  AREAID = ? " 
								+ ", AREANAME = ? "
								+ ", LASTUPDATETIMEDBY = ? "
								+ ", LASTUPDATETIMEDSTAMP = '" + currentDateTime + "' "
					+ "WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			pStmt.setString(1, oArea.getAREAID());
			pStmt.setString(2, oArea.getAREANAME());
			pStmt.setString(3, oArea.getLASTUPDATETIMEDBY());
			pStmt.setInt(4, oArea.getID());
			
			pStmt.executeUpdate();

			connection.commit();

			return true;

		} catch (SQLException ex) {
			errorMessage = ex.getMessage();
			return false;
		}
		
	}
	
	public TBAREA getAreaById(int ID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBAREA WHERE ID = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setInt(1, ID);
		pStmt.setString(2, "A");
		
		ArrayList<TBAREA> alArea;

		alArea = this.getAreaData(pStmt);

		if (alArea.size() == 0)
			return null;

		return alArea.get(0);		
	}
	
	public TBAREA getAreaByAreaId(String AREAID) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBAREA WHERE UPPER(AREAID) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, AREAID.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBAREA> alArea;

		alArea = this.getAreaData(pStmt);

		if (alArea.size() == 0)
			return null;

		return alArea.get(0);		
	}
	
	public TBAREA getAreaByAreaName(String AREANAME) throws SQLException {			
		String sqlStmt = "SELECT * FROM TBAREA WHERE UPPER(AREANAME) = ? AND RECORDSTATUS = ? ";

		PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);

		pStmt.setString(1, AREANAME.toUpperCase());
		pStmt.setString(2, "A");
		
		ArrayList<TBAREA> alArea;

		alArea = this.getAreaData(pStmt);

		if (alArea.size() == 0)
			return null;

		return alArea.get(0);		
	}
	
	public TBAREA[] getAreaAll(String offset) throws SQLException {
    	String itemPerPage = "30";
    	String tableName = "TBAREA";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    		
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum " +
    					"FROM "+tableName+" WHERE RECORDSTATUS = ?) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBAREA> alArea;

		alArea = this.getAreaData(pStmt);

		if (alArea.size() == 0)
			return null;

		TBAREA[] areas = new TBAREA[alArea.size()];

		alArea.toArray(areas);

		return areas;
	}
	
	public int countAreaAll() throws SQLException {
    	String sqlStmt = "SELECT * FROM TBAREA WHERE RECORDSTATUS = ? ";        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
		
	public TBAREA[] getAreaByFilter(String AREAID, String AREANAME, String offset) throws SQLException {
    	String AreaIdFilter = "1 = 1";
    	String AreaNameFilter = "1 = 1";
    	String itemPerPage = "30";
    	if(AREAID != "" && AREAID.trim().length() > 0){
    		AreaIdFilter = "UPPER(AREAID) = '" + AREAID.toUpperCase() + "'";	
    	}
    	if(AREANAME != "" && AREANAME.trim().length() > 0){
    		AreaNameFilter = "UPPER(AREANAME) LIKE ('%" + AREANAME.toUpperCase() + "%')";	
    	} 
    	String tableName = "TBAREA";
    	int startRow = Integer.parseInt(offset) + 1;
    	int endRow = Integer.parseInt(offset) + Integer.parseInt(itemPerPage);
    	
    	String sqlStmt = "SELECT * FROM " + 
    					"(Select *, ROW_NUMBER() over (order by ID) as RowNum FROM "+tableName+" " +
    					"WHERE " + AreaIdFilter + " AND " + AreaNameFilter + " AND RECORDSTATUS = ? ) TableName " + 
    					"WHERE RowNum >= " + startRow + " AND RowNum <= " + endRow + " ";
    	
        PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
        pStmt.setString(1, "A");
        
        ArrayList<TBAREA> alArea = new ArrayList<TBAREA>();
        alArea = this.getAreaData(pStmt);
        TBAREA[] areas = new TBAREA[alArea.size()];
		alArea.toArray(areas);
		return areas;
    }
	
	public int countAreaByFilter(String AREAID, String AREANAME) throws SQLException {
		String AreaIdFilter = "1=1";
    	String AreaNameFilter = "1=1";

    	if(AREAID != "" && AREAID.trim().length() > 0){
    		AreaIdFilter = "UPPER(AREAID) = '" + AREAID.toUpperCase() + "'";	
    	}
    	if(AREANAME != "" && AREANAME.trim().length() > 0){
    		AreaNameFilter = "UPPER(AREANAME) LIKE ('%" + AREANAME.toUpperCase() + "%')";	
    	} 
    	
        String sqlStmt = "SELECT * FROM TBAREA " + 
        				"WHERE " +  AreaIdFilter + " AND " + AreaNameFilter + " " +
        				"AND RECORDSTATUS = ? ";
        
        String sSelectStmt = "SELECT COUNT(*) AS COUNTED FROM ("+ sqlStmt + ") THETABLE ";
        PreparedStatement pSelectStmt = this.connection.prepareStatement(sSelectStmt);
        pSelectStmt.setString(1, "A");
        ResultSet srs = pSelectStmt.executeQuery();
        while (srs.next()) { 
        	return srs.getInt("COUNTED");
        }    	
        return 0; 
    }
	
	public boolean deleteArea(int ID) {
		try {
			connection.setAutoCommit(false);

			String sqlStmt = "DELETE FROM TBAREA WHERE ID = ? ";
			//String sqlStmt = "UPDATE TBAREA SET RECORDSTATUS = ? WHERE ID = ? ";

			PreparedStatement pStmt = connection.prepareStatement(sqlStmt);

			//pStmt.setString(1, "D");
			//pStmt.setInt(2, ID);

			pStmt.setInt(1, ID);
			
			int iRet = pStmt.executeUpdate();

			if (iRet != 1) {
				connection.rollback();
				errorMessage = "Failed to delete area master data";
				return false;
			}

			connection.commit();

			return true;

		} catch (SQLException ex) {

			errorMessage = ex.getMessage();
			return false;
		}
	}
	
	public TBAREA[] getAreaAllNoOffset() throws SQLException {
    	String tableName = "TBAREA";
    	String sqlStmt = "SELECT * FROM "+tableName+" WHERE RECORDSTATUS = ? ";
  
    	PreparedStatement pStmt = this.connection.prepareStatement(sqlStmt);
    	pStmt.setString(1, "A");
    	
		ArrayList<TBAREA> alArea;

		alArea = this.getAreaData(pStmt);

		if (alArea.size() == 0)
			return null;

		TBAREA[] areas = new TBAREA[alArea.size()];

		alArea.toArray(areas);

		return areas;
	}

	public ArrayList<TBAREA> getAreaData(PreparedStatement pStmt)
			throws SQLException {

		ArrayList<TBAREA> alArea = new ArrayList<TBAREA>();

		ResultSet srs = pStmt.executeQuery();

		while (srs.next()) {
			TBAREA oArea = new TBAREA();
			oArea.setID(srs.getInt("ID"));
			oArea.setAREAID(srs.getString("AREAID"));
			oArea.setAREANAME(srs.getString("AREANAME"));
			oArea.setRECORDSTATUS(srs.getString("RECORDSTATUS"));
			oArea.setCREATEDSTAMP(srs.getTimestamp("CREATEDSTAMP"));
			oArea.setCREATEDBY(srs.getString("CREATEDBY"));
			oArea.setLASTUPDATETIMEDSTAMP(srs.getTimestamp("LASTUPDATETIMEDSTAMP"));
			oArea.setLASTUPDATETIMEDBY(srs.getString("LASTUPDATETIMEDBY"));
			alArea.add(oArea);
		}

		return alArea;
	}
}
