package Biometrics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

public class DeviceControl {

	/** Update (add/delete) branches in an access group.
	 * @param oldbranches : list of branches (comma delimited string) before edit.
	 * @param newbranches : list of branches (comma delimited string) after edit.
	 * @param accessgrpid : id of the target access group.
	 */
	public static void moveFromBranchesToBranches(String oldbranches, String newbranches, String accessgrpid)
	{
		String[] oldlist = null;
		String[] newlist = null;
		
		// Load the branches (comma delimited) into array for ease of use.
		if (oldbranches.length() > 0)
			oldlist = oldbranches.split(",");
		if (newbranches.length() > 0)
			newlist = newbranches.split(",");

		// Identify which branch to be added and which to be deleted. 
		if (oldlist != null && newlist != null)
		{
			// Compare the 2 array and strike-out the common branch.
			for (int i=0; i < newlist.length; i++)
			{
				// look for the new branch in the old list.
				for (int ix=0; ix < oldlist.length; ix++)
				{
					if (newlist[i].equals(oldlist[ix]))
					{
						// strike-out this (common) element in both array.
						newlist[i] = "";
						oldlist[ix] = "";
						break;
					}
				}
			}
		}
		
		// Delete old branches from the access group.
        if (oldlist != null)
        {        
        	StringBuilder oldbrns = new StringBuilder();
        	// Build the list of old branch into a single string.
	        for (String s : oldlist)
	        {
	        	if (s.length() > 0)
	        	{
	        		if (oldbrns.length() > 0)
	        			oldbrns.append(",");
	        		oldbrns.append(s);
	        	}
	        }
	        
	        if (oldbrns.length() > 0)
	        {
	        	System.out.println("Delete " + oldbrns.toString());
	        	String response = deleteAccessGrp(accessgrpid, oldbrns.toString());
	        	System.out.println(response);	        
	        }
		}
        
        // Enrol new branches into the access group.
        if (newlist != null)
        {
        	StringBuilder newbrns = new StringBuilder();
	        // Build the list of new branch into a single string.
	        for (String s : newlist)
	        {
	        	if (s.length() > 0)
	        	{
	        		if (newbrns.length() > 0)
	        			newbrns.append(",");
	        		newbrns.append(s);
	        	}
	        }
        	
	        if (newbrns.length() > 0)
	        {
	        	System.out.println("Enroll " + newbrns.toString());
	        	String response = enrollAccessGrp(accessgrpid, newbrns.toString());
	        	System.out.println(response);	
	        }
        }
	}
	
	/** 
	 * Add a new Device to the DeviceService engine.
	 * @return OK = successful, ER message = error
	 */
	public static String addDevice(String unitid, String ipaddr, String portnum, String branchid)
	{
		final String ACTIONCODE = "1";
		
		String actstr = ACTIONCODE+"|"+unitid+"|"+ipaddr+","+portnum+","+branchid;
		String response = submitAction(actstr);
		return response;		
	}

	public static String disableDevice(String unitid)
	{
		final String ACTIONCODE = "2";
		
		String actstr = ACTIONCODE+"|"+unitid+"|";
		String response = submitAction(actstr);
		return response;		
	}

	public static String enableDevice(String unitid)
	{
		final String ACTIONCODE = "3";
		
		String actstr = ACTIONCODE+"|"+unitid+"|";
		String response = submitAction(actstr);
		return response;				
	}
   
	public static String restartDevice(String unitid)
	{
		final String ACTIONCODE = "4";
		

		String actstr = ACTIONCODE+"|"+unitid+"|";
		String response = submitAction(actstr);
		return response;				
	}
	
	/**
	 * Restore the users master data to a Device.
	 */
	public static String restoreMasterData(String unitid)
	{
		final String ACTIONCODE = "5";
		
		String actstr = ACTIONCODE+"|"+unitid+"|";
		String response = submitAction(actstr);
		return response;				
	}	
	

	/**
	 * Enrol (or update) a staff profile into Device.
	 */
	public static String enrollStaff(String staffid)
	{
		final String ACTIONCODE = "6";
		
		String actstr = ACTIONCODE+"|"+staffid+"|";
		String response = submitAction(actstr);
		return response;
	}
	
	/**
	 * Delete staff profile including his fingerprint.
	 * @param staffid : Staff Id
	 * @return OK = successful, ER message = error
	 */
	public static String deleteStaff(String staffid)
	{
		final String ACTIONCODE = "7";
		
		String actstr = ACTIONCODE+"|"+staffid+"|";
		String response = submitAction(actstr);
		return response;
	}

	/**
	 * Delete staff's fingerprint. Staff basic profile is retained.
	 * @param staffid
	 * @param fingerinx
	 * @return OK = successful, ER message = error message
	 */
	public static String deleteFingerprint(String staffid, String fingerinx)
	{
		final String ACTIONCODE = "8";
		
		String actstr = ACTIONCODE+"|"+staffid+"|"+fingerinx;
		String response = submitAction(actstr);
		return response;
	}

	/**
	 * Delete a Device from the DeviceService engine.
	 */
	public static String deleteDevice(String unitid)
	{
		final String ACTIONCODE = "9";
		
		String actstr = ACTIONCODE+"|"+unitid+"|";
		String response = submitAction(actstr);
		return response;		
	}
	
	/** Transfer staff from one access group to another.
	 * 
	 * @param staffid
	 * @param oldAccessGrpid
	 * @param newAccessGrpid
	 * @return OK = successful, ER message = error message
	 */
	public static String transferStaff(String staffid, String oldAccessGrpid, String newAccessGrpid)
	{
		final String ACTIONCODE = "10";
		
		String actstr = ACTIONCODE+"|"+staffid+"|"+oldAccessGrpid+","+newAccessGrpid;
		String response = submitAction(actstr);
		return response;
	}
	
	/**
	 * Enrol a list of branches into a specified access group.
	 * @param accessGrpid : Id of the target access group.
	 * @param branches : list of branches (comma delimited string) to be added.
	 * @return OK = successful, ER message = error message
	 */
	public static String enrollAccessGrp(String accessGrpid, String branches)
	{
		final String ACTIONCODE = "11";
		
		String actstr = ACTIONCODE+"|"+accessGrpid+"|"+branches;
		String response = submitAction(actstr);
		return response;
	}

	/** Delete a list of branches from a specified access group.
	 * @param accessGrpid : Id of the target access group.
	 * @param branches : list of branches (comma delimited string) to be deleted. 
	 * @return OK = successful, ER message = error message
	 */
	public static String deleteAccessGrp(String accessGrpid, String branches)
	{
		final String ACTIONCODE = "12";
		
		String actstr = ACTIONCODE+"|"+accessGrpid+"|"+branches;
		String response = submitAction(actstr);
		return response;
	}
	
// ------ Helper classes ---------
	
	/** Submit an action command to the DeviceService. Return a response string.
	 * @param actionstr : formatted string
	 * @return OK = successful, ER message = error
	 */
	private static String submitAction(String actionstr)
	{
		Socket mSock = null;
        String response = "";
        
		try
		{
			mSock = new Socket("127.0.0.1", 4371);			
			BufferedReader in = new BufferedReader(new InputStreamReader(mSock.getInputStream(), StandardCharsets.US_ASCII));
			
			// send action command to server.
			byte[] bytes = actionstr.getBytes(StandardCharsets.US_ASCII);
			OutputStream out = mSock.getOutputStream();
			out.write(bytes);
			out.write(13);
			out.write(10);
			out.flush();
			
			mSock.setSoTimeout(30000);		// read timeout
			StringBuilder s = new StringBuilder(2048);
			String line;
			// read in the entire response message.
			while ((line = in.readLine()) != null)
				s.append(line);

			mSock.setSoLinger(true, 5);
			mSock.close();
			
			response = s.toString();
		}
		
		catch (UnknownHostException e)
		{
			response = "ER UnknownHost";
		}
		catch (IOException e)
		{
			response = "ER " + e.getMessage();
		}
		
		return response;
	}
	
}
