package Biometrics;

import java.util.Date;

public class TBDEVICE {
	private int ID;
	private String DEVICEID;
	private String DEVICENAME;
	private String IPADDRESS;
	private String PORTNO;
	private String BRANCHID;
	private String ALLOWBIOMETRIC;
	private String ALLOWPIN;
	private String ALLOWACCESSCARD;
	private String RECORDSTATUS;
	private String CREATEDBY;
	private Date CREATEDSTAMP;
	private String LASTUPDATETIMEDBY;
	private Date LASTUPDATETIMEDSTAMP;
	private String Status;
	
	private String 	SvcInstance ;
	
	private String BRANCHNAME;
	
	public int getID() {return ID;}
	public void setID(int ID) {this.ID = ID;}
	
	public String getDEVICEID() {return DEVICEID;}
	public void setDEVICEID(String DEVICEID) {this.DEVICEID = DEVICEID;}
	
	public String getDEVICENAME() {return DEVICENAME;}
	public void setDEVICENAME(String DEVICENAME) {this.DEVICENAME = DEVICENAME;}
	
	public String getIPADDRESS() {return IPADDRESS;}
	public void setIPADDRESS(String IPADDRESS) {this.IPADDRESS = IPADDRESS;}
	
	public String getPORTNO() {return PORTNO;}
	public void setPORTNO(String PORTNO) {this.PORTNO = PORTNO;}
	
	public String getBRANCHID() {return BRANCHID;}
	public void setBRANCHID(String BRANCHID) {this.BRANCHID = BRANCHID;}
	
	public String getALLOWBIOMETRIC() {return ALLOWBIOMETRIC;}
	public void setALLOWBIOMETRIC(String ALLOWBIOMETRIC) {this.ALLOWBIOMETRIC = ALLOWBIOMETRIC;}
	
	public String getALLOWPIN() {return ALLOWPIN;}
	public void setALLOWPIN(String ALLOWPIN) {this.ALLOWPIN = ALLOWPIN;}
	
	public String getALLOWACCESSCARD() {return ALLOWACCESSCARD;}
	public void setALLOWACCESSCARD(String ALLOWACCESSCARD) {this.ALLOWACCESSCARD = ALLOWACCESSCARD;}
	
	public String getRECORDSTATUS() {return RECORDSTATUS;}
	public void setRECORDSTATUS(String RECORDSTATUS) {this.RECORDSTATUS = RECORDSTATUS;}
	
	public String getCREATEDBY() {return CREATEDBY;}
	public void setCREATEDBY(String CREATEDBY) {this.CREATEDBY = CREATEDBY;}
	
	public Date getCREATEDSTAMP() {return CREATEDSTAMP;}
	public void setCREATEDSTAMP(Date CREATEDSTAMP) {this.CREATEDSTAMP = CREATEDSTAMP;}
	
	public String getLASTUPDATETIMEDBY() {return LASTUPDATETIMEDBY;}
	public void setLASTUPDATETIMEDBY(String LASTUPDATETIMEDBY) {this.LASTUPDATETIMEDBY = LASTUPDATETIMEDBY;}
	
	public Date getLASTUPDATETIMEDSTAMP() {return LASTUPDATETIMEDSTAMP;}
	public void setLASTUPDATETIMEDSTAMP(Date LASTUPDATETIMEDSTAMP) {this.LASTUPDATETIMEDSTAMP = LASTUPDATETIMEDSTAMP;}
	
	public String getStatus() {return Status;}
	public void setStatus(String Status) {this.Status = Status;}
	
	public String getSvcInstance() {return SvcInstance ;}
	public void setSvcInstance(String SvcInstance ) {this.SvcInstance  = SvcInstance ;}

	public String getBRANCHNAME() {return BRANCHNAME;}
	public void setBRANCHNAME(String BRANCHNAME) {this.BRANCHNAME = BRANCHNAME;}
}
