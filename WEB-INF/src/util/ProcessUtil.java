package util;

public class ProcessUtil {
	
	public boolean CheckRunning(Process process) {
	    try {
	        process.exitValue();
	        return false;
	    } catch (Exception e) {
	        return true;
	    }
	}	
}
