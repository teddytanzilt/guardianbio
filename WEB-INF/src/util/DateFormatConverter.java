package util;

import java.util.Date;

import java.text.SimpleDateFormat;
import java.text.DateFormat;

import java.util.TimeZone;

public final class DateFormatConverter {

	public static String format(Date date, String format) {
		DateFormat df = new SimpleDateFormat(format);
		//df.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
		String result = df.format(date);

		return result;
	}

	public static String format2(Date date, String format) {
		DateFormat df = new SimpleDateFormat(format);
		df.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
		String result = df.format(date);

		return result;
	}

}
