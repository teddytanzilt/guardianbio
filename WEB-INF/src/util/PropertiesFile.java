package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFile {
	
	public static String getProperty(String filePath, String propertyName) throws Exception {
		
		String value = "";
		
		try {
			Properties properties = new Properties();
			
			properties.load(new FileInputStream(filePath));
			value = properties.getProperty(propertyName);
			properties = null;
		}catch (Exception e) {
		    throw e;
		}
		
		return value;
	}
	
	public static void setProperty(String filePath, String propertyName, String value) throws Exception {
		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream(filePath));
			//properties.put(propertyName, value);
			properties.setProperty(propertyName, value);
			FileOutputStream out = new FileOutputStream(filePath);
			properties.store(out, "/* properties updated */");
		} catch (Exception e) {
			throw e;
		}
	}

}
