package util;

public class PadString {
	
	public static String toCenter(String value, char padChar, int length){
		
		char[] result = new char[length];
		
		for (int i = 0; i < result.length; i++)
				result[i] = padChar;
		
		//int beginIndex = length - value.length() % 2 == 1 ? (length - value.length() - 1) / 2 : (length - value.length()) / 2;
		
		int beginIndex = (length - value.length()) / 2;
		
		value.getChars(0, value.length(), result, beginIndex); 
		
		return new String(result);
	}
	
	public static String toLeft(String value, char padChar, int length){
		
		char[] result = new char[length];
		
		for (int i = 0; i < result.length; i++)
				result[i] = padChar;
			
		value.getChars(0, value.length(), result, length - value.length());
		
		return new String(result);
	}
	
	public static String toRight(String value, char padChar, int length){

		char[] result = new char[length];
		
		for (int i = 0; i < result.length; i++)
				result[i] = padChar;
			
		value.getChars(0, value.length(), result, 0);
		
		return new String(result);
	}
}
